package com.ratection;

import com.ratection.common.util.DateUtils;

import java.sql.*;
import java.util.Date;
import java.util.Random;

/**
 * Created by twh on 17/7/23.
 */
public class GenerateData {
    public static void main(String[] args) throws Exception {
        generateRandomData();
    }

    public static void generateRandomData() throws Exception {
        Statement statement = getStatement();
        Integer dis = 50;
        double longitude = 104.06;
        double latitude = 30.67;
        double r = 6371.137;//地球半径米
        double dlat = dis / r;
        dlat = dlat * 180 / Math.PI;
        double minlng = Double.valueOf(longitude) - dlat;
        double minlat = Double.valueOf(latitude) - dlat;
        double maxlng = Double.valueOf(longitude) + dlat;
        double maxlat = Double.valueOf(latitude) + dlat;
        Date now = DateUtils.getDate();
        Integer count = 100000;
        String city = "成都";
        String province = "四川";
        System.out.println("begin insert!");
        for (Integer i = 3; i <= count; i++) {
            now = new Date(now.getTime() + 300000);
            Integer userId = 1;
            if (i % 2 == 0) {
                userId = 1;
            } else {
                userId = 2;
            }
            int times = userId * 210;
            double radomlongitude = radom(minlng, maxlng);
            double radomlatitude = radom(minlat, maxlat);
            String district = getDistrict(radomlongitude, radomlatitude);
            String sql="insert into hj_cp_data(id,checkpoint_value,noise_value,type,longitude,latitude,temp,envid,envname,continue_time,remark,device_version,mac,province,city,district,business,street,user_id,is_delete,check_time) values" +
                    "(" + i + "," + radom(0, 1000)
                    + "," + 20 + "," + 0 + ","
                    + radomlongitude + "," + radomlatitude + "," + "30.0"
                    + "," + 1 + "," + "'室内'"
                    + "," + times + "," + "'111'," + "'v1.2'"
                    + "," + "'34:23:87:F4:A4:38'" + ",'" + province + "','" + city + "','"
                    + district + "'," + "'1'" + "," + "'1'"
                    + "," + userId + "," + 0
                    + ",'" + new Timestamp(now.getTime()) + "')";
            execute(statement,sql);
        }
        statement.close();
        System.out.println("end insert!");
    }

    public static double radom(double min, double max) {
        Random random = new Random();
        return min + (max - min) * random.nextDouble();
    }

    public static String getDistrict(double longitude, double latitude) {
        if (longitude >= 103.6 && longitude <= 103.8 && latitude >= 30.2 && latitude <= 30.4) {
            return "高新区";
        } else if (longitude > 103.8 && longitude <= 104.0 && latitude > 30.4 && latitude <= 30.6) {
            return "武侯区";
        } else if (longitude > 104.0 && longitude <= 104.2 && latitude > 30.6 && latitude <= 30.8) {
            return "青羊区";
        } else if (longitude > 104.2 && longitude <= 104.4 && latitude > 30.8 && latitude <= 40.0) {
            return "青北江区";
        } else if (longitude > 104.4 && longitude <= 104.6 && latitude > 40.0 && latitude <= 40.2) {
            return "龙泉驿区";
        } else {
            return "新都区";
        }
    }

    public static Statement getStatement() {
        String driverName = "com.mysql.jdbc.Driver";
        String dbURL = "jdbc:mysql://127.0.0.1:3306/hj?&useUnicode=true&characterEncoding=UTF-8";
        String userName = "root";
        String userPwd = "franklin";
        try {
            Class.forName(driverName);
            Connection dbConn = DriverManager.getConnection(dbURL, userName, userPwd);
            Statement st = dbConn.createStatement();
            return st;
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static void execute(Statement st, String sql) {
        try {
            st.executeUpdate(sql);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

