package com.ratection;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ratection.common.util.DateUtils;

public class Test {
	public static void main(String []args) throws Exception{
		/*Map<String, Object> map = new HashMap<>();
		map.put("content", "反馈测试，你们的appbug太多啦，赶紧升级啊！我擦 ");
		String url = "http://api.ratection.com/v2/user/feedback?token=d7fa09261c911b83a1fc577383a24b9c";
		ObjectMapper mapper = new ObjectMapper();
		String json =  mapper.writeValueAsString(map);
		System.out.println(stream2String(postMethod("http://api.ratection.com/v2/app/addversion?token=e152ab255466253e967e5a1cc96289f9&platform=ios", 
				null,json)));*/
		upload();
		multiupload();
		//System.out.println(DateUtils.getDate(1506066410000l));
	}
	public static void feedback() throws Exception{
		Map<String, Object> map = new HashMap<>();
		map.put("content", "反馈测试，你们的appbug太多啦，赶紧升级啊！我擦 ");
		String url = "http://api.ratection.com/v2/user/feedback?token=d7fa09261c911b83a1fc577383a24b9c";
		ObjectMapper mapper = new ObjectMapper();
		String json =  mapper.writeValueAsString(map);
		System.out.println(json);
		System.out.println(stream2String(postMethod(url, null,json)));
	}
    public static String getDistrict() {
    	int i = new Random().nextInt(5);
        if (i==0) {
            return "高新区";
        } else if (i==1) {
            return "武侯区";
        } else if (i==2) {
            return "青羊区";
        } else if (i==3) {
            return "青北江区";
        } else if (i==4) {
            return "龙泉驿区";
        } else {
            return "新都区";
        }
    }
	public static void upload() throws Exception{
		Map<String, Object> map = new HashMap<>();
		map.put("cpValue", "500");
		map.put("nValue", "12");
		map.put("type", 0);
		map.put("longitude", 116.518484d);
		map.put("latitude", 39.709266);
		map.put("temp", 30.5);
		map.put("envid", 1);
		map.put("envname", "室内");
        String city = "成都";
        String province = "四川";
        map.put("city", city);
        map.put("province", province);
        map.put("district", getDistrict());
		map.put("continueTime", 300);
		map.put("remark", "辐射测试"+System.currentTimeMillis());
		map.put("deviceVersion", "v1.2");
		map.put("mac", "34:23:87:F4:A4:38");
		map.put("userId", 1);
		String url = "http://localhost:8080/v2/data/cpupload?token=02ce368adec1460860afcedbc5ae2e08";
		ObjectMapper mapper = new ObjectMapper();
		String json =  mapper.writeValueAsString(map);
		//System.out.println(json);
		System.out.println(stream2String(postMethod(url, null,json)));
	}
	public static void multiupload() throws Exception{
		List<Map> list = new ArrayList<>();
		Map<String, Object> map = new HashMap<>();
		map.put("checkTime", DateUtils.formatTimestamp("2017-01-01 11:11:11").getTime());
		map.put("cpValue", 1);
		map.put("nValue", "12");
		map.put("type", 0);
		map.put("longitude", 116.518484d);
		map.put("latitude", 39.709266);
		map.put("temp", 30.5);
		map.put("envid", 1);
		map.put("envname", "室内");
        String city = "成都";
        String province = "四川";
        map.put("city", city);
        map.put("province", province);
        map.put("district", getDistrict());
		map.put("continueTime", 300);
		map.put("remark", "辐射测试"+System.currentTimeMillis());
		map.put("deviceVersion", "v1.2");
		map.put("mac", "34:23:87:F4:A4:38");
		list.add(map);
		Map<String, Object> map1 = new HashMap<>();
		map1.put("checkTime", DateUtils.formatTimestamp("2017-01-01 11:11:11").getTime());
		map1.put("cpValue", 1);
		map1.put("nValue", "12");
		map1.put("type", 0);
		map1.put("longitude", 116.518484d);
		map1.put("latitude", 39.709266);
		map1.put("temp", 30.5);
		map1.put("envid", 1);
		map1.put("envname", "室内");
        String city1 = "成都";
        String province1 = "四川";
        map1.put("city", city1);
        map1.put("province", province1);
        map1.put("district", getDistrict());
		map1.put("continueTime", 300);
		map1.put("remark", "辐射测试"+System.currentTimeMillis());
		map1.put("deviceVersion", "v1.2");
		map1.put("mac", "34:23:87:F4:A4:38");
		list.add(map1);
		String url = "http://localhost:8080/v2/data/multiupload?token=02ce368adec1460860afcedbc5ae2e08";
		ObjectMapper mapper = new ObjectMapper();
		String json =  mapper.writeValueAsString(list);
		//System.out.println(json);
		System.out.println(stream2String(postMethod(url, null,json)));
	}
	public static void login()throws JsonProcessingException, IOException{
		Map<String, String> map = new HashMap<>();
		map.put("clientId", "47327d3df06df0b4");map.put("phone", "13693463991");map.put("captcha", "9209");
		String url = "http://api.ratection.com/v2/user/login";
		ObjectMapper mapper = new ObjectMapper();
		String json =  mapper.writeValueAsString(map);
		System.out.println(json);
		System.out.println(stream2String(postMethod(url, null,json)));
	}
    public static String stream2String(InputStream in) {
        if (in != null) {
            try {
                byte[] b = new byte[1024];
                int size;
                StringBuffer sb = new StringBuffer();
                while ((size = in.read(b)) > 0) {
                    sb.append(new String(b, 0, size, "UTF-8"));
                }
                return sb.toString();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }
	public static InputStream postMethod(String url, Map<String, String> headers, String json) throws IOException {
        try {
            HttpURLConnection con = (HttpURLConnection) new URL(url).openConnection();
            //headers
            con.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
            con.setRequestMethod("POST");
            if (headers != null) {
                Set<String> set = headers.keySet();
                Iterator<String> iter = set.iterator();
                while (iter.hasNext()) {
                    String key = iter.next();
                    con.setRequestProperty(key, headers.get(key));
                }
            }
            //body
            con.setDoOutput(true);
            con.setDoInput(true);
            if (json != null) {
                OutputStream os = con.getOutputStream();
                os.write(json.getBytes("utf-8"));
                os.flush();
                os.close();
            }

            if (con.getResponseCode() == 200) {
                return con.getInputStream();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
