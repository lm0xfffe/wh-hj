package com.ratection.module.push.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ratection.common.bean.BasicPageResultBean;
import com.ratection.common.util.PageUtil;
import com.ratection.module.push.service.PushService;

@Controller
public class PushController {
	@Autowired
    ObjectMapper mapper;
	@Autowired
	PushService pushService;
    @RequestMapping(value = "/push/history", method = RequestMethod.GET)
    public
    @ResponseBody
    Object pushhistory( HttpServletRequest request,
    		@RequestParam(required = false, defaultValue = "0") Integer cursor,
			@RequestParam(required = false, defaultValue = "20") Integer limit,
			@RequestParam(required = false) Integer page) throws Exception {
        limit = PageUtil.checkLimit(limit, 20);
		cursor = PageUtil.checkCursorByPage(cursor, limit, page);
		Long total = pushService.getPushsCount();
		List list = pushService.getPushs(cursor, limit);
        return new BasicPageResultBean(total, cursor, limit, list);
    }
}
