package com.ratection.module.push.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.ratection.common.mysql.model.MsgPush;
import com.ratection.common.service.MysqlBasicService;
import com.ratection.common.util.DateUtils;
import com.ratection.common.util.PushMessageUtil;

@Service
public class PushService extends MysqlBasicService{
	public void createAllPush(String name, String content){
		//是否在添加或者修改的时候就把数据推送
		try {
			MsgPush pushBean = new MsgPush();
			pushBean.setMessageTitle(name);
			pushBean.setMessagePublishTime(DateUtils.getDate());
			pushBean.setMessageContent(content);
			pushBean.setStatus("0");
			pushBean.setIsDelete(0);
			String msgAndroid = PushMessageUtil.sendAndroidBroadcast(name, name, content, name);
			String msgIOS = PushMessageUtil.sendIOSBroadcast(content, name);
			if(msgAndroid.indexOf("SUCCESS") > -1 || msgIOS.indexOf("SUCCESS") > -1) {
				pushBean.setStatus("1");
			}
			add(pushBean);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public void createUniPush(Integer type, String devicetoken,String phone, String name, String content){
		//是否在添加或者修改的时候就把数据推送
		try {
			MsgPush pushBean = new MsgPush();
			pushBean.setMessageTitle(name);
			pushBean.setMessagePublishTime(DateUtils.getDate());
			pushBean.setMessageContent(content);
			if(type == 2){
				PushMessageUtil.sendAndroidUnicast(devicetoken, name,name,content, null,null);
			}else if(type == 1){
				PushMessageUtil.sendIOSUnicast(devicetoken, name, content);
			}
			pushBean.setStatus("1");
			add(pushBean);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
    public List<MsgPush> getPushs(Integer cursor, Integer limit) throws Exception {
        String hql = "from " + MsgPush.class.getName() + " where isDelete=0 ";
        hql += " order by messagePublishTime desc ";
        return gets(hql, cursor, limit);
    }

    public long getPushsCount() throws Exception {
        String hql = "select count(*) from " + MsgPush.class.getName();
        return getTotalCountByHql(hql);
    }
    public void deletePush(Integer id) {
        String hql = "update " + MsgPush.class.getName()
                + " m set m.isDelete=1 where id=" + id;
        update(hql);
    }
}
