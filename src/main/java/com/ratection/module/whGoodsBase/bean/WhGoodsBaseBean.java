package com.ratection.module.whGoodsBase.bean;

import java.util.Date;

public class WhGoodsBaseBean {
    private Integer id;

    private Integer warehouseId;

    private Integer goodsId;

    private Date createTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getWarehouseId() {
        return warehouseId;
    }

    public void setWarehouseId(Integer warehouseId) {
        this.warehouseId = warehouseId;
    }

    public Integer getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Integer goodsId) {
        this.goodsId = goodsId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public WhGoodsBaseBean() {
    }

    public WhGoodsBaseBean(Integer id, Integer warehouseId, Integer goodsId, Date createTime) {
        this.id = id;
        this.warehouseId = warehouseId;
        this.goodsId = goodsId;
        this.createTime = createTime;
    }

    @Override
    public String toString() {
        return "WhGoodsBaseBean{" +
                "id=" + id +
                ", warehouseId=" + warehouseId +
                ", goodsId=" + goodsId +
                ", createTime=" + createTime +
                '}';
    }
}
