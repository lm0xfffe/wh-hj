package com.ratection.module.whGoodsBase.service;

import com.ratection.common.mysql.model.WhGoodsBase;
import com.ratection.common.service.MysqlBasicService;
import com.ratection.common.util.MysqlHqlUtil;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class WhGoodsBaseService extends MysqlBasicService {
    public long getWhGoodBaseCount() throws Exception {
        String hql = "select count(*) from " + WhGoodsBase.class.getName();
        return getTotalCountByHql(hql);
    }

    //unchecked
    public List<WhGoodsBase> getWhGoodsBase() throws Exception{
        String goodsQueryHql = " from " + WhGoodsBase.class.getName();
        return gets(goodsQueryHql);
    }

    //unchecked
    public List<WhGoodsBase> getWarehouseGoods(int cursor, int limit) throws Exception {
        Session session = sessionFacotry.getCurrentSession();
        List<WhGoodsBase> list = null;
        try {
            session.beginTransaction();
            Criteria c = session.createCriteria(WhGoodsBase.class);
            c.setFirstResult(cursor);
            c.setMaxResults(limit);
            c.addOrder(Order.desc("id"));
            list = c.list();
            session.getTransaction().commit();
        } catch (Exception e) {
            session.getTransaction().rollback();
            e.printStackTrace();
        }
        return list;
    }

    //unchecked
    public void adWhGoodsBase(WhGoodsBase whgood) throws Exception {
        add(whgood);
    }

    //unchecked
    public void updateWhGoodsBase(WhGoodsBase whgood) {
        update(MysqlHqlUtil.getSingleUpdateHql(whgood, "id"));
    }

    //unchecked
    public void deleteWhGoodsBase(WhGoodsBase whgood) {
        delete(whgood);
    }

    //unchecked
    public WhGoodsBase getWhGoodsBaseByID(Integer id)throws Exception{
        String hql = " from "+ WhGoodsBase.class.getName() + " where id = " + id + " ";
        return get(hql);
    }

    //unchecked
    public List<WhGoodsBase> getWhGoodsBaseByWid(Integer Wid)throws Exception{
        String hql = " from "+ WhGoodsBase.class.getName() + " where warehouseId = " + Wid + " ";
        return gets(hql);
    }

    //unchecked
    public List<WhGoodsBase> getWhGoodsBaseByGid(Integer gid)throws Exception{
        String hql = " from "+ WhGoodsBase.class.getName() + " where goodId = " + gid + " ";
        return gets(hql);
    }

    public WhGoodsBase getWhGoodsBase(Integer Wid, Integer gid)throws Exception{
        String hql = " from "+ WhGoodsBase.class.getName() + " where warehouseId = " + Wid + " and "+ " goodId = " + gid + "";
        return get(hql);
    }
}
