package com.ratection.module.whGoodsBase.controller;

import com.ratection.common.bean.BasicPageResultBean;
import com.ratection.common.bean.SingleResultBean;
import com.ratection.common.mysql.model.Token;
import com.ratection.common.mysql.model.WhGoods;
import com.ratection.common.mysql.model.WhGoodsBase;
import com.ratection.module.whGoodsBase.bean.WhGoodsBaseBean;
import com.ratection.module.whGoodsBase.service.WhGoodsBaseService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
public class WhGoodsBaseController {
    @Autowired
    WhGoodsBaseService whGoodsBaseService;

    private static Logger logger = LoggerFactory.getLogger(WhGoodsBaseController.class);

    @RequestMapping(value = "/whGoodBase/info", method = RequestMethod.GET)
    public
    @ResponseBody
    Object whGoodBaseInfo(
            HttpServletRequest request) throws Exception {
        List<WhGoodsBase> list = whGoodsBaseService.getWhGoodsBase();
        return new BasicPageResultBean((long)list.size(), 0, list.size(), list);
    }

    @RequestMapping(value = "/whGoodBase/id", method = RequestMethod.GET)
    public
    @ResponseBody
    Object whGoodBaseInfoByID(
            HttpServletRequest request,
            @RequestParam(required = false) Integer id) throws Exception {
        WhGoodsBase warehouse = whGoodsBaseService.getWhGoodsBaseByID(id);
        SingleResultBean orb = new SingleResultBean(warehouse);
        return orb;
    }

    @RequestMapping(value = "/whGoodBase/wid", method = RequestMethod.GET)
    public
    @ResponseBody
    Object whGoodBaseInfoByWid(
            HttpServletRequest request,
            @RequestParam(required = false) Integer wid) throws Exception {
        List<WhGoodsBase> list = whGoodsBaseService.getWhGoodsBaseByWid(wid);
        return new BasicPageResultBean((long)list.size(), 0, list.size(), list);
    }

    @RequestMapping(value = "/whGoodBase/gid", method = RequestMethod.GET)
    public
    @ResponseBody
    Object whGoodBaseInfoByGid(
            HttpServletRequest request,
            @RequestParam(required = false) Integer gid) throws Exception {
        List<WhGoodsBase> list = whGoodsBaseService.getWhGoodsBaseByGid(gid);
        return new BasicPageResultBean((long)list.size(), 0, list.size(), list);
    }

    @RequestMapping(value = "/whGoodBase/aid", method = RequestMethod.GET)
    public
    @ResponseBody
    Object whGoodBaseInfoByGid(
            HttpServletRequest request,
            @RequestParam(required = false) Integer wid,
            @RequestParam(required = false) Integer gid) throws Exception {
        WhGoodsBase goodsBase = whGoodsBaseService.getWhGoodsBase(wid, gid);
        SingleResultBean orb = new SingleResultBean(goodsBase);
        return orb;
    }

    @RequestMapping(value = "/whGoodBase/add",method = RequestMethod.POST)
    public @ResponseBody Object add(
            @RequestBody WhGoodsBaseBean warehouseRequest) throws Exception{

        WhGoodsBase whGoods = new WhGoodsBase();
        whGoods.setWarehouseId(warehouseRequest.getWarehouseId());
        whGoods.setGoodsId(warehouseRequest.getGoodsId());
        whGoods.setCreateTime(warehouseRequest.getCreateTime());

        whGoodsBaseService.add(whGoods);
        SingleResultBean orb = new SingleResultBean(whGoods);
        return orb;
    }
    @RequestMapping(value = "/whGoodBase/update",method = RequestMethod.POST)
    public @ResponseBody Object update(@RequestBody WhGoodsBaseBean warehouseRequest){
        WhGoodsBase whGoods = whGoodsBaseService.getById("WhGoodsBase",warehouseRequest.getId());
        whGoods.setWarehouseId(warehouseRequest.getWarehouseId());
        whGoods.setGoodsId(warehouseRequest.getGoodsId());
        whGoods.setCreateTime(warehouseRequest.getCreateTime());

        whGoodsBaseService.updateWhGoodsBase(whGoods);
        SingleResultBean orb = new SingleResultBean(whGoods);
        return orb;
    }

    @RequestMapping(value = "/whGoodBase/delete",method = RequestMethod.POST)
    public @ResponseBody Object delete(@RequestBody WhGoodsBaseBean warehouseRequest){
        WhGoodsBase whGoods = new WhGoodsBase();
        whGoods.setId(warehouseRequest.getId());

        whGoodsBaseService.delete(whGoods);
        SingleResultBean orb = new SingleResultBean(whGoods);
        return orb;
    }
}
