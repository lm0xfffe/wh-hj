package com.ratection.module.goodsBase.controller;

import com.ratection.common.bean.BasicPageResultBean;
import com.ratection.common.bean.SingleResultBean;
import com.ratection.common.mysql.model.GoodsBase;
import com.ratection.module.goodsBase.bean.GoodsBaseBean;
import com.ratection.module.goodsBase.service.GoodsBaseService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
public class GoodsBaseController {
    @Autowired
    GoodsBaseService goodsBaseService;

    private static Logger logger = LoggerFactory.getLogger(GoodsBaseController.class);

    @RequestMapping(value = "/goods/info", method = RequestMethod.GET)
    public
    @ResponseBody
    Object info(
            HttpServletRequest request) throws Exception {
            List<GoodsBase> list = goodsBaseService.getGoods();
        return new BasicPageResultBean((long)list.size(), 0, list.size(), list);
    }

    @RequestMapping(value = "/goods/id", method = RequestMethod.GET)
    public
    @ResponseBody
    Object infoByID(
            HttpServletRequest request,
            @RequestParam(required = false) Integer goodsid) throws Exception {
        GoodsBase good = goodsBaseService.getGoodsByID(goodsid);
        SingleResultBean orb = new SingleResultBean(good);
        return orb;
    }

    @RequestMapping(value = "/goods/name", method = RequestMethod.GET)
    public
    @ResponseBody
    Object infoByName(
            HttpServletRequest request,
            @RequestParam(required = false) String name) throws Exception {
        List<GoodsBase> list = goodsBaseService.getGoodsByName(name);
        return new BasicPageResultBean((long)list.size(), 0, list.size(), list);
    }

    @RequestMapping(value = "/goods/company", method = RequestMethod.GET)
    public
    @ResponseBody
    Object infoByCompany(
            HttpServletRequest request,
            @RequestParam(required = false) String company) throws Exception {
        List<GoodsBase> list = goodsBaseService.getGoodsByCompany(company);
        return new BasicPageResultBean((long)list.size(), 0, list.size(), list);
    }

    @RequestMapping(value = "/goods/add",method = RequestMethod.POST)
    public @ResponseBody Object add(@RequestBody GoodsBaseBean goodsRequest) throws Exception{
        GoodsBase goods = new GoodsBase();
        goods.setEquivalent(goodsRequest.getEquivalent());
        goods.setName(goodsRequest.getName());
        goods.setProCompany(goodsRequest.getProCompany());
        goods.setType(goodsRequest.getType());
        goods.setUnit(goodsRequest.getUnit());
        goods.setValidityTemr(goodsRequest.getValidityTemr());

        goodsBaseService.add(goods);
        SingleResultBean orb = new SingleResultBean(goods);
        return orb;
    }
    @RequestMapping(value = "/goods/update",method = RequestMethod.POST)
    public @ResponseBody Object update(@RequestBody GoodsBaseBean goodsRequest){
        GoodsBase goods = goodsBaseService.getById("GoodsBase",goodsRequest.getId());
        goods.setEquivalent(goodsRequest.getEquivalent());
        goods.setName(goodsRequest.getName());
        goods.setProCompany(goodsRequest.getProCompany());
        goods.setType(goodsRequest.getType());
        goods.setUnit(goodsRequest.getUnit());
        goods.setValidityTemr(goodsRequest.getValidityTemr());

        goodsBaseService.updateGoodsBase(goods);
        SingleResultBean orb = new SingleResultBean(goods);
        return orb;
    }

    @RequestMapping(value = "/goods/delete",method = RequestMethod.POST)
    public @ResponseBody Object delete(@RequestBody GoodsBaseBean goodsRequest){
        GoodsBase goods = new GoodsBase();
        goods.setId(goodsRequest.getId());

        goodsBaseService.delete(goods);
        SingleResultBean orb = new SingleResultBean(goods);
        return orb;
    }
}
