package com.ratection.module.goodsBase.service;

import com.ratection.common.mysql.model.GoodsBase;
import com.ratection.common.service.MysqlBasicService;
import com.ratection.common.util.MysqlHqlUtil;
import org.springframework.stereotype.Service;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;

import java.util.List;

@Service
public class GoodsBaseService extends MysqlBasicService {

    //unchecked
    public long getGoodsCount() throws Exception {
        String hql = "select count(*) from " + GoodsBase.class.getName();
        return getTotalCountByHql(hql);
    }

    //unchecked
    public List<GoodsBase> getGoods() throws Exception{
        String goodsQueryHql = " from " + GoodsBase.class.getName();
        return gets(goodsQueryHql);
    }

    //unchecked
    public List<GoodsBase> getGoods(int cursor, int limit) throws Exception {
        Session session = sessionFacotry.getCurrentSession();
        List<GoodsBase> list = null;
        try {
            session.beginTransaction();
            Criteria c = session.createCriteria(GoodsBase.class);
            c.setFirstResult(cursor);
            c.setMaxResults(limit);
            c.addOrder(Order.desc("id"));
            list = c.list();
            session.getTransaction().commit();
        } catch (Exception e) {
            session.getTransaction().rollback();
            e.printStackTrace();
        }
        return list;
    }

    //unchecked
    public void addGoodsBase(GoodsBase goods) throws Exception {
        add(goods);
    }

    //unchecked
    public void updateGoodsBase(GoodsBase goods) {
        update(MysqlHqlUtil.getSingleUpdateHql(goods, "id"));
    }

    //unchecked
    public void deleteGoodsBase(GoodsBase goods) {
        delete(goods);
    }

    //unchecked
    public boolean isGoodsExists(String name) {
        return isValueExists(GoodsBase.class.getName(), "name", name);
    }

    //unchecked
    public GoodsBase getGoodsByID(Integer goodsId)throws Exception{
        String hql = " from "+ GoodsBase.class.getName() + " where id =" + goodsId + " ";
        return get(hql);
    }

    public List<GoodsBase> getGoodsByName(String name)throws Exception{
        String hql = " from "+ GoodsBase.class.getName() + " where name = '" + name + "' ";
        return gets(hql);
    }

    public List<GoodsBase> getGoodsByCompany(String company)throws Exception{
        String hql = " from "+ GoodsBase.class.getName() + " where proCompany = '" + company + "' ";
        return gets(hql);
    }
}
