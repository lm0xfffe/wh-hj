package com.ratection.module.goodsBase.bean;

import java.math.BigDecimal;

public class GoodsBaseBean {
    private Integer id;

    private String name;

    private String unit;

    private BigDecimal equivalent;

    private String proCompany;

    private String validityTemr;

    private String type;

    public GoodsBaseBean() {
        super();
    }

    public GoodsBaseBean(Integer id, String name, String unit, BigDecimal equivalent, String proCompany, String validityTemr, String type) {
        super();

        this.id = id;
        this.name = name;
        this.unit = unit;
        this.equivalent = equivalent;
        this.proCompany = proCompany;
        this.validityTemr = validityTemr;
        this.type = type;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public BigDecimal getEquivalent() {
        return equivalent;
    }

    public void setEquivalent(BigDecimal equivalent) {
        this.equivalent = equivalent;
    }

    public String getProCompany() {
        return proCompany;
    }

    public void setProCompany(String proCompany) {
        this.proCompany = proCompany;
    }

    public String getValidityTemr() {
        return validityTemr;
    }

    public void setValidityTemr(String validityTemr) {
        this.validityTemr = validityTemr;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "GoodsBaseBean{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", unit='" + unit + '\'' +
                ", equivalent=" + equivalent +
                ", proCompany='" + proCompany + '\'' +
                ", validityTemr='" + validityTemr + '\'' +
                ", type='" + type + '\'' +
                '}';
    }
}
