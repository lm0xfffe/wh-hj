package com.ratection.module.whGoods.bean;

import java.math.BigDecimal;
import java.util.Date;

public class WhGoodsBean {
    private Integer id;

    //仓库id
    private int warehouseId;

    //货品id
    private Integer goodsId;

    //数量
    private BigDecimal num;

    //当量
    private BigDecimal equivalent;

    //更新时间
    private Date updateTime;

    //更新人
    private Integer updatePerson;

    //位置
    private String position;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getWarehouseId() {
        return warehouseId;
    }

    public void setWarehouseId(int warehouseId) {
        this.warehouseId = warehouseId;
    }

    public Integer getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Integer goodsId) {
        this.goodsId = goodsId;
    }

    public BigDecimal getNum() {
        return num;
    }

    public void setNum(BigDecimal num) {
        this.num = num;
    }

    public BigDecimal getEquivalent() {
        return equivalent;
    }

    public void setEquivalent(BigDecimal equivalent) {
        this.equivalent = equivalent;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Integer getUpdatePerson() {
        return updatePerson;
    }

    public void setUpdatePerson(Integer updatePerson) {
        this.updatePerson = updatePerson;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public WhGoodsBean() {
        super();
    }

    public WhGoodsBean(Integer id, int warehouseId, Integer goodsId, BigDecimal num, BigDecimal equivalent, Date updateTime, Integer updatePerson, String position) {
        super();
        this.id = id;
        this.warehouseId = warehouseId;
        this.goodsId = goodsId;
        this.num = num;
        this.equivalent = equivalent;
        this.updateTime = updateTime;
        this.updatePerson = updatePerson;
        this.position = position;
    }

    @Override
    public String toString() {
        return "WhGoodsBean{" +
                "id=" + id +
                ", warehouseId=" + warehouseId +
                ", goodsId=" + goodsId +
                ", num=" + num +
                ", equivalent=" + equivalent +
                ", updateTime=" + updateTime +
                ", updatePerson=" + updatePerson +
                ", position='" + position + '\'' +
                '}';
    }
}
