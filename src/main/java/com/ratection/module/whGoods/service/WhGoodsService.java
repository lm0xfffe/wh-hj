package com.ratection.module.whGoods.service;

import com.ratection.common.mysql.model.WhGoods;
import com.ratection.common.service.MysqlBasicService;
import com.ratection.common.util.MysqlHqlUtil;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 仓库货物service
 * @author
 * @version 1.0
 */
@Service
public class WhGoodsService extends MysqlBasicService {
    public long getWarehouseGoodsCount() throws Exception {
        String hql = "select count(*) from " + WhGoods.class.getName();
        return getTotalCountByHql(hql);
    }

    //unchecked
    public List<WhGoods> getWarehouseGoods() throws Exception {
        String goodsQueryHql = " from " + WhGoods.class.getName();
        return gets(goodsQueryHql);
    }

    //unchecked
    public List<WhGoods> getWarehouseGoods(int cursor, int limit) throws Exception {
        Session session = sessionFacotry.getCurrentSession();
        List<WhGoods> list = null;
        try {
            session.beginTransaction();
            Criteria c = session.createCriteria(WhGoods.class);
            c.setFirstResult(cursor);
            c.setMaxResults(limit);
            c.addOrder(Order.desc("id"));
            list = c.list();
            session.getTransaction().commit();
        } catch (Exception e) {
            session.getTransaction().rollback();
            e.printStackTrace();
        }
        return list;
    }

    //unchecked
    public void adWarehouseGoods(WhGoods whgood) throws Exception {
        add(whgood);
    }

    //unchecked
    public void updateWarehouseGoods(WhGoods whgood) {
        update(MysqlHqlUtil.getSingleUpdateHql(whgood, "id"));
    }

    //unchecked
    public void deleteWarehouseGoods(WhGoods whgood) {
        delete(whgood);
    }

    //unchecked
    public WhGoods getWarehouseGoodsByID(Integer whgoodID) throws Exception {
        String hql = " from " + WhGoods.class.getName() + " where id = " + whgoodID + " ";
        return get(hql);
    }

    public List<WhGoods> getWarehouseGoodsByWid(Integer whgoodID) throws Exception {
        String hql = " from " + WhGoods.class.getName() + " where warehouseId = " + whgoodID + " ";
        return gets(hql);
    }

    //unchecked
    public List<WhGoods> getWarehouseGoodsByGid(Integer goodID) throws Exception {
        String hql = " from " + WhGoods.class.getName() + " where goodsId = " + goodID + " ";
        return gets(hql);
    }

    public WhGoods getWarehouseGoods(Integer wid, Integer goodID) throws Exception {
        String hql = " from " + WhGoods.class.getName() + " where warehouseId = " + wid + " and " + " goodsId = " + goodID + "";
        return get(hql);
    }
}
