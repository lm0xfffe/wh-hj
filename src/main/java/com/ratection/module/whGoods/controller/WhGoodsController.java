package com.ratection.module.whGoods.controller;

import com.ratection.common.bean.BasicPageResultBean;
import com.ratection.common.bean.SingleResultBean;
import com.ratection.common.mysql.model.Token;
import com.ratection.common.mysql.model.WhGoods;
import com.ratection.module.whGoods.bean.WhGoodsBean;
import com.ratection.module.whGoods.service.WhGoodsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
public class WhGoodsController {
    @Autowired
    WhGoodsService whGoodsService;

    private static Logger logger = LoggerFactory.getLogger(WhGoodsController.class);

    @RequestMapping(value = "/whGoods/info", method = RequestMethod.GET)
    public
    @ResponseBody
    Object whGoodsInfo(
            HttpServletRequest request) throws Exception {
        List<WhGoods> list = whGoodsService.getWarehouseGoods();
        return new BasicPageResultBean((long)list.size(), 0, list.size(), list);
    }

    @RequestMapping(value = "/whGoods/id", method = RequestMethod.GET)
    public
    @ResponseBody
    Object whGoodsInfoByID(
            HttpServletRequest request,
            @RequestParam(required = false) Integer whID) throws Exception {
        WhGoods warehouse = whGoodsService.getWarehouseGoodsByID(whID);
        SingleResultBean orb = new SingleResultBean(warehouse);
        return orb;
    }

    @RequestMapping(value = "/whGoods/gid", method = RequestMethod.GET)
    public
    @ResponseBody
    Object whGoodsInfoByGid(
            HttpServletRequest request,
            @RequestParam(required = false) Integer gid) throws Exception {
        List<WhGoods> list = whGoodsService.getWarehouseGoodsByGid(gid);
        return new BasicPageResultBean((long)list.size(), 0, list.size(), list);
    }

    @RequestMapping(value = "/whGoods/aid", method = RequestMethod.GET)
    public
    @ResponseBody
    Object whGoodsInfoByGid(
            HttpServletRequest request,
            @RequestParam(required = false) Integer whID,
            @RequestParam(required = false) Integer gid) throws Exception {
        WhGoods goods = whGoodsService.getWarehouseGoods(whID, gid);
        SingleResultBean orb = new SingleResultBean(goods);
        return orb;
    }

    @RequestMapping(value = "/whGoods/add",method = RequestMethod.POST)
    public @ResponseBody Object add(
            HttpServletRequest request,
            @RequestBody WhGoodsBean warehouseRequest) throws Exception{
        Token token = (Token) request.getAttribute("X-TOKEN");

        WhGoods whGoods = new WhGoods();
        whGoods.setUpdatePerson(token.getUserId());
        whGoods.setWarehouseId(warehouseRequest.getWarehouseId());
        whGoods.setGoodsId(warehouseRequest.getGoodsId());
        whGoods.setNum(warehouseRequest.getNum());
        whGoods.setEquivalent(warehouseRequest.getEquivalent());
        whGoods.setUpdateTime(warehouseRequest.getUpdateTime());
        whGoods.setPosition(warehouseRequest.getPosition());

        whGoodsService.add(whGoods);
        SingleResultBean orb = new SingleResultBean(whGoods);
        return orb;
    }
    @RequestMapping(value = "/whGoods/update",method = RequestMethod.POST)
    public @ResponseBody Object update(@RequestBody WhGoodsBean warehouseRequest){
        WhGoods whGoods = whGoodsService.getById("WhGoods",warehouseRequest.getId());
        whGoods.setWarehouseId(warehouseRequest.getWarehouseId());
        whGoods.setGoodsId(warehouseRequest.getGoodsId());
        whGoods.setNum(warehouseRequest.getNum());
        whGoods.setEquivalent(warehouseRequest.getEquivalent());
        whGoods.setUpdateTime(warehouseRequest.getUpdateTime());
        whGoods.setPosition(warehouseRequest.getPosition());

        whGoodsService.updateWarehouseGoods(whGoods);
        SingleResultBean orb = new SingleResultBean(whGoods);
        return orb;
    }

    @RequestMapping(value = "/whGoods/delete",method = RequestMethod.POST)
    public @ResponseBody Object delete(@RequestBody WhGoodsBean warehouseRequest){
        WhGoods whGoods = new WhGoods();
        whGoods.setId(warehouseRequest.getId());

        whGoodsService.delete(whGoods);
        SingleResultBean orb = new SingleResultBean(whGoods);
        return orb;
    }
}

