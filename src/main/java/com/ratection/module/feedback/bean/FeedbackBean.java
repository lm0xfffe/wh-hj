package com.ratection.module.feedback.bean;

import javax.validation.constraints.NotNull;

public class FeedbackBean {
    @NotNull
    private String content;
    @NotNull
    private String phone;
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	@Override
	public String toString() {
		return "FeedbackBean [content=" + content + ", phone=" + phone + "]";
	}

}
