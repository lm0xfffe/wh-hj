package com.ratection.module.feedback.service;


import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Service;

import com.ratection.common.mysql.model.Feedback;
import com.ratection.common.service.MysqlBasicService;
import com.ratection.common.util.MysqlHqlUtil;

@Service
public class FeedbackService extends MysqlBasicService {
    public void addFeedback(Integer userId, String phone, String name, String content) throws Exception {
        Feedback fb = new Feedback();
        fb.setUserId(userId);
        fb.setName(name);
        fb.setPhone(phone);
        fb.setContent(content);
        fb.setState(0);
        fb.setCreateTime(new Date());
        add(fb);
    }
    
    public Feedback getFeedback(Integer id){
    	return getById(Feedback.class.getName(), id);
    }
    public void updateFeedback(Feedback fb){
    	update(MysqlHqlUtil.getSingleUpdateHql(fb, "id"));
    }
    public List<Feedback> getFeedbacks(String phone, String content, Integer cursor, Integer limit) {
        Session session = sessionFacotry.getCurrentSession();
        List<Feedback> list = null;
        try {
            session.beginTransaction();
            Criteria c = session.createCriteria(Feedback.class);
            if(content != null){
				Criterion criterion = Restrictions.like("content", content, MatchMode.ANYWHERE);
				c.add(criterion);
			}
            if(phone != null){
            	c.add(Restrictions.eq("phone", phone));
			}
            c.setFirstResult(cursor);
            c.setMaxResults(limit);
            c.addOrder(Order.desc("id"));
            list = c.list();
            session.getTransaction().commit();
        } catch (Exception e) {
            session.getTransaction().rollback();
            e.printStackTrace();
        }
        return list;
    }

    public long getFeedbacksCount(String phone, String content) throws Exception {
        String hql = "select count(*) from " + Feedback.class.getName()+" where 1=1 ";
		if(phone != null){
			hql += " and phone='" + phone +"'";
		}
		if(content != null){
			hql += " and content like '%" + content +"%'";
		}
        return getTotalCountByHql(hql);
    }
}

