package com.ratection.module.goods.service;

import com.ratection.common.mysql.model.Good;
import com.ratection.common.service.MysqlBasicService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GoodsService extends MysqlBasicService {
    public List<Good> getGoods() throws Exception{
        String deviceQueryHql = " from " + Good.class.getName();
        return gets(deviceQueryHql);
    }
}
