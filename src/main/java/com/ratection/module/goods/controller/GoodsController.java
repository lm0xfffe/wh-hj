package com.ratection.module.goods.controller;

import com.ratection.common.bean.SingleResultBean;
import com.ratection.common.mysql.model.Good;
import com.ratection.common.mysql.model.Version;
import com.ratection.module.goods.bean.GoodCreateBean;
import com.ratection.module.goods.bean.GoodUpdateBean;
import com.ratection.module.goods.service.GoodsService;
import com.ratection.module.user.controller.Usercontroller;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
public class GoodsController {
    @Autowired
    GoodsService goodsService;
    private static Logger logger = LoggerFactory.getLogger(GoodsController.class);
    @RequestMapping(value ="/good/get",method = RequestMethod.GET)
    public @ResponseBody
    Object getGoods(
            HttpServletRequest request) throws Exception {
        List<Good> list = goodsService.getGoods();
        SingleResultBean orb = new SingleResultBean(list);
        return orb;
    }

    @RequestMapping(value ="/good/init",method = RequestMethod.GET)
    public @ResponseBody Object getGoods() throws Exception{
        Good good = new Good();
        good.setName("黑索今");
        good.setUnit("PCS");
        good.setNumber(1.20);
        good.setUnitPrice(1.2);
        good.setAmount(1.20*1.2);
        good.setStatus("R");

        goodsService.add(good);
        List<Good> list = goodsService.getGoods();
        SingleResultBean orb = new SingleResultBean(list);
        return orb;
    }
    @RequestMapping(value = "/good/add",method = RequestMethod.POST)
    public @ResponseBody Object add(@RequestBody GoodCreateBean goodsRequest) throws Exception{
        logger.info("goodsRequest is:" + goodsRequest);
        Good goods = new Good();
        goods.setName(goodsRequest.getName());
        goods.setUnit(goodsRequest.getUnit());
        goods.setNumber(goodsRequest.getNumber());
        goods.setUnitPrice(goodsRequest.getUnitPrice());
        goods.setAmount(goodsRequest.getAmount());
        goods.setStatus(goodsRequest.getStatus());

        goodsService.add(goods);
        SingleResultBean orb = new SingleResultBean(goods);
        return orb;
    }
    @RequestMapping(value = "/good/update",method = RequestMethod.POST)
    public @ResponseBody Object update(@RequestBody GoodUpdateBean goodsRequest){
        Good good = goodsService.getById("Goods",goodsRequest.getId());

        good.setStatus(goodsRequest.getStatus());

        goodsService.updateBean(good);
        SingleResultBean orb = new SingleResultBean(good);
        logger.info("-------------------------updateObject-------------------" + good.getName());
        return orb;
        //goodsService.delete(deletObject);
    }
}
