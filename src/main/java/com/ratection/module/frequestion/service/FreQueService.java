package com.ratection.module.frequestion.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.ratection.common.mysql.model.FreQuestion;
import com.ratection.common.service.MysqlBasicService;

@Service
public class FreQueService extends MysqlBasicService {
	public void createFreQ(FreQuestion fq) throws Exception{
		add(fq);
	}
	public List<FreQuestion> getFreQs() throws Exception{
		String hql = "from " + FreQuestion.class.getName() + " where isDelete=0 ";
		return gets(hql);
	}
    public void deleteFreQs(Integer id) {
        String hql = "update " + FreQuestion.class.getName()
                + " m set m.isDelete=1 where id=" + id;
        update(hql);
    }
}
