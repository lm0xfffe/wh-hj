package com.ratection.module.admin.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ratection.common.mysql.model.CheckPointData;
import com.ratection.common.mysql.model.Feedback;
import com.ratection.common.mysql.model.FreQuestion;
import com.ratection.common.mysql.model.MsgPush;
import com.ratection.common.mysql.model.Token;
import com.ratection.common.mysql.model.User;
import com.ratection.common.util.DateUtils;
import com.ratection.common.util.HttpRequestUtils;
import com.ratection.common.util.PageUtil;
import com.ratection.module.auth.AccessTokenGenerator;
import com.ratection.module.device.service.DeviceService;
import com.ratection.module.feedback.service.FeedbackService;
import com.ratection.module.frequestion.service.FreQueService;
import com.ratection.module.push.service.PushService;
import com.ratection.module.user.bean.UserCacheBean;
import com.ratection.module.user.facade.TokenFacade;
import com.ratection.module.user.facade.UserFacade;

@Controller
public class AdminController {
    @Autowired
    UserFacade userFacade;
    @Autowired
    TokenFacade tokenFacade;
    @Autowired
    FeedbackService feedbackService;
    @Autowired
    DeviceService deviceService;
    @Autowired
    ObjectMapper mapper;
    @Autowired
    PushService pushService;
    @Autowired
    FreQueService freQueService;
    private static Logger logger = LoggerFactory.getLogger(AdminController.class);
    @RequestMapping(value = "/admin")
    public ModelAndView index(HttpServletRequest request, HttpServletResponse response) {
        ModelAndView mav = new ModelAndView("welcome");
        return mav;
    }

    @RequestMapping(value = "/admin/login", method = RequestMethod.GET)
    public ModelAndView loginerror(
            HttpServletRequest request
    ) throws Exception {
        ModelAndView mav = new ModelAndView("welcome");
        return mav;
    }

    @RequestMapping(value = "/admin/login", method = RequestMethod.POST)
    public ModelAndView login(
            HttpServletRequest request,
            @RequestParam String name,
            @RequestParam String password
    ) throws Exception {
    	logger.info("user{}:{} login.",name,password);
        ModelAndView errormav = new ModelAndView("welcome");
        User user = userFacade.getUserbyPhone(name);
        if (user == null) {
            errormav.addObject("errorinfo", "<script language=\"javascript\">alert('用户不存在！');</script>");
        } else if (user.getType() == 1) {
            errormav.addObject("errorinfo", "<script language=\"javascript\">alert('权限不够！');</script>");
        } else if (user.getPassword() != null /*&& !user.getPassword().equals(password)*/) {
            if (user.getType() >= 100) {
                Token token = null;
                UserCacheBean ucb = userFacade.getUser(user.getId());
                if (ucb.getToken() != null) {
                    token = tokenFacade.getToken(ucb.getToken());
                    if(token != null){
                    	token.setUpdateTime(DateUtils.getDate());
                    }else{
                    	token = new Token();
                        token.setClientId("47327d3df06df0b4");
                        token.setUpdateTime(DateUtils.getDate());
                        token.setCreateTime(token.getUpdateTime());
                        token.setUserId(user.getId());
                        token.setName(name);
                        token.setIp(HttpRequestUtils.getIpAddr(request));
                        token.setType(user.getType());
                        token.setToken(AccessTokenGenerator.getInstance().getAccessToken());
                        token.setExpirseIn(AccessTokenGenerator.TokenExpirseIn);
                        tokenFacade.addToken(token);
                    }
                    
                } else {
                    token = new Token();
                    token.setClientId("47327d3df06df0b4");
                    token.setUpdateTime(DateUtils.getDate());
                    token.setCreateTime(token.getUpdateTime());
                    token.setUserId(user.getId());
                    token.setName(name);
                    token.setIp(HttpRequestUtils.getIpAddr(request));
                    token.setType(user.getType());
                    token.setToken(AccessTokenGenerator.getInstance().getAccessToken());
                    token.setExpirseIn(AccessTokenGenerator.TokenExpirseIn);
                    tokenFacade.addToken(token);
                }
                userFacade.updateUserToken(user.getId(), token.getToken());
                userFacade.login(user.getId());

                HttpSession hs = request.getSession(true);
                hs.setMaxInactiveInterval(7200);
                hs.setAttribute("token", token);

                return getusers(request, 0, 20, 1);
            } else {
                errormav.addObject("errorinfo", "<script language=\"javascript\">alert('权限不够！');</script>");
            }
        } else {
        	logger.info("user passwd is {}:{} login.",user.getPassword(),password);
            errormav.addObject("errorinfo", "<script language=\"javascript\">alert('用户名密码错误！');</script>");
        }
        return errormav;
    }

    @RequestMapping(value = "/admin/logout", method = RequestMethod.GET)
    public ModelAndView loginout(HttpSession session) {
        session.removeAttribute("token");
        session.invalidate();
        ModelAndView mav = new ModelAndView("welcome");
        mav.addObject("info", "退出登录");
        return mav;
    }

    @RequestMapping(value = "/admin/getusers", method = RequestMethod.GET)
    public ModelAndView getusers(
            HttpServletRequest request,
            @RequestParam(required = false, defaultValue = "0") Integer cursor,
            @RequestParam(required = false, defaultValue = "50") Integer limit,
            @RequestParam(required = false, defaultValue = "1") Integer page
    ) throws Exception {
        limit = PageUtil.checkLimit(limit, 500);
        cursor = PageUtil.checkCursorByPage(cursor, limit, page);
        List<User> list = userFacade.getUserList(cursor, limit);
        Long total = userFacade.getUserCount();
        ModelAndView mav = new ModelAndView("listusers");
        mav.addObject("list", list);
        mav.addObject("page", page);
        mav.addObject("count", total);
        mav.addObject("navNum", 1);
        mav.addObject("total", PageUtil.getTotalPage(total, limit));
        return mav;
    }

    @RequestMapping(value = "/admin/getcpdatas", method = RequestMethod.GET)
    public ModelAndView getcpdatas(
            HttpServletRequest request,
            @RequestParam(required = false) Integer cursor,
            @RequestParam(required = false) Integer envid,
            @RequestParam(required = false) String phone,
            @RequestParam(required = false) String province,
            @RequestParam(required = false, defaultValue = "50") Integer limit,
            @RequestParam(required = false, defaultValue = "1") Integer page
    ) throws Exception {
    	logger.info("get cp data envid{}:phone{}.",envid,phone);
        Token t = (Token) request.getSession().getAttribute("token");
        limit = PageUtil.checkLimit(limit, 500);
        cursor = PageUtil.checkCursorByPage(cursor, limit, page);
        Integer env = null;
        if (envid != null && envid >= 0) {
            env = envid;
        }
        Integer userId = null;
        if (phone != null && !phone.trim().equals("")) {
            User user = userFacade.getUserbyPhone(phone);
            if (user != null) {
                userId = user.getId();
            } else {
                userId = 0;//TODO::很挫 要改
            }
        }
        List<CheckPointData> cpds = deviceService.getDeviceDatas(userId, env, null, null, null, null, null, province, null,null,cursor, limit);
        long total = deviceService.getDeviceDatasCount(userId, env, null, null,null,null,null, province, null, null);
        ModelAndView mav = new ModelAndView("listcpdatas");
        mav.addObject("list", cpds);
        mav.addObject("page", page);
        mav.addObject("envidret", envid);
        mav.addObject("count", total);
        mav.addObject("navNum", 2);
        mav.addObject("accesstoken", t.getToken());
        mav.addObject("total", PageUtil.getTotalPage(total, limit));
        return mav;
    }

    @RequestMapping(value = "/admin/getcpdatas", method = RequestMethod.POST)
    public ModelAndView postcpdatas(
            HttpServletRequest request,
            @RequestParam(required = false) Integer cursor,
            @RequestParam(required = false) Integer envid,
            @RequestParam(required = false) String phone,
            @RequestParam(required = false, defaultValue = "50") Integer limit,
            @RequestParam(required = false, defaultValue = "1") Integer page
    ) throws Exception {
        return getcpdatas(request, cursor, envid, phone,null, limit, page);
    }

    @RequestMapping(value = "/data/deletes", method = RequestMethod.GET)
    public ModelAndView handlealarm(
            HttpServletRequest request,
            @RequestParam String cpid
    ) throws Exception {
        deviceService.deleteDeviceDatas(cpid);
        return getcpdatas(request, 0, null, null,null,  50, 1);
    }

    @RequestMapping(value = "/admin/getfeedbacks", method = RequestMethod.GET)
    public ModelAndView getfeedbacks(
            HttpServletRequest request,
            @RequestParam(required = false) String phone,
            @RequestParam(required = false) String content,
            @RequestParam(required = false, defaultValue = "0") Integer cursor,
            @RequestParam(required = false, defaultValue = "50") Integer limit,
            @RequestParam(required = false, defaultValue = "1") Integer page
    ) throws Exception {
        limit = PageUtil.checkLimit(limit, 500);
        cursor = PageUtil.checkCursorByPage(cursor, limit, page);
        if(phone != null && phone.trim().equals("")){
        	phone = null;
        }
        if(content != null && content.trim().equals("")){
        	content = null;
        }
        List<Feedback> list = feedbackService.getFeedbacks(phone,content,cursor, limit);
        Long total = feedbackService.getFeedbacksCount(phone,content);
        ModelAndView mav = new ModelAndView("listfeedbacks");
        mav.addObject("list", list);
        mav.addObject("page", page);
        mav.addObject("count", total);
        mav.addObject("navNum", 3);
        mav.addObject("total", PageUtil.getTotalPage(total, limit));
        return mav;
    }
    @RequestMapping(value = "/admin/getfeedbacks", method = RequestMethod.POST)
    public ModelAndView postfbs(
            HttpServletRequest request,
            @RequestParam(required = false) String phone,
            @RequestParam(required = false) String content,
            @RequestParam(required = false) Integer cursor,
            @RequestParam(required = false, defaultValue = "50") Integer limit,
            @RequestParam(required = false, defaultValue = "1") Integer page
    ) throws Exception {
        return getfeedbacks(request, phone, content, cursor, limit, page);
    }
	@RequestMapping(value = "/admin/handlefeedback", method = RequestMethod.GET)
	public ModelAndView handlefeedback(
			HttpServletRequest request,
			@RequestParam Integer id
			) throws Exception{
		Feedback da = feedbackService.getFeedback(id);
		da.setState(1);
		feedbackService.updateFeedback(da);
		return getfeedbacks(request, null, null, 0, 50, 1);
	}


	@RequestMapping(value = "/admin/getpushs", method = RequestMethod.GET)
	public ModelAndView getpushs(
			HttpServletRequest request,
			@RequestParam(required = false) String info,
			@RequestParam(required = false, defaultValue = "0") Integer cursor,
			@RequestParam(required = false, defaultValue = "50") Integer limit,
			@RequestParam(required = false, defaultValue = "1") Integer page
			) throws Exception{
		limit = PageUtil.checkLimit(limit, 500);
		cursor = PageUtil.checkCursorByPage(cursor, limit, page);

		List<MsgPush> list = pushService.getPushs(cursor, limit);
		Long total = pushService.getPushsCount();
		ModelAndView mav = new ModelAndView("listpushs");
		mav.addObject("list", list);
		mav.addObject("page", page);
		mav.addObject("count", total);
		mav.addObject("navNum", 4);
		mav.addObject("info", info);
		mav.addObject("total", PageUtil.getTotalPage(total, limit));
		return mav;
	}
	@RequestMapping(value = "/admin/toaddpush", method = RequestMethod.GET)
	public ModelAndView toaddpush(
			HttpServletRequest request
			) throws Exception{
		ModelAndView mav = new ModelAndView("addpush");
		return mav;
	}
	@RequestMapping(value = "/admin/removepush", method = RequestMethod.GET)
	public ModelAndView removepush(
			HttpServletRequest request,
			@RequestParam Integer id
			) throws Exception{
		pushService.deletePush(id);
		return getpushs(request, null, 0, 100, 1);
	}
	@RequestMapping(value = "/admin/pushlistcost", method = RequestMethod.POST)
	public ModelAndView pushlistcost(
			HttpServletRequest request,
			@RequestParam String name,
			@RequestParam String content
			) throws Exception{
		try{
			pushService.createAllPush(name, content);
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return getpushs(request, null,0, 20, 1);
	}
	@RequestMapping(value = "/admin/pushunicost", method = RequestMethod.POST)
	public ModelAndView pushunicost(
			HttpServletRequest request,
			@RequestParam String name,
			@RequestParam String phone,
			@RequestParam String content
			) throws Exception{
		String info = null;
		try{
			User user = userFacade.getUserbyPhone(phone);
			if(user != null && user.getDevicetoken() != null){
				pushService.createUniPush(user.getpType(), user.getDevicetoken(), phone, name, content);
			}else{
				info = "<script language=\"javascript\">alert('推送失败，用户手机可能禁用了核境推送');</script>";
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return getpushs(request,info, 0, 20, 1);
	}
	
	@RequestMapping(value = "/admin/getfrequestions", method = RequestMethod.GET)
	public ModelAndView getfrequestions(
			HttpServletRequest request
			) throws Exception{
		List list = freQueService.getFreQs();
		ModelAndView mav = new ModelAndView("listfrequestions");
		mav.addObject("list", list);
		mav.addObject("page", 1);
		mav.addObject("count", list.size());
		mav.addObject("navNum", 5);
		mav.addObject("total", 1);
		return mav;
	}
	@RequestMapping(value = "/admin/toaddfrequestions", method = RequestMethod.GET)
	public ModelAndView toaddfrequestions(
			HttpServletRequest request
			) throws Exception{
		ModelAndView mav = new ModelAndView("addfrequestion");
		Token t = (Token) request.getSession().getAttribute("token");
		mav.addObject("accesstoken", t.getToken());
		return mav;
	}
	@RequestMapping(value = "/admin/addfrequestions", method = RequestMethod.POST)
	public ModelAndView addfrequestions(
			HttpServletRequest request,
			@RequestParam String name,
			@RequestParam String content
			) throws Exception{
		ModelAndView mav = new ModelAndView("listfrequestions");
		try{
			FreQuestion fq = new FreQuestion();
			fq.setContent(content);
			fq.setIsDelete(0);
			fq.setCreateTime(DateUtils.getDate());
			fq.setTitle(name);
			freQueService.createFreQ(fq);
		}catch(Exception e){
			e.printStackTrace();
			mav.addObject("errorinfo", "<script language=\"javascript\">alert('添加失败！');</script>");
		}finally{
			mav = getfrequestions(request);
		}
		return mav;
	}
	@RequestMapping(value = "/admin/removefrequestions", method = RequestMethod.GET)
	public ModelAndView removefrequestions(
			HttpServletRequest request,
			@RequestParam Integer id
			) throws Exception{
		freQueService.deleteFreQs(id);
		return getfrequestions(request);
	}

}
