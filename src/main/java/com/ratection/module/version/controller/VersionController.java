package com.ratection.module.version.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.commons.collections.MapUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ratection.common.bean.SingleResultBean;
import com.ratection.common.mysql.model.CheckPointData;
import com.ratection.common.mysql.model.UserDevice;
import com.ratection.common.mysql.model.Token;
import com.ratection.common.mysql.model.Version;
import com.ratection.common.util.DateUtils;
import com.ratection.common.util.LocationUtil;
import com.ratection.module.device.bean.DeviceDataUploadBean;
import com.ratection.module.version.service.VersionService;

@Controller
public class VersionController{
	@Autowired
	ObjectMapper mapper;
	@Autowired
	VersionService versionService;
	//private static Logger logger = LoggerFactory.getLogger(VersionController.class);
	@RequestMapping(value = "/app/version", method = RequestMethod.GET)
	public @ResponseBody
	Object get(
			HttpServletRequest request) throws Exception {
		List<Version> list = versionService.getVersions();
		SingleResultBean orb = new SingleResultBean(list);
		return orb;
	}
	@RequestMapping(value = "/app/addversion", method = RequestMethod.GET)
    public
    @ResponseBody
    Object deviceUpload(
            HttpServletRequest request,
            @RequestParam String platform
    ) throws Exception {
		Version v = new Version();
		v.setAddr("www.ratection.com");
		v.setCreateTime(DateUtils.getDate());
		v.setUpdateTime(v.getCreateTime());
		v.setDescription("初始版本");
		v.setPlatform(platform);
		v.setVersion("v1.0");
		versionService.createVersion(v);
        return new SingleResultBean(v);
    }
}
