package com.ratection.module.version.service;

import java.util.List;




import org.springframework.stereotype.Service;

import com.ratection.common.mysql.model.Version;
import com.ratection.common.service.MysqlBasicService;

@Service
public class VersionService extends MysqlBasicService{
	public void createVersion(Version v) throws Exception{
		add(v);
	}

	public List<Version> getVersions() throws Exception{
		String deviceQueryHql = " from " + Version.class.getName();
		return gets(deviceQueryHql);
	}
}
