package com.ratection.module.whGoodsDetail.controller;

import com.ratection.common.bean.SingleResultBean;
import com.ratection.common.mysql.model.WhGoodsDetail;
import com.ratection.module.whGoodsDetail.bean.WhGoodsDetailBean;
import com.ratection.module.whGoodsDetail.service.WhGoodsDetailService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class WhGoodsDetailController {
    @Autowired
    WhGoodsDetailService whGoodsDetailService;

    private static Logger logger = LoggerFactory.getLogger(WhGoodsDetailController.class);

    @RequestMapping(value = "/whGoodDetail/add",method = RequestMethod.POST)
    public @ResponseBody Object add(
            @RequestBody WhGoodsDetailBean warehouseRequest) throws Exception{

        WhGoodsDetail whGoods = new WhGoodsDetail();
        whGoods.setWarehouseId(warehouseRequest.getWarehouseId());
        whGoods.setGoodsId(warehouseRequest.getGoodsId());
        whGoods.setBatch(warehouseRequest.getBatch());
        whGoods.setNum(warehouseRequest.getNum());
        whGoods.setEnquivalent(warehouseRequest.getEnquivalent());

        whGoodsDetailService.add(whGoods);
        SingleResultBean orb = new SingleResultBean(whGoods);
        return orb;
    }

    @RequestMapping(value = "/whGoodDetail/update",method = RequestMethod.POST)
    public @ResponseBody
    Object update(@RequestBody WhGoodsDetailBean warehouseRequest){
        WhGoodsDetail whGoods = whGoodsDetailService.getById("WhGoodsDetail",warehouseRequest.getId());
        whGoods.setWarehouseId(warehouseRequest.getWarehouseId());
        whGoods.setGoodsId(warehouseRequest.getGoodsId());
        whGoods.setBatch(warehouseRequest.getBatch());
        whGoods.setNum(warehouseRequest.getNum());
        whGoods.setEnquivalent(warehouseRequest.getEnquivalent());

        whGoodsDetailService.updateWhGoodsDetail(whGoods);
        SingleResultBean orb = new SingleResultBean(whGoods);
        return orb;
    }

    @RequestMapping(value = "/whGoodDetail/delete",method = RequestMethod.POST)
    public @ResponseBody Object delete(@RequestBody WhGoodsDetailBean warehouseRequest){
        WhGoodsDetail whGoods = new WhGoodsDetail();
        whGoods.setId(warehouseRequest.getId());

        whGoodsDetailService.delete(whGoods);
        SingleResultBean orb = new SingleResultBean(whGoods);
        return orb;
    }
}
