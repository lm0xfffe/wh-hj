package com.ratection.module.whGoodsDetail.bean;

import java.math.BigDecimal;

public class WhGoodsDetailBean {
    private  Integer id;

    //货品id
    private  Integer goodsId;

    //批次
    private  String batch;

    //数量
    private BigDecimal num;

    //当量
    private BigDecimal enquivalent;

    //仓库id
    private  Integer warehouseId;

    public WhGoodsDetailBean() {
    }

    public WhGoodsDetailBean(Integer id, Integer goodsId, String batch, BigDecimal num, BigDecimal enquivalent, Integer warehouseId) {
        this.id = id;
        this.goodsId = goodsId;
        this.batch = batch;
        this.num = num;
        this.enquivalent = enquivalent;
        this.warehouseId = warehouseId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Integer goodsId) {
        this.goodsId = goodsId;
    }

    public String getBatch() {
        return batch;
    }

    public void setBatch(String batch) {
        this.batch = batch;
    }

    public BigDecimal getNum() {
        return num;
    }

    public void setNum(BigDecimal num) {
        this.num = num;
    }

    public BigDecimal getEnquivalent() {
        return enquivalent;
    }

    public void setEnquivalent(BigDecimal enquivalent) {
        this.enquivalent = enquivalent;
    }

    public Integer getWarehouseId() {
        return warehouseId;
    }

    public void setWarehouseId(Integer warehouseId) {
        this.warehouseId = warehouseId;
    }

    @Override
    public String toString() {
        return "WhGoodsDetailBean{" +
                "id=" + id +
                ", goodsId=" + goodsId +
                ", batch='" + batch + '\'' +
                ", num=" + num +
                ", enquivalent=" + enquivalent +
                ", warehouseId=" + warehouseId +
                '}';
    }
}
