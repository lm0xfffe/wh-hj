package com.ratection.module.whGoodsDetail.service;

import com.ratection.common.mysql.model.WhGoodsDetail;
import com.ratection.common.service.MysqlBasicService;
import com.ratection.common.util.MysqlHqlUtil;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class WhGoodsDetailService extends MysqlBasicService {
    public long getWhGoodsDetailCount() throws Exception {
        String hql = "select count(*) from " + WhGoodsDetail.class.getName();
        return getTotalCountByHql(hql);
    }

    //unchecked
    public List<WhGoodsDetail> getWhGoodsDetail() throws Exception{
        String goodsQueryHql = " from " + WhGoodsDetail.class.getName();
        return gets(goodsQueryHql);
    }

    //unchecked
    public List<WhGoodsDetail> getWhGoodsDetail(int cursor, int limit) throws Exception {
        Session session = sessionFacotry.getCurrentSession();
        List<WhGoodsDetail> list = null;
        try {
            session.beginTransaction();
            Criteria c = session.createCriteria(WhGoodsDetail.class);
            c.setFirstResult(cursor);
            c.setMaxResults(limit);
            c.addOrder(Order.desc("id"));
            list = c.list();
            session.getTransaction().commit();
        } catch (Exception e) {
            session.getTransaction().rollback();
            e.printStackTrace();
        }
        return list;
    }

    //unchecked
    public void addWhGoodsDetail(WhGoodsDetail whgood) throws Exception {
        add(whgood);
    }

    //unchecked
    public void updateWhGoodsDetail(WhGoodsDetail whgood) {
        update(MysqlHqlUtil.getSingleUpdateHql(whgood, "id"));
    }

    //unchecked
    public void deleteWhGoodsDetail(WhGoodsDetail whgood) {
        delete(whgood);
    }

    //unchecked
    public WhGoodsDetail getWhGoodsDetailByID(Integer id)throws Exception{
        String hql = " from "+ WhGoodsDetail.class.getName() + " where id = " + id + " ";
        return get(hql);
    }

    //unchecked
    public List<WhGoodsDetail> getWhGoodsDetailByWid(Integer Wid)throws Exception{
        String hql = " from "+ WhGoodsDetail.class.getName() + " where warehouseId = " + Wid + " ";
        return gets(hql);
    }

    //unchecked
    public List<WhGoodsDetail> getWhGoodsDetailByGid(Integer goodID)throws Exception{
        String hql = " from "+ WhGoodsDetail.class.getName() + " where goodsId = " + goodID + " ";
        return gets(hql);
    }

    public WhGoodsDetail getWhGoodsDetail(Integer Wid, Integer goodID)throws Exception{
        String hql = " from "+ WhGoodsDetail.class.getName() + " where warehouse_id = " + Wid + " and "+ " goods_id = " + goodID + "";
        return get(hql);
    }
}
