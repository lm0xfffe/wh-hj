package com.ratection.module.whInOutRecordDetail.service;

import com.ratection.common.mysql.model.WhInOutRecordDetail;
import com.ratection.common.service.MysqlBasicService;
import com.ratection.common.util.MysqlHqlUtil;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class WhInOutRecordDetailService extends MysqlBasicService {
    //unchecked
    public void addWhInOutRecordDetail(WhInOutRecordDetail record) throws Exception {
        record.setUpdateTime(new Date());
        add(record);
    }

    //unchecked
    public void updateWhInOutRecordDetail(WhInOutRecordDetail record) {
        record.setUpdateTime(new Date());
        update(MysqlHqlUtil.getSingleUpdateHql(record, "id"));
    }

    //unchecked
    public void deleteWhInOutRecordDetail(WhInOutRecordDetail record) {
        delete(record);
    }

    //unchecked
    public List<WhInOutRecordDetail> getWhInOutRecordDetailById(Integer id)throws Exception{
        String hql = " from "+ WhInOutRecordDetail.class.getName() + " where id = " + id + " ";
        return gets(hql);
    }
}
