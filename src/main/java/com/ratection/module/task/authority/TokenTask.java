package com.ratection.module.task.authority;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.ratection.common.mysql.model.Token;
import com.ratection.common.util.DateUtils;
import com.ratection.module.auth.AccessTokenGenerator;
import com.ratection.module.device.service.DeviceService;
import com.ratection.module.user.facade.TokenFacade;
import com.ratection.module.user.facade.UserFacade;
import com.ratection.module.user.service.TokenService;

@Component
public class TokenTask {
    @Autowired
    DeviceService deviceService;

    @Autowired
    TokenService tokenService;

    @Autowired
    TokenFacade tokenFacade;
    @Autowired
    UserFacade userFacade;

    private static Logger logger = LoggerFactory.getLogger(TokenTask.class);

    @Scheduled(cron = "0 0 2 * * ?")
    public void statisticDeviceData() {
        try {
            //TODO:统计 & 清理token
            logger.info("statistic data schedule");
            Date curTime = DateUtils.getDate();
            Date beginTime = DateUtils.getYesterDayBeginTime(curTime);
            Date endTime = DateUtils.getYesterDayEndTime(curTime);
            deviceService.createDeviceStatisticsData(beginTime.getTime(),endTime.getTime());

            logger.info("clear token");
            long totals = tokenService.getTokenCount();
    		List<Token> ls = new ArrayList<Token>();
    		int cursor = 0;
    		while(cursor < totals){
    			try {
    				List<Token> ss = tokenService.getTokens(cursor, 100);
    				ls.addAll(ss);
    				cursor += 100;
    			} catch (Exception e) {
    				logger.error("clear token error,cursor{},total{}", cursor,totals);
    				e.printStackTrace();
    			}
    		}
            for (Token token : ls) {
                Date createTime = token.getCreateTime();
                Date currTime = DateUtils.getDate();
                long diff = currTime.getTime() - createTime.getTime();
                diff = diff/1000;
                if(diff > AccessTokenGenerator.TokenExpirseIn){
                    tokenFacade.deleteToken(token.getToken());
                    userFacade.logout(token.getUserId());
                }
            }
        } catch (Exception e){
            logger.error("statisticDeviceData error!",e);
        }
    }
}


