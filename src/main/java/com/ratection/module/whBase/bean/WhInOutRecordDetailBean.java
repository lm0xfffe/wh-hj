package com.ratection.module.whBase.bean;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

public class WhInOutRecordDetailBean {
    //操作员
    private String operatorName;

    //仓库名称
    private String whName;

    //操作类型，1-出库，2-入库
    private String type;

    //货物名称
    private String goodName;

    //单位
    private String unit;

    private BigDecimal num;

    private Date updateTime;

    private String updateTimeStr;

    public WhInOutRecordDetailBean() {
    }

    public WhInOutRecordDetailBean(String operatorName, String whName, String type, String goodName, String unit, BigDecimal num, Date updateTime) {
        this.operatorName = operatorName;
        this.whName = whName;
        this.type = type;
        this.goodName = goodName;
        this.unit = unit;
        this.num = num;
        this.updateTime = updateTime;

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String dateString = formatter.format(updateTime);
        setUpdateTimeStr(dateString);
    }

    public String getOperatorName() {
        return operatorName;
    }

    public void setOperatorName(String operatorName) {
        this.operatorName = operatorName;
    }

    public String getWhName() {
        return whName;
    }

    public void setWhName(String whName) {
        this.whName = whName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getGoodName() {
        return goodName;
    }

    public void setGoodName(String goodName) {
        this.goodName = goodName;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public BigDecimal getNum() {
        return num;
    }

    public void setNum(BigDecimal num) {
        this.num = num;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getUpdateTimeStr() {
        return updateTimeStr;
    }

    public void setUpdateTimeStr(String updateTimeStr) {
        this.updateTimeStr = updateTimeStr;
    }

    @Override
    public String toString() {
        return "WhInOutRecordDetailBean{" +
                "operatorName='" + operatorName + '\'' +
                ", whName='" + whName + '\'' +
                ", type='" + type + '\'' +
                ", goodName='" + goodName + '\'' +
                ", unit='" + unit + '\'' +
                ", num=" + num +
                ", updateTime=" + updateTime +
                ", updateTimeStr='" + updateTimeStr + '\'' +
                '}';
    }
}
