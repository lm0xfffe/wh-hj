package com.ratection.module.whBase.bean;

import javax.persistence.Entity;
import java.math.BigDecimal;
import java.util.Date;

public class WhGoodsInfoBean {

    private Integer whId;

    private Integer goodId;

    private String goodName;

    private String unit;

    //数量
    private BigDecimal num;

    //当量
    private BigDecimal equivalent;

    //更新时间
    private Date updateTime;

    //位置
    private String position;

    public WhGoodsInfoBean() {
    }

    public WhGoodsInfoBean(Integer whId, Integer goodId, String goodName, String unit, BigDecimal num, BigDecimal equivalent, Date updateTime, String position) {
        this.whId = whId;
        this.goodId = goodId;
        this.goodName = goodName;
        this.unit = unit;
        this.num = num;
        this.equivalent = equivalent;
        this.updateTime = updateTime;
        this.position = position;
    }

    public Integer getWhId() {
        return whId;
    }

    public void setWhId(Integer whId) {
        this.whId = whId;
    }

    public Integer getGoodId() {
        return goodId;
    }

    public void setGoodId(Integer goodId) {
        this.goodId = goodId;
    }

    public String getGoodName() {
        return goodName;
    }

    public void setGoodName(String goodName) {
        this.goodName = goodName;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public BigDecimal getNum() {
        return num;
    }

    public void setNum(BigDecimal num) {
        this.num = num;
    }

    public BigDecimal getEquivalent() {
        return equivalent;
    }

    public void setEquivalent(BigDecimal equivalent) {
        this.equivalent = equivalent;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    @Override
    public String toString() {
        return "WhGoodsInfoBean{" +
                "whId=" + whId +
                ", goodId=" + goodId +
                ", goodName='" + goodName + '\'' +
                ", unit='" + unit + '\'' +
                ", num=" + num +
                ", equivalent=" + equivalent +
                ", updateTime=" + updateTime +
                ", position='" + position + '\'' +
                '}';
    }
}

