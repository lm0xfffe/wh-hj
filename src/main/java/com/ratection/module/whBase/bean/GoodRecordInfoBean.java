package com.ratection.module.whBase.bean;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

public class GoodRecordInfoBean {
    private Integer id;

    private Integer whId;

    private String whName;

    private Integer goodId;

    private String goodName;

    private String unit;

    //数量
    private BigDecimal num;

    //当量
    private BigDecimal equivalent;

    //类型
    private String type;

    //操作人id
    private Integer operator;

    //操作人账号
    private String operatorName;

    //操作时间
    private Date update_time;

    //位置
    private String position;

    //备注
    private String remark;

    //批次
    private String batch;

    //源地点
    private String origination;

    //目的地
    private String destination;

    private String updateTimeStr;

    public GoodRecordInfoBean() {
    }

    public GoodRecordInfoBean(Integer id, Integer whId, String whName, Integer goodId, String goodName, String unit, BigDecimal num, BigDecimal equivalent, String type, Integer operator, String operatorName, Date update_time, String position, String remark, String batch) {
        this.id = id;
        this.whId = whId;
        this.whName = whName;
        this.goodId = goodId;
        this.goodName = goodName;
        this.unit = unit;
        this.num = num;
        this.equivalent = equivalent;
        this.type = type;
        this.operator = operator;
        this.operatorName = operatorName;
        this.update_time = update_time;
        this.position = position;
        this.remark = remark;
        this.batch = batch;

        SetTimeStr(update_time);
    }

    public GoodRecordInfoBean(Integer id, Integer whId, String whName, Integer goodId, String goodName, String unit, BigDecimal num, BigDecimal equivalent, String type, Integer operator, String operatorName, Date update_time, String position, String remark, String batch, String origination, String destination) {
        this.id = id;
        this.whId = whId;
        this.whName = whName;
        this.goodId = goodId;
        this.goodName = goodName;
        this.unit = unit;
        this.num = num;
        this.equivalent = equivalent;
        this.type = type;
        this.operator = operator;
        this.operatorName = operatorName;
        this.update_time = update_time;
        this.position = position;
        this.remark = remark;
        this.batch = batch;
        this.origination = origination;
        this.destination = destination;

        SetTimeStr(update_time);
    }

    public void SetTimeStr(Date update_time) {
        if(update_time != null) {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String dateString = formatter.format(update_time);
            setUpdateTimeStr(dateString);
        }
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getWhId() {
        return whId;
    }

    public void setWhId(Integer whId) {
        this.whId = whId;
    }

    public String getWhName() {
        return whName;
    }

    public void setWhName(String whName) {
        this.whName = whName;
    }

    public Integer getGoodId() {
        return goodId;
    }

    public void setGoodId(Integer goodId) {
        this.goodId = goodId;
    }

    public String getGoodName() {
        return goodName;
    }

    public void setGoodName(String goodName) {
        this.goodName = goodName;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public BigDecimal getNum() {
        return num;
    }

    public void setNum(BigDecimal num) {
        this.num = num;
    }

    public BigDecimal getEquivalent() {
        return equivalent;
    }

    public void setEquivalent(BigDecimal equivalent) {
        this.equivalent = equivalent;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getOperator() {
        return operator;
    }

    public void setOperator(Integer operator) {
        this.operator = operator;
    }

    public String getOperatorName() {
        return operatorName;
    }

    public void setOperatorName(String operatorName) {
        this.operatorName = operatorName;
    }

    public Date getUpdate_time() {
        return update_time;
    }

    public void setUpdate_time(Date update_time) {
        this.update_time = update_time;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getBatch() {
        return batch;
    }

    public void setBatch(String batch) {
        this.batch = batch;
    }

    public String getOrigination() {
        return origination;
    }

    public void setOrigination(String origination) {
        this.origination = origination;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getUpdateTimeStr() {
        return updateTimeStr;
    }

    public void setUpdateTimeStr(String updateTimeStr) {
        this.updateTimeStr = updateTimeStr;
    }

    @Override
    public String toString() {
        return "GoodRecordInfoBean{" +
                "id=" + id +
                ", whId=" + whId +
                ", whName='" + whName + '\'' +
                ", goodId=" + goodId +
                ", goodName='" + goodName + '\'' +
                ", unit='" + unit + '\'' +
                ", num=" + num +
                ", equivalent=" + equivalent +
                ", type='" + type + '\'' +
                ", operator=" + operator +
                ", operatorName='" + operatorName + '\'' +
                ", update_time=" + update_time +
                ", position='" + position + '\'' +
                ", remark='" + remark + '\'' +
                ", batch='" + batch + '\'' +
                ", origination='" + origination + '\'' +
                ", destination='" + destination + '\'' +
                ", updateTimeStr='" + updateTimeStr + '\'' +
                '}';
    }
}
