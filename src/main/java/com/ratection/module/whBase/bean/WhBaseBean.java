package com.ratection.module.whBase.bean;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

public class WhBaseBean {
    private Integer id;

    //名称
    private String name;

    //当前当量
    private BigDecimal currentEquivalent;

    //剩余当量
    private BigDecimal surplusEquivalent;

    //总当量
    private BigDecimal totalEquivalent;

    //上次更新时间
    private Date lastUpdateTime;

    //上次检查时间
    private Date lastInspectTime;

    //上次盘库时间
    private Date lastCheckTime;

    private BigDecimal threshold;

    private String deviceId;

    //上次更新时间
    private String lastUpdateTimeStr;

    //上次检查时间
    private String lastInspectTimeStr;

    //上次盘库时间
    private String lastCheckTimeStr;

    public WhBaseBean() {
        super();
    }

    public WhBaseBean(Integer id, String name, BigDecimal currentEquivalent, BigDecimal surplusEquivalent, BigDecimal totalEquivalent, Date lastUpdateTime, Date lastInspectTime, Date lastCheckTime, BigDecimal threshold, String deviceId) {

        super();
        this.id = id;
        this.name = name;
        this.currentEquivalent = currentEquivalent;
        this.surplusEquivalent = surplusEquivalent;
        this.totalEquivalent = totalEquivalent;
        this.lastUpdateTime = lastUpdateTime;
        this.lastInspectTime = lastInspectTime;
        this.lastCheckTime = lastCheckTime;
        this.threshold = threshold;
        this.deviceId = deviceId;
    }

    public void SetTimeStr(Date lastUpdateTime, Date lastInspectTime, Date lastCheckTime) {
        if(lastUpdateTime != null) {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String dateString = formatter.format(lastUpdateTime);
            setLastUpdateTimeStr(dateString);
        }

        if(lastInspectTime != null) {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String dateString = formatter.format(lastInspectTime);
            setLastInspectTime(lastInspectTime);
        }

        if(lastCheckTime != null) {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String dateString = formatter.format(lastCheckTime);
            setLastCheckTimeStr(dateString);
        }
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getCurrentEquivalent() {
        return currentEquivalent;
    }

    public void setCurrentEquivalent(BigDecimal currentEquivalent) {
        this.currentEquivalent = currentEquivalent;
    }

    public BigDecimal getSurplusEquivalent() {
        return surplusEquivalent;
    }

    public void setSurplusEquivalent(BigDecimal surplusEquivalent) {
        this.surplusEquivalent = surplusEquivalent;
    }

    public BigDecimal getTotalEquivalent() {
        return totalEquivalent;
    }

    public void setTotalEquivalent(BigDecimal totalEquivalent) {
        this.totalEquivalent = totalEquivalent;
    }

    public Date getLastUpdateTime() {


        return lastUpdateTime;
    }

    public void setLastUpdateTime(Date lastUpdateTime) {

        this.lastUpdateTime = lastUpdateTime;
    }

    public Date getLastInspectTime() {
        return lastInspectTime;
    }

    public void setLastInspectTime(Date lastInspectTime) {


        this.lastInspectTime = lastInspectTime;
    }

    public Date getLastCheckTime() {
        return lastCheckTime;
    }

    public void setLastCheckTime(Date lastCheckTime) {
        this.lastCheckTime = lastCheckTime;
    }

    public BigDecimal getThreshold() {
        return threshold;
    }

    public void setThreshold(BigDecimal threshold) {
        this.threshold = threshold;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getLastUpdateTimeStr() {
        return lastUpdateTimeStr;
    }

    public void setLastUpdateTimeStr(String lastUpdateTimeStr) {
        this.lastUpdateTimeStr = lastUpdateTimeStr;
    }

    public String getLastInspectTimeStr() {
        return lastInspectTimeStr;
    }

    public void setLastInspectTimeStr(String lastInspectTimeStr) {
        this.lastInspectTimeStr = lastInspectTimeStr;
    }

    public String getLastCheckTimeStr() {
        return lastCheckTimeStr;
    }

    public void setLastCheckTimeStr(String lastCheckTimeStr) {
        this.lastCheckTimeStr = lastCheckTimeStr;
    }

    @Override
    public String toString() {

        return "WhBaseBean{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", currentEquivalent=" + currentEquivalent +
                ", surplusEquivalent=" + surplusEquivalent +
                ", totalEquivalent=" + totalEquivalent +
                ", lastUpdateTime=" + lastUpdateTime +
                ", lastInspectTime=" + lastInspectTime +
                ", lastCheckTime=" + lastCheckTime +
                ", threshold=" + threshold +
                ", deviceId='" + deviceId + '\'' +
                ", lastUpdateTimeStr='" + lastUpdateTimeStr + '\'' +
                ", lastInspectTimeStr='" + lastInspectTimeStr + '\'' +
                ", lastCheckTimeStr='" + lastCheckTimeStr + '\'' +
                '}';
    }
}
