package com.ratection.module.whBase.service;

import com.ratection.common.mysql.model.*;
import com.ratection.common.service.MysqlBasicService;
import com.ratection.common.util.MysqlHqlUtil;
import com.ratection.module.whBase.bean.GoodRecordInfoBean;
import com.ratection.module.whBase.bean.WhGoodsInfoBean;
import com.ratection.module.whBase.bean.WhInOutRecordDetailBean;
import com.ratection.module.whInOutRecord.bean.WhInOutRecordBean;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 仓库service
 * @author
 * @version 1.0
 */
@Service
public class WhBaseService extends MysqlBasicService {
    public long getWarehouseCount() throws Exception {
        String hql = "select count(*) from " + WhBase.class.getName();
        return getTotalCountByHql(hql);
    }

    //unchecked
    public List<WhBase> getWarehouses() throws Exception{
        String goodsQueryHql = " from " + WhBase.class.getName();
        return gets(goodsQueryHql);
    }

    //unchecked
    public List<WhBase> getWarehouses(int cursor, int limit) throws Exception {
        Session session = sessionFacotry.getCurrentSession();
        List<WhBase> list = null;
        try {
            session.beginTransaction();
            Criteria c = session.createCriteria(WhBase.class);
            c.setFirstResult(cursor);
            c.setMaxResults(limit);
            c.addOrder(Order.desc("id"));
            list = c.list();
            session.getTransaction().commit();
        } catch (Exception e) {
            session.getTransaction().rollback();
            e.printStackTrace();
        }
        return list;
    }

    //unchecked
    public void addWarehouse(WhBase warehouse) throws Exception {
        add(warehouse);
    }

    //unchecked
    public void updateWarehouse(WhBase warehouse) {
        update(MysqlHqlUtil.getSingleUpdateHql(warehouse, "id"));
    }

    //unchecked
    public void deleteWarehouse(WhBase warehouse) {
        delete(warehouse);
    }

    //unchecked
    public boolean isWarehouseExists(String name) {
        return isValueExists(WhBase.class.getName(), "name", name);
    }

    //unchecked
    public WhBase getWarehouseByID(Integer warehouseID)throws Exception{
        String hql = " from "+ WhBase.class.getName() + " where id =" + warehouseID + " ";
        return get(hql);
    }

    public List<WhBase> getWarehouseByName(String name)throws Exception{
        String hql = " from "+ WhBase.class.getName() + " where name = '" + name + "' ";
        return gets(hql);
    }

    //获取仓库物品存放信息
    public List<WhGoodsInfoBean> getWarehouseGoodsByWid(Integer warehouseID)throws Exception{
        String hql = "select  new com.ratection.module.whBase.bean.WhGoodsInfoBean(whg.warehouseId, gb.id, gb.name, gb.unit, whg.num,gb.equivalent, whg.updateTime,  whg.position) " +
                " from "+ WhGoods.class.getName() + " whg , " + GoodsBase.class.getName()  +
                " gb where gb.id =  whg.goodsId and whg.warehouseId = " + warehouseID;
        return gets(hql);
    }

//    public List<WhGoodsInfoBean> getWarehouseGoodsByWid(Integer warehouseID)throws Exception{
//        String hql = "select  whg.warehouseId as whId, gb.id as goodId, gb.name as goodName, whg.num as num, gb.unit as unit,whg.equivalent as equivalent," +
//                "whg.position as position, whg.updateTime as updateTime from "+ WhGoods.class.getName() + " whg , " + GoodsBase.class.getName()  +
//                " gb where gb.id =  whg.goodsId and whg.warehouseId = " + warehouseID;
//        return gets(hql);
//    }

    //获取仓库物品种类
    public List<GoodsBase> getWarehouseGoodsType(Integer warehouseID)throws Exception{
        String hql = "from " + GoodsBase.class.getName() + " where id  in ( SELECT goodId from "
                + WhGoodsBase.class.getName()  + " where warehouseId =" + warehouseID + ")";
        return gets(hql);
    }

    //获取仓库物品存放上限
    public List<GoodsBase> getWarehouseGoodsAlarm(Integer warehouseID)throws Exception{
        String hql = "from " + GoodsBase.class.getName() + " where id  in ( SELECT goodId from "
                + WhGoodsBase.class.getName()  + " where warehouseId =" + warehouseID + ")";
        return gets(hql);
    }

    //获取仓库物品出入库记录
    public List<WhInOutRecordBean> getWarehouseGoodsRecord(Integer warehouseID)throws Exception{
        //SELECT * from t_warehouse_in_out_record t1, t_warehouse_in_out_record_detail t2, t_goods_base t3, hj_user t4 where t1.record_id = t2.id and t1.goods_id = t3.id and t2.operator = t4.id
//        String hql = "select  new com.ratection.module.whInOutRecord.bean.WhInOutRecordBean(ior.id, ior.warehouseId, ior.goodsId, gb.name, gb.unit, iord.num, iord.equivalent, " +
//                "iord.type, user.id, user.name, iord.updateTime, whg.position, iord.remark, ir.batch) " + " from "
//                + WhInOutRecord.class.getName() + " ior , "
//                + WhInOutRecordDetail.class.getName()  + " iord , "
//                + GoodsBase.class.getName() +  " gb , "
//                + User.class.getName() + " user , "
//                + WhGoods.class.getName() +
//                " whg where  ior.recordId = iord.id and ior.goodsId = gb.id and iord.operator = user.id and ior.warehouseId = whg.warehouseId and ior.goodsId = whg.goodsId and ior.warehouseId = " + warehouseID;
        String hql = "select  new com.ratection.module.whInOutRecord.bean.WhInOutRecordBean(ior.id, ior.warehouseId, ior.goodsId, gb.name, gb.unit, iord.num, iord.equivalent, " +
                "iord.type, user.id, user.name, iord.updateTime, whg.position, iord.remark, ior.batch ) " + " from "
                + WhInOutRecord.class.getName() + " ior , "
                + WhInOutRecordDetail.class.getName()  + " iord , "
                + GoodsBase.class.getName() +  " gb , "
                + User.class.getName() + " user , "
                + WhGoods.class.getName() + " whg "
                + " where ior.warehouseId = " + warehouseID
                + " and ior.recordId = iord.id and ior.goodsId = gb.id and iord.operator = user.id and ior.warehouseId = whg.warehouseId and ior.goodsId = whg.goodsId";
        return gets(hql);
    }

    //获取仓库物品出入库记录
    public List<WhInOutRecordBean> getWarehouseGoodsRecord(Integer warehouseID, String type)throws Exception{
        //SELECT * from t_warehouse_in_out_record t1, t_warehouse_in_out_record_detail t2, t_goods_base t3, hj_user t4 where t1.record_id = t2.id and t1.goods_id = t3.id and t2.operator = t4.id
        String hql = "select  new com.ratection.module.whInOutRecord.bean.WhInOutRecordBean(ior.id, ior.warehouseId, ior.goodsId, gb.name, gb.unit, iord.num, iord.equivalent, " +
                "iord.type, user.id, user.name, iord.updateTime, whg.position, iord.remark, ior.batch ) " + " from "
                + WhInOutRecord.class.getName() + " ior , "
                + WhInOutRecordDetail.class.getName()  + " iord , "
                + GoodsBase.class.getName() +  " gb , "
                + User.class.getName() + " user , "
                + WhGoods.class.getName() + " whg "
                + " where ior.warehouseId = " + warehouseID + " and iord.type = " + type
                + " and ior.recordId = iord.id and ior.goodsId = gb.id and iord.operator = user.id and ior.warehouseId = whg.warehouseId and ior.goodsId = whg.goodsId";
        return gets(hql);
    }

//    public List<WhGoodsInfoBean> getWarehouseGoodsAlarmByWid(Integer warehouseID)throws Exception{
//        String hql = "select  whg.warehouseId as whId, gb.id as goodId, gb.name as goodName, whg.num as num, gb.unit as unit,whg.equivalent as equivalent," +
//                "whg.position as position, whg.updateTime as updateTime from "+ WhGoods.class.getName() + " whg , " + GoodsBase.class.getName()  +
//                " gb where gb.id =  whg.goodsId and whg.warehouseId = " + warehouseID;
//        return gets(hql);
//    }

    //获取最近仓库物品出入库记录
    public List<WhInOutRecordDetailBean> getWarehouseNearRecord(Integer uid, Integer limit)throws Exception{

//        SELECT  u.`name`, wh.`name`, rd.type, gb.`name`, gb.unit, rd.num, rd.updateTime from
//        t_warehouse_in_out_record_detail as rd,
//                t_warehouse_in_out_record as r ,
//        hj_user as u,
//                t_goods_base as gb,
//        t_warehouse_base as wh
//        WHERE rd.id = r.record_id and rd.operator = u.id and r.goods_id = gb.id and wh.id = r.warehouse_id
//        and u.id = 1
//        ORDER BY rd.updateTime DESC limit 0,10

        //, wh.name, iord.type, gb.name, gb.unit, iord.num, iord.updateTime
        String hql = "select  new com.ratection.module.whBase.bean.WhInOutRecordDetailBean(u.name, wh.name, iord.type, gb.name, gb.unit, iord.num, iord.updateTime) " + " from "
         //String hql = " from "
                + WhInOutRecordDetail.class.getName() + " iord , "
                + WhInOutRecord.class.getName()  + " ior ,"
                + User.class.getName() + " u ,"
                + GoodsBase.class.getName() +  " gb , "
                + WhBase.class.getName() + " wh "
                + " where iord.operator = " + uid
                + " and iord.id = ior.recordId and iord.operator = u.id and ior.goodsId = gb.id and wh.id = ior.warehouseId"
                + " order by iord.updateTime";

        return gets(hql,0, limit);
    }

    public List<GoodRecordInfoBean> getGoodRecordInfo(String batch)throws Exception {

        String hql = "select  new com.ratection.module.whBase.bean.GoodRecordInfoBean(ior.id, ior.warehouseId, wh.name, ior.goodsId, gb.name, gb.unit, iord.num, iord.equivalent, " +
                "iord.type, user.id, user.name, iord.updateTime, whg.position, iord.remark, ior.batch, iord.origination, iord.destination) " + " from "
                + WhInOutRecord.class.getName() + " ior , "
                + WhInOutRecordDetail.class.getName()  + " iord , "
                + GoodsBase.class.getName() +  " gb , "
                + User.class.getName() + " user , "
                + WhGoods.class.getName() + " whg,"
                + WhBase.class.getName() + " wh "
                + " where ior.batch = " + batch
                + " and ior.recordId = iord.id and ior.goodsId = gb.id and iord.operator = user.id and ior.warehouseId = whg.warehouseId and ior.goodsId = whg.goodsId and wh.id = ior.warehouseId"
                + " order by iord.updateTime asc";
        return gets(hql);
    }
}
