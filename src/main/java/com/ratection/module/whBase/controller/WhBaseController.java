package com.ratection.module.whBase.controller;

import com.ratection.common.bean.BasicPageResultBean;
import com.ratection.common.bean.SingleResultBean;
import com.ratection.common.exception.ErrorCode;
import com.ratection.common.exception.ErrorCodeException;
import com.ratection.common.mysql.model.*;
import com.ratection.module.goodsBase.service.GoodsBaseService;
import com.ratection.module.user.service.UserService;
import com.ratection.module.whBase.bean.GoodRecordInfoBean;
import com.ratection.module.whBase.bean.WhBaseBean;
import com.ratection.module.whBase.bean.WhGoodsInfoBean;
import com.ratection.module.whBase.bean.WhInOutRecordDetailBean;
import com.ratection.module.whBase.service.WhBaseService;
import com.ratection.module.whGoods.service.WhGoodsService;
import com.ratection.module.whGoodsBase.service.WhGoodsBaseService;
import com.ratection.module.whGoodsDetail.service.WhGoodsDetailService;
import com.ratection.module.whInOutRecord.bean.WhInOutRecordBean;
import com.ratection.module.whInOutRecord.service.WhInOutRecordService;
import com.ratection.module.whInOutRecordDetail.service.WhInOutRecordDetailService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;

@Controller
public class WhBaseController {
    @Autowired
    WhBaseService whBaseService;

    @Autowired
    WhGoodsService whGoodsService;

    @Autowired
    GoodsBaseService goodsBaseService;

    @Autowired
    WhGoodsDetailService whGoodsDetailService;

    @Autowired
    UserService userService;

    @Autowired
    WhGoodsBaseService whGoodsBaseService;

    @Autowired
    WhInOutRecordService whInOutRecordService;

    @Autowired
    WhInOutRecordDetailService whInOutRecordDetailService;

    private static Logger logger = LoggerFactory.getLogger(WhBaseController.class);

    @RequestMapping(value = "/warehouse/info", method = RequestMethod.GET)
    public
    @ResponseBody
    Object whInfo(
            HttpServletRequest request) throws Exception {
        List<WhBase> list = whBaseService.getWarehouses();
        return new BasicPageResultBean((long)list.size(), 0, list.size(), list);
    }

    @RequestMapping(value = "/warehouse/id", method = RequestMethod.GET)
    public
    @ResponseBody
    Object whInfoByID(
            HttpServletRequest request,
            @RequestParam(required = false) Integer whID) throws Exception {
        WhBase warehouse = whBaseService.getWarehouseByID(whID);

        SingleResultBean orb = new SingleResultBean(warehouse);
        return orb;
    }

    @RequestMapping(value = "/warehouse/name", method = RequestMethod.GET)
    public
    @ResponseBody
    Object whInfoByName(
            HttpServletRequest request,
            @RequestParam(required = false) String whName) throws Exception {
        List<WhBase> list = whBaseService.getWarehouseByName(whName);
        return new BasicPageResultBean((long)list.size(), 0, list.size(), list);
    }

    @RequestMapping(value = "/warehouse/add",method = RequestMethod.POST)
    public @ResponseBody Object add(@RequestBody WhBaseBean warehouseRequest) throws Exception{
        WhBase warehouse = new WhBase();
        warehouse.setName(warehouseRequest.getName());
        warehouse.setCurrentEquivalent(warehouseRequest.getCurrentEquivalent());
        warehouse.setSurplusEquivalent(warehouseRequest.getSurplusEquivalent());
        warehouse.setTotalEquivalent(warehouseRequest.getTotalEquivalent());
        warehouse.setLastUpdateTime(warehouseRequest.getLastUpdateTime());
        warehouse.setLastCheckTime(warehouseRequest.getLastCheckTime());
        warehouse.setLastInspectTime(warehouseRequest.getLastInspectTime());
        warehouse.setThreshold(warehouseRequest.getThreshold());
        warehouse.setDeviceId(warehouseRequest.getDeviceId());

        whBaseService.add(warehouse);
        SingleResultBean orb = new SingleResultBean(warehouse);
        return orb;
    }

    @RequestMapping(value = "/warehouse/update",method = RequestMethod.POST)
    public @ResponseBody Object update(@RequestBody WhBaseBean warehouseRequest){
        WhBase warehouse = whBaseService.getById("WhBase",warehouseRequest.getId());
        warehouse.setName(warehouseRequest.getName());
        warehouse.setCurrentEquivalent(warehouseRequest.getCurrentEquivalent());
        warehouse.setSurplusEquivalent(warehouseRequest.getSurplusEquivalent());
        warehouse.setTotalEquivalent(warehouseRequest.getTotalEquivalent());
        warehouse.setLastUpdateTime(warehouseRequest.getLastUpdateTime());
        warehouse.setLastCheckTime(warehouseRequest.getLastCheckTime());
        warehouse.setLastInspectTime(warehouseRequest.getLastInspectTime());
        warehouse.setThreshold(warehouseRequest.getThreshold());
        warehouse.setDeviceId(warehouseRequest.getDeviceId());

        whBaseService.updateWarehouse(warehouse);
        SingleResultBean orb = new SingleResultBean(warehouse);
        return orb;
    }

    @RequestMapping(value = "/warehouse/delete",method = RequestMethod.POST)
    public @ResponseBody Object delete(@RequestBody WhBaseBean warehouseRequest){
        WhBase warehouse = new WhBase();
        warehouse.setId(warehouseRequest.getId());

        whBaseService.delete(warehouse);
        SingleResultBean orb = new SingleResultBean(warehouse);
        return orb;
    }

    @RequestMapping(value = "/warehouse/getGoods", method = RequestMethod.GET)
    public
    @ResponseBody
    Object whGetGooodInfoByWid(
            HttpServletRequest request,
            @RequestParam(required = true) Integer wid) throws Exception {
        List<WhGoodsInfoBean> list = whBaseService.getWarehouseGoodsByWid(wid);
        return new BasicPageResultBean((long)list.size(), 0, list.size(), list);
    }

    @RequestMapping(value = "/warehouse/getGoodsType", method = RequestMethod.GET)
    public
    @ResponseBody
    Object whGetGooodsType(
            HttpServletRequest request,
            @RequestParam(required = true) Integer wid) throws Exception {
        List<GoodsBase> list = whBaseService.getWarehouseGoodsType(wid);
        return new BasicPageResultBean((long)list.size(), 0, list.size(), list);
    }

    @RequestMapping(value = "/warehouse/getGoodThreshold", method = RequestMethod.GET)
    public
    @ResponseBody
    Object whGetGooodsType(
            HttpServletRequest request,
            @RequestParam(required = true) Integer wid,
            @RequestParam(required = true) Integer gid) throws Exception {

        double threshold = 0;
        SingleResultBean orb = new SingleResultBean(threshold);

//        GoodsBase gb = goodsBaseService.getGoodsByID(gid);
//
//        if(null == gb)
//            throw new ErrorCodeException(ErrorCode.ACCESS_DENIED);

        WhGoodsDetail goodsDetail = whGoodsDetailService.getWhGoodsDetail(wid, gid);

        if(null == goodsDetail) {
            orb.setState(false);
            orb.setError_code(ErrorCode.INVALID_GOOD.getErrorCode());
            orb.setErrorMsg(ErrorCode.INVALID_GOOD.getError());
        }
        else {
            //        threshold = gb.getEquivalent().multiply(goodsDetail.getNum()).doubleValue();
            threshold = goodsDetail.getEnquivalent().doubleValue();
            orb.setResult(threshold);
        }

        return orb;
    }

    @RequestMapping(value = "/warehouse/getRecord", method = RequestMethod.GET)
    public
    @ResponseBody
    Object getRecord(
            HttpServletRequest request,
            @RequestParam(required = true) Integer wid) throws Exception {

        List<WhInOutRecordBean> list = whBaseService.getWarehouseGoodsRecord(wid);
        return new BasicPageResultBean((long)list.size(), 0, list.size(), list);
    }

    @RequestMapping(value = "/warehouse/inOutRecord", method = RequestMethod.GET)
    public
    @ResponseBody
    Object getInOutRecord(
            HttpServletRequest request,
            @RequestParam(required = true) Integer wid,
            @RequestParam(required = true) String type) throws Exception {

        List<WhInOutRecordBean> list = whBaseService.getWarehouseGoodsRecord(wid, type);
        return new BasicPageResultBean((long)list.size(), 0, list.size(), list);
    }

    @RequestMapping(value = "/warehouse/goodInOut", method = RequestMethod.POST)
    public
    @ResponseBody
    Object whGoodInOut(
            HttpServletRequest request,
            @RequestBody WhInOutRecordBean inOutRecordBean) throws Exception {

        SingleResultBean orb = new SingleResultBean(inOutRecordBean);
        orb.setState(true);

        WhBase whBase = whBaseService.getWarehouseByID(inOutRecordBean.getWhId());

        //查找是否有该仓库
        if(whBase == null) {
            orb.setState(false);
            orb.setError_code(ErrorCode.INVALID_WAREHOUSE.getErrorCode());//仓库不对
            orb.setErrorMsg(ErrorCode.INVALID_WAREHOUSE.getError());
            return orb;
        }

        User user =  userService.getUser(inOutRecordBean.getOperator());

        //查找是否有该用户
        if(user == null) {
            orb.setState(false);
            orb.setError_code(ErrorCode.INVALID_USER.getErrorCode());//操作员未找到
            orb.setErrorMsg(ErrorCode.INVALID_USER.getError());
            return orb;
        }

        WhGoodsBase goodsBase =  whGoodsBaseService.getWhGoodsBase(inOutRecordBean.getWhId(), inOutRecordBean.getGoodId());

        //查找该仓库是否可以存放该货物
        if(goodsBase == null) {
            orb.setState(false);
            orb.setError_code(ErrorCode.INVALID_GOOD_ACCESS.getErrorCode());//仓库 货物不对
            orb.setErrorMsg(ErrorCode.INVALID_GOOD_ACCESS.getError());
            return orb;
        }

        WhGoodsDetail goodsDetail = whGoodsDetailService.getWhGoodsDetail(inOutRecordBean.getWhId(), inOutRecordBean.getGoodId());

        //查找该货物信息是否存在
        GoodsBase goodBase = goodsBaseService.getGoodsByID( inOutRecordBean.getGoodId());
        if(goodsBase == null) {
            orb.setState(false);
            orb.setError_code(ErrorCode.INVALID_GOOD.getErrorCode());//货物不对
            orb.setErrorMsg(ErrorCode.INVALID_GOOD.getError());
            return orb;
        }

        WhGoods goods = whGoodsService.getWarehouseGoods(inOutRecordBean.getWhId(), inOutRecordBean.getGoodId());
        //当前操作的当量
        double equivalent = inOutRecordBean.getNum().doubleValue() * goodBase.getEquivalent().doubleValue();

        //入库操作
        if(inOutRecordBean.getType().equals("1")){
            //当前操作的当量大于仓库剩余当量，错误
            if(equivalent > whBase.getSurplusEquivalent().doubleValue()) {
                orb.setState(false);
                orb.setError_code(ErrorCode.OUT_OF_WAREHOUSE.getErrorCode());//仓库库存不够
                orb.setErrorMsg(ErrorCode.OUT_OF_WAREHOUSE.getError());
                return orb;
            }

            if(goodsDetail != null) {
                //判断是否已经存放过该货物
                if(goods == null) {
                    //没有存放判断当前当量是否超过该货物规定存放总量
                    if (equivalent > goodsDetail.getEnquivalent().doubleValue()) {
                        orb.setState(false);
                        orb.setError_code(ErrorCode.OUT_OF_GOOD.getErrorCode());//允许存放的该货物超出上限
                        orb.setErrorMsg(ErrorCode.OUT_OF_GOOD.getError());
                        return orb;
                    }
                }
                else
                {
                    //已经存放过判断仓库总量是否超过该货物规定存放总量
                    if (equivalent +  goods.getEquivalent().doubleValue() > goodsDetail.getEnquivalent().doubleValue()) {
                        orb.setState(false);
                        orb.setError_code(ErrorCode.OUT_OF_GOOD.getErrorCode());//允许存放的该货物超出上限
                        orb.setErrorMsg(ErrorCode.OUT_OF_GOOD.getError());
                        return orb;
                    }
                }
            }
            //更新仓库信息，更新剩余总量，当前总量
            BigDecimal newCurrentEquivalent = whBase.getCurrentEquivalent().add(new BigDecimal(equivalent));
            BigDecimal newSurplusEquivalent = whBase.getSurplusEquivalent().subtract(new BigDecimal(equivalent));
            whBase.setCurrentEquivalent(newCurrentEquivalent);
            whBase.setSurplusEquivalent(newSurplusEquivalent);
            whBaseService.updateWarehouse(whBase);

            //没有库存,添加库存，保存批次，当量
            if(goods == null) {
                WhGoods addGooods = new WhGoods();
                addGooods.setUpdatePerson(user.getId());
                addGooods.setPosition(inOutRecordBean.getPosition());
                addGooods.setUpdateTime(inOutRecordBean.getUpdate_time());
                addGooods.setEquivalent(new BigDecimal(equivalent));
                addGooods.setNum(inOutRecordBean.getNum());
                addGooods.setWarehouseId(inOutRecordBean.getWhId());
                addGooods.setGoodsId(inOutRecordBean.getGoodId());

                whGoodsService.adWarehouseGoods(addGooods);
            }else   {
                //存在该货物更新数据
                BigDecimal newEquivalent = goods.getEquivalent().add(new BigDecimal(equivalent));
                BigDecimal newNum = goods.getNum().add(inOutRecordBean.getNum());

                goods.setEquivalent(newEquivalent);
                goods.setNum(newNum);
                whGoodsService.updateWarehouseGoods(goods);
            }

            //添加库存记录
            WhInOutRecordDetail recordDetail = new WhInOutRecordDetail(inOutRecordBean.getType(), user.getId(), user.getId(),
                    inOutRecordBean.getUpdate_time(), new BigDecimal(equivalent), inOutRecordBean.getNum(), user.getId(),
                    inOutRecordBean.getOrigination(), inOutRecordBean.getDestination(), inOutRecordBean.getRemark());

            whInOutRecordDetailService.addWhInOutRecordDetail(recordDetail);

            WhInOutRecord record = new WhInOutRecord(inOutRecordBean.getGoodId(), inOutRecordBean.getNum(),  new BigDecimal(equivalent), recordDetail.getId(),
                    inOutRecordBean.getWhId(), inOutRecordBean.getBatch());
            whInOutRecordService.addWhInOutRecor(record);
        }
        //出库
        else if(inOutRecordBean.getType().equals("2")){
            //没有库存,添加库存
            if(goods == null) {
                orb.setState(false);
                orb.setError_code(ErrorCode.NOT_FOUND_GOOD.getErrorCode());//没有库存
                orb.setErrorMsg(ErrorCode.NOT_FOUND_GOOD.getError());
            }else   { //更新
                if (equivalent > goods.getEquivalent().doubleValue()) {
                    orb.setState(false);
                    orb.setError_code(ErrorCode.GOOD_BALANCE_NOT_ENOUGH.getErrorCode());//允许出库的该货物超出上限
                    orb.setErrorMsg(ErrorCode.GOOD_BALANCE_NOT_ENOUGH.getError());
                    return orb;
                }

                //更新仓库信息
                BigDecimal newCurrentEquivalent = whBase.getCurrentEquivalent().subtract(new BigDecimal(equivalent));
                BigDecimal newSurplusEquivalent = whBase.getSurplusEquivalent().add(new BigDecimal(equivalent));
                whBase.setCurrentEquivalent(newCurrentEquivalent);
                whBase.setSurplusEquivalent(newSurplusEquivalent);
                whBaseService.updateWarehouse(whBase);

                BigDecimal newEquivalent = goods.getEquivalent().subtract(new BigDecimal(equivalent));
                BigDecimal newNum = goods.getNum().subtract(inOutRecordBean.getNum());

                goods.setEquivalent(newEquivalent);
                goods.setNum(newNum);
                whGoodsService.updateWarehouseGoods(goods);
            }

            //添加库存记录
            WhInOutRecordDetail recordDetail = new WhInOutRecordDetail(inOutRecordBean.getType(), user.getId(), user.getId(),
                    inOutRecordBean.getUpdate_time(), new BigDecimal(equivalent), inOutRecordBean.getNum(), user.getId(),
                    inOutRecordBean.getOrigination(), inOutRecordBean.getDestination(), inOutRecordBean.getRemark());

            whInOutRecordDetailService.addWhInOutRecordDetail(recordDetail);

            WhInOutRecord record = new WhInOutRecord(inOutRecordBean.getGoodId(), inOutRecordBean.getNum(),  new BigDecimal(equivalent), recordDetail.getId(),
                    inOutRecordBean.getWhId(), inOutRecordBean.getBatch());
            whInOutRecordService.addWhInOutRecor(record);
        }
        else {
            orb.setState(false);
            orb.setError_code(ErrorCode.REQUST_ERROR.getErrorCode());//类型错误
            orb.setErrorMsg(ErrorCode.REQUST_ERROR.getError());
        }

        return orb;
    }

    @RequestMapping(value = "/warehouse/refresh", method = RequestMethod.POST)
    public
    @ResponseBody
    Object whRefresh(
            HttpServletRequest request,
            @RequestParam(required = true) Integer wid) throws Exception {
        WhBase whb = whBaseService.getWarehouseByID(wid);

        if(whb == null)
            throw new ErrorCodeException(ErrorCode.RESOURCE_DOSENOT_EXISTS);

        BigDecimal currentEquivalent = new BigDecimal("0");

        List<WhGoodsInfoBean> GoodsInfoList = whBaseService.getWarehouseGoodsByWid(whb.getId());

        if(GoodsInfoList == null)
            throw new ErrorCodeException(ErrorCode.RESOURCE_DOSENOT_EXISTS);

        for( int i = 0 ; i < GoodsInfoList.size() ; i++) {
            WhGoodsInfoBean info = GoodsInfoList.get(i);

            BigDecimal euivalent = info.getEquivalent();
            BigDecimal num = info.getNum();
            BigDecimal sum = euivalent.multiply(num);

            currentEquivalent = currentEquivalent.add(sum);
        }

        BigDecimal totalEquivalent = whb.getTotalEquivalent();

        if(currentEquivalent.compareTo(totalEquivalent) > 0)
            throw new ErrorCodeException(ErrorCode.ACCESS_DENIED);

        BigDecimal surplusEquivalent = new BigDecimal(totalEquivalent.subtract(currentEquivalent).doubleValue());
        whb.setSurplusEquivalent(surplusEquivalent);
        whb.setCurrentEquivalent(currentEquivalent);

        whBaseService.updateWarehouse(whb);

        SingleResultBean orb = new SingleResultBean(whb);
        return orb;
    }

    @RequestMapping(value = "/warehouse/getNearRecord", method = RequestMethod.GET)
    public
    @ResponseBody
    Object WhNearRecord(
            HttpServletRequest request,
            @RequestParam(required = true) Integer uid,
            @RequestParam(required = true) Integer limit) throws Exception {

        List<WhInOutRecordDetailBean> list = whBaseService.getWarehouseNearRecord(uid, limit);
        return new BasicPageResultBean((long)list.size(), 0, list.size(), list);
    }

    @RequestMapping(value = "/warehouse/getWhBatchInfo", method = RequestMethod.GET)
    public
    @ResponseBody
    Object WhBatchInfo(
            HttpServletRequest request,
            @RequestParam(required = true) Integer wid) throws Exception {

        List<String> batchList = new LinkedList<>();

        List<WhInOutRecord> list = whInOutRecordService.getWhInOutRecordByWid(wid);

        if(list != null) {
            for( int i = 0 ; i < list.size() ; i++) {
                if(list.get(i) != null && list.get(i).getBatch() != null)
                    batchList.add(list.get(i).getBatch());
            }
        }

        return new BasicPageResultBean((long)batchList.size(), 0, batchList.size(), batchList);
    }

    @RequestMapping(value = "/warehouse/getGoodRecordInfo", method = RequestMethod.GET)
    public
    @ResponseBody
    Object GoodRecordInfo(
            HttpServletRequest request,
            @RequestParam(required = true) String batch) throws Exception {

        List<GoodRecordInfoBean> list = whBaseService.getGoodRecordInfo(batch);
        return new BasicPageResultBean((long)list.size(), 0, list.size(), list);
    }
}
