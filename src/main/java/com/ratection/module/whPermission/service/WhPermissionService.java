package com.ratection.module.whPermission.service;

import com.ratection.common.mysql.model.WhBase;
import com.ratection.common.mysql.model.WhPermission;
import com.ratection.common.service.MysqlBasicService;
import com.ratection.common.util.MysqlHqlUtil;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class WhPermissionService extends MysqlBasicService {

    public long getWhPermissionCount() throws Exception {
        String hql = "select count(*) from " + WhPermission.class.getName();
        return getTotalCountByHql(hql);
    }

    //unchecked
    public List<WhPermission> getWhPermission() throws Exception{
        String goodsQueryHql = " from " + WhPermission.class.getName();
        return gets(goodsQueryHql);
    }

    //unchecked
    public List<WhPermission> getWhPermission(int cursor, int limit) throws Exception {
        Session session = sessionFacotry.getCurrentSession();
        List<WhPermission> list = null;
        try {
            session.beginTransaction();
            Criteria c = session.createCriteria(WhPermission.class);
            c.setFirstResult(cursor);
            c.setMaxResults(limit);
            c.addOrder(Order.desc("id"));
            list = c.list();
            session.getTransaction().commit();
        } catch (Exception e) {
            session.getTransaction().rollback();
            e.printStackTrace();
        }
        return list;
    }

    //unchecked
    public void addWhGoodsDetail(WhPermission whgood) throws Exception {
        add(whgood);
    }

    //unchecked
    public void updateWhPermission(WhPermission whgood) {
        update(MysqlHqlUtil.getSingleUpdateHql(whgood, "id"));
    }

    //unchecked
    public void deleteWhPermission(WhPermission whgood) {
        delete(whgood);
    }

    //unchecked
    public WhPermission getWhPermissionByID(Integer id)throws Exception{
        String hql = " from "+ WhPermission.class.getName() + " where id = " + id + " ";
        return get(hql);
    }

    //unchecked
    public List<WhPermission> getWhPermissionByWid(Integer wid)throws Exception{
        String hql = " from "+ WhPermission.class.getName() + " where warehouseId = " + wid + " ";
        return gets(hql);
    }

    //unchecked
    public List<WhPermission> getWhPermissionByUid(Integer uid)throws Exception{
        String hql = " from "+ WhPermission.class.getName() + " where userId = " + uid + " ";
        return gets(hql);
    }

    public List<WhPermission> getWhPermission(Integer uid, Integer wid)throws Exception{
        String hql = " from "+ WhPermission.class.getName() + " where warehouseId = " + uid + " and "+ " userId = " + wid + "";
        return gets(hql);
    }

    public List<WhBase> getUserWarehouse(int uid) throws Exception {
        //String hql = " SELECT * from t_warehouse_base where id in ( SELECT warehouse_id from t_jcqx_warehouse WHERE user_id = 1)";

        String hql = "from " + WhBase.class.getName() + " where id  in ( SELECT id from "
                + WhPermission.class.getName()  + " where userId =" + uid + ")";
        return gets(hql);
    }
}
