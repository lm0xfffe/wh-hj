package com.ratection.module.whInOutRecord.service;

import com.ratection.common.mysql.model.WhInOutRecord;
import com.ratection.common.service.MysqlBasicService;
import com.ratection.common.util.MysqlHqlUtil;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class WhInOutRecordService extends MysqlBasicService {
    //unchecked
    public void addWhInOutRecor(WhInOutRecord record) throws Exception {
        add(record);
    }

    //unchecked
    public void updateWhInOutRecord(WhInOutRecord record) {
        update(MysqlHqlUtil.getSingleUpdateHql(record, "id"));
    }

    //unchecked
    public void deleteWhInOutRecord(WhInOutRecord record) {
        delete(record);
    }

    //unchecked
    public List<WhInOutRecord> getWhInOutRecordByWid(Integer Wid)throws Exception{
        String hql = " from "+ WhInOutRecord.class.getName() + " where warehouse_id = " + Wid + " ";
        return gets(hql);
    }

    public List<WhInOutRecord> getWhInOutRecord(Integer Wid, Integer goodID)throws Exception{
        String hql = " from "+ WhInOutRecord.class.getName() + " where warehouseId = " + Wid + " and "+ " goodsId = " + goodID + "";
        return gets(hql);
    }
}
