package com.ratection.module.user.service;

import com.ratection.common.mysql.model.User;
import com.ratection.common.mysql.model.WhBase;
import com.ratection.common.mysql.model.WhPermission;
import com.ratection.common.service.MysqlBasicService;
import com.ratection.common.util.MysqlHqlUtil;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService extends MysqlBasicService {
    public void addUser(User user) throws Exception {
        add(user);
    }

    @SuppressWarnings("unchecked")
    public List<User> getUsers(int cursor, int limit) throws Exception {
        Session session = sessionFacotry.getCurrentSession();
        List<User> list = null;
        try {
            session.beginTransaction();
            Criteria c = session.createCriteria(User.class);
            c.setFirstResult(cursor);
            c.setMaxResults(limit);
            c.addOrder(Order.desc("id"));
            list = c.list();
            session.getTransaction().commit();
        } catch (Exception e) {
            session.getTransaction().rollback();
            e.printStackTrace();
        }
        return list;
    }

    public long getUsersCount() throws Exception {
        String hql = "select count(*) from " + User.class.getName();
        return getTotalCountByHql(hql);
    }

    public void updateUser(User user) {
        Session session = sessionFacotry.getCurrentSession();
        try {
            session.beginTransaction();
            String hql = MysqlHqlUtil.getSingleUpdateHql(user, "id");
            session.createQuery(hql).executeUpdate();
            session.getTransaction().commit();
        } catch (Exception e) {
            session.getTransaction().rollback();
            e.printStackTrace();
        }
    }

    public User getUser(Integer id) {
        Session session = sessionFacotry.getCurrentSession();
        User mu = null;
        try {
            session.beginTransaction();
            Criteria c = session.createCriteria(User.class);
            c.add(Restrictions.eq("id", id));
            Object o = c.uniqueResult();
            if (o != null) {
                mu = (User) o;
            }
            session.getTransaction().commit();
        } catch (Exception e) {
            session.getTransaction().rollback();
            e.printStackTrace();
        }
        return mu;
    }

    // xxx
    public User getUserbyPhone(String phone) {
        return getUserbyField("phone", phone);
    }

    public User getUserbyEmail(String email) {
        return getUserbyField("email", email);
    }

    public User getUserbyField(String key, String value) {
        Session session = sessionFacotry.getCurrentSession();
        User mu = null;
        try {
            session.beginTransaction();
            Criteria c = session.createCriteria(User.class);
            c.add(Restrictions.eq(key, value));
            Object o = c.uniqueResult();
            if (o != null) {
                mu = (User) o;
            }
            session.getTransaction().commit();
        } catch (Exception e) {
            session.getTransaction().rollback();
            e.printStackTrace();
        }
        return mu;
    }

    public boolean isPhoneExists(String phone) {
        return isValueExists(User.class.getName(), "phone", phone);
    }

    public void updateUserPassword(String phone, String password) {
        String hql = "update " + User.class.getName() + " m set m.password='"
                + password + "' where " + "m.phone='" + phone + "'";
        update(hql);
    }

    public void updateUserType(Integer userId, int type) {
        String hql = "update " + User.class.getName() + " m set m.type="
                + type + " where " + "m.id=" + userId;
        update(hql);
    }

    public void updateUserTypeAndName(Integer userId, int type, String name) {
        String hql = "update " + User.class.getName() + " m set m.type="
                + type + ",m.name='" + name + "' where " + "m.id=" + userId;
        update(hql);
    }

    public void updateUserPasswordByEmail(String email, String password) {
        String hql = "update " + User.class.getName() + " m set m.password='"
                + password + "' where " + "m.email='" + email + "'";
        update(hql);
    }

    public boolean validatePhoneAndPassword(String phone, String password) {
        Session session = sessionFacotry.getCurrentSession();
        Long count = 0l;
        try {
            session.beginTransaction();
            String hql = "select count(*) from " + User.class.getName()
                    + " c where c.phone='" + phone + "'"
                    + " and c.password='" + password + "'";
            Query query = session.createQuery(hql);
            count = (Long) query.uniqueResult();
            session.getTransaction().commit();
        } catch (Exception e) {
            session.getTransaction().rollback();
            e.printStackTrace();
        }
        return count > 0 ? true : false;
    }
}
