package com.ratection.module.user.controller;

import java.io.IOException;
import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import com.ratection.common.mysql.model.WhBase;
import com.ratection.common.mysql.model.WhPermission;
import com.ratection.module.whBase.service.WhBaseService;
import com.ratection.module.whPermission.service.WhPermissionService;
import org.apache.commons.lang.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ratection.common.bean.BasicPageResultBean;
import com.ratection.common.bean.SingleResultBean;
import com.ratection.common.exception.Error;
import com.ratection.common.exception.ErrorCode;
import com.ratection.common.exception.ErrorCodeException;
import com.ratection.common.mysql.model.Token;
import com.ratection.common.mysql.model.User;
import com.ratection.common.util.DateUtils;
import com.ratection.common.util.HttpRequestUtils;
import com.ratection.common.util.LocationUtil;
import com.ratection.common.util.PageUtil;
import com.ratection.common.util.ValueUtil;
import com.ratection.module.auth.AccessTokenGenerator;
import com.ratection.module.auth.AuthValidater;
import com.ratection.module.feedback.bean.FeedbackBean;
import com.ratection.module.feedback.service.FeedbackService;
import com.ratection.module.file.QiniuService;
import com.ratection.module.frequestion.service.FreQueService;
import com.ratection.module.push.service.PushService;
import com.ratection.module.sms.SmsHandler;
import com.ratection.module.user.bean.PhoneLoginBean;
import com.ratection.module.user.bean.RegisterResultBean;
import com.ratection.module.user.bean.UserCacheBean;
import com.ratection.module.user.bean.UserUpdateBean;
import com.ratection.module.user.captcha.CaptchaService;
import com.ratection.module.user.facade.TokenFacade;
import com.ratection.module.user.facade.UserFacade;

/**
 * @author franklin.xkk
 */
@Controller
public class Usercontroller {
    @Autowired
    ObjectMapper mapper;
    @Autowired
    TokenFacade tokenFacade;
    @Autowired
    UserFacade userFacade;
    @Autowired
    AuthValidater authValidater;
    @Autowired
    CaptchaService captchaService;
    @Autowired
    QiniuService qiniuService;
    @Autowired
    FeedbackService feedbackService;
    @Autowired
    PushService pushService;
    @Autowired
    FreQueService freQueService;

    @Autowired
    WhPermissionService whPermissionService;

    @Autowired
    WhBaseService whBaseService;

    private static Logger logger = LoggerFactory.getLogger(Usercontroller.class);

    @RequestMapping(value = "/user/login", method = RequestMethod.POST)
    public
    @ResponseBody
    Object phoneLogin(
            HttpServletRequest request,
            @Valid @RequestBody PhoneLoginBean loginBean
    ) throws Exception {
        String captcha = null;
        String city = null;
        String province = null;
        String district = null;
        captcha = captchaService.getCaptcha(loginBean.getPhone());
        logger.info("captcha:{},login-info:{}", captcha, mapper.writeValueAsString(loginBean));
		if(loginBean.getCaptcha().equals("1111") && loginBean.getPhone().equals("15210580419")){

		}else{
//			if(loginBean.getCaptcha() == null || !captcha.equals(loginBean.getCaptcha())){
            if(false){
				throw new ErrorCodeException(ErrorCode.VARIFICATION_CODE_ERROR);
			}else if(!authValidater.validateClient(loginBean.getClientId())){
	    		throw new ErrorCodeException(ErrorCode.INVALID_CLIENT);
	    	}
		}
        if (loginBean.getLatitude() != null && loginBean.getLongitude() != null) {
        	loginBean.setLatitude(ValueUtil.formateDouble(loginBean.getLatitude(), 6));
        	loginBean.setLongitude(ValueUtil.formateDouble(loginBean.getLongitude(), 6));
            Map<String, String> map = LocationUtil.getLocation(loginBean.getLatitude().doubleValue(), loginBean.getLongitude().doubleValue());
            if (map != null && map.get("province") != null) {
                city = map.get("city");
                province = map.get("province");
                district = map.get("district");
            }
        }
        User user = userFacade.getUserbyPhone(loginBean.getPhone());
        if (user != null) {
            if (loginBean.getpType() != null) {
                user.setpType(loginBean.getpType());
                userFacade.updateUser(user);
            }
            Token token = null;
            UserCacheBean ucb = userFacade.getUser(user.getId());
            if (ucb.getToken() != null) {
                token = tokenFacade.getToken(ucb.getToken());
                token.setUpdateTime(DateUtils.getDate());
            } else {
                token = new Token();
                token.setClientId(loginBean.getClientId());
                token.setUpdateTime(DateUtils.getDate());
                token.setCreateTime(token.getUpdateTime());
                token.setUserId(user.getId());
                token.setName(user.getPhone());
                token.setIp(HttpRequestUtils.getIpAddr(request));
                token.setType(user.getType());
                token.setToken(AccessTokenGenerator.getInstance().getAccessToken());
                token.setExpirseIn(AccessTokenGenerator.TokenExpirseIn);
                tokenFacade.addToken(token);
            }
            if (loginBean.getLatitude() != null && loginBean.getLongitude() != null) {
                user.setLatitude(loginBean.getLatitude());
                user.setLongitude(loginBean.getLongitude());
                user.setProvince(province);
                user.setCity(city);
                user.setDistrict(district);
                userFacade.updateUser(user);
            }
            userFacade.updateUserToken(user.getId(), token.getToken());
            userFacade.login(user.getId());
            RegisterResultBean rrb = new RegisterResultBean(token.getToken(), token.getUserId(), loginBean.getPhone(), token.getType(),
                    user.getProvince(), user.getCity(), user.getDistrict());
            return new SingleResultBean(rrb);
        } else {
            user = new User(
                    loginBean.getPhone(),
                    loginBean.getPhone(),
                    loginBean.getpType(),
                    DateUtils.getDate());
            user.setType(0);
            if (loginBean.getLatitude() != null && loginBean.getLongitude() != null) {
                user.setLatitude(loginBean.getLatitude());
                user.setLongitude(loginBean.getLongitude());
                user.setProvince(province);
                user.setCity(city);
                user.setDistrict(district);
            }
            userFacade.addUser(user);

            Token token = new Token();
            token.setName(user.getPhone());
            token.setClientId(loginBean.getClientId());
            token.setUpdateTime(DateUtils.getDate());
            token.setCreateTime(token.getUpdateTime());
            token.setUserId(user.getId());
            token.setToken(AccessTokenGenerator.getInstance().getAccessToken());
            token.setType(user.getType());
            token.setIp(HttpRequestUtils.getIpAddr(request));
            token.setExpirseIn(AccessTokenGenerator.TokenExpirseIn);
            tokenFacade.addToken(token);
            userFacade.updateUserToken(user.getId(), token.getToken());
            userFacade.login(user.getId());
            logger.info("register user:{}, phone{},email{}", user.getId(), user.getPhone(), user.getEmail());
            RegisterResultBean rrb = new RegisterResultBean(token.getToken(), token.getUserId(), loginBean.getPhone(), token.getType(),
                    user.getProvince(), user.getCity(), user.getDistrict());
            return new SingleResultBean(rrb);
        }
    }

    @RequestMapping(value = "/user/logout", method = RequestMethod.GET)
    public
    @ResponseBody
    Object logout(
            HttpServletRequest request,
            @RequestParam String token) throws Exception {
        Token t = tokenFacade.getToken(token);
        if (t != null) {
            tokenFacade.deleteToken(token);
            userFacade.logout(t.getUserId());
        }
        return new SingleResultBean(null, true);
    }

    @RequestMapping(value = "/user/info", method = RequestMethod.GET)
    public
    @ResponseBody
    Object info(
            HttpServletRequest request) throws Exception {
        Token token = (Token) request.getAttribute("X-TOKEN");
        UserCacheBean ucb = userFacade.getUser(token.getUserId());
        return new SingleResultBean(ucb);
    }

    @RequestMapping(value = "/user/update", method = RequestMethod.POST)
    public
    @ResponseBody
    Object update(
            HttpServletRequest request,
            @Valid @RequestBody UserUpdateBean userUpdateBean
    ) throws Exception {
        Token token = (Token) request.getAttribute("X-TOKEN");
        User user = mapper.convertValue(userUpdateBean, User.class);
        user.setId(token.getUserId());
        userFacade.updateUser(user);
        return new SingleResultBean(null, true);
    }

    @RequestMapping(value = "/user/icon", method = RequestMethod.POST)
    public
    @ResponseBody
    Object userImg(
            HttpServletRequest request,
            @RequestParam MultipartFile[] file
    ) throws Exception {
        Token token = (Token) request.getAttribute("X-TOKEN");
        String url = null;
        try {
            if (file != null && file.length > 0) {
                url = qiniuService.uploadFile("user-" + UUID.randomUUID().toString(), file[0].getInputStream());
                User user = new User();
                user.setId(token.getUserId());
                user.setIcon(url);
                userFacade.updateUser(user);
            } else {
                throw new ErrorCodeException(ErrorCode.RESOURCE_DOSENOT_EXISTS);
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("upload user icon error, userid={}", token.getUserId());
            throw new ErrorCodeException(ErrorCode.RESOURCE_DOSENOT_EXISTS);
        }
        return new SingleResultBean(url);
    }


    @RequestMapping(value = "/user/feedback", method = RequestMethod.POST)
    public
    @ResponseBody
    Object addfeedback(
            HttpServletRequest request,
            @Valid @RequestBody FeedbackBean fb
    ) throws Exception {
        Token token = (Token) request.getAttribute("X-TOKEN");
        feedbackService.addFeedback(token.getUserId(), fb.getPhone(), token.getName(), fb.getContent());
        return new SingleResultBean(null, true);
    }
    @RequestMapping(value = "/user/pushs", method = RequestMethod.GET)
    public
    @ResponseBody
    Object pushs(
    		@RequestParam(required = false, defaultValue = "0") Integer cursor,
			@RequestParam(required = false, defaultValue = "20") Integer limit,
			@RequestParam(required = false) Integer page
    ) throws Exception {
    	limit = PageUtil.checkLimit(limit, 100);
		cursor = PageUtil.checkCursorByPage(cursor, limit, page);
		Long total = pushService.getPushsCount();;
		List list = pushService.getPushs(cursor, limit);
		return new BasicPageResultBean(total, cursor, limit, list);
    }
    @RequestMapping(value = "/get-phone-captcha", method = RequestMethod.GET)
    public
    @ResponseBody
    Object getPhoneCode(
            @RequestParam(required = true) String phone,
            @RequestParam(required = true) String clientid,
            @RequestParam(required = false, defaultValue = "1") Integer type,
            @RequestParam(required = false, defaultValue = "1") Integer language
    ) {
        if (!authValidater.validateClient(clientid)) {
            throw new ErrorCodeException(ErrorCode.INVALID_CLIENT);
        } else {
            if (phone.matches(ValueUtil.PHONEREX)) {
                String captcha = RandomStringUtils.random(4, "1234567890");
                try {
                    captchaService.addCaptcha(phone, captcha, new Date());
                    SmsHandler.sendSMS(phone, SmsHandler.REGISTER_TEMPLATE, captcha);
                } catch (IOException e) {
                    logger.error("get phone captcha error at:{},msg;{}", phone, e.getMessage());
                }
            } else {
                throw new ErrorCodeException(ErrorCode.PHONE_NUMBER_ERROR);
            }

        }
        return new SingleResultBean(null, true);
    }

    @RequestMapping(value = "/auth/token", method = RequestMethod.GET)
    public @ResponseBody Object authToken(
    		HttpServletRequest request,
    		@RequestParam String token
    		) throws Exception {
    	Token t;
    	try {
    		t = tokenFacade.getToken(token);
    		if (t == null) {
    			return new ErrorCodeException(new Error(request.getRequestURI(), ErrorCode.EXPIRED_TOKEN));
    		}
    	} catch (Exception e) {
    		e.printStackTrace();
    		return new ErrorCodeException(new Error(request.getRequestURI(), ErrorCode.TOKEN_ERROR, token));
    	}
    	UserCacheBean u = userFacade.getUser(t.getUserId());
    	RegisterResultBean rrb = new RegisterResultBean(t.getToken(), t.getUserId(), u.getPhone(), u.getType());
    	return new SingleResultBean(rrb, true);
    }
    @RequestMapping(value = "/user/frequestions", method = RequestMethod.GET)
    public
    @ResponseBody
    Object frequestions( HttpServletRequest request
    ) throws Exception {
    	List list = freQueService.getFreQs();
        return new BasicPageResultBean((long)(list.size()), 0, (int)(list.size()), list);
    }
    @RequestMapping(value = "/image/upload", method = RequestMethod.POST)
    @ResponseBody
    public Map upload(@RequestParam CommonsMultipartFile imgFile, HttpServletRequest request) throws Exception{
        String url = null;
        try {
            if (imgFile != null) {
                url = qiniuService.uploadFile("admin-" + UUID.randomUUID().toString(), imgFile.getInputStream());
            } else {
                throw new ErrorCodeException(ErrorCode.RESOURCE_DOSENOT_EXISTS);
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new ErrorCodeException(ErrorCode.RESOURCE_DOSENOT_EXISTS);
        }
        Map map = new HashMap<>();
        map.put("error", 0);
        map.put("url", url);
        return map;
    }

    @RequestMapping(value = "/user/getWh", method = RequestMethod.GET)
    public
    @ResponseBody
    Object getWarehouse(
            HttpServletRequest request) throws Exception {
        Token token = (Token) request.getAttribute("X-TOKEN");
        Integer id = token.getUserId();
//        List<WhPermission> listPermission = whPermissionService.getWhPermissionByUid(id);
//
//        List list = new ArrayList();
//        if(null != listPermission ) {
//            for( int i = 0 ; i < listPermission.size() ; i++) {
//                WhBase whBase = whBaseService.getWarehouseByID(listPermission.get(i).getWarehouseId());
//
//                if(null != whBase)
//                    list.add(whBase);
//            }
//        }

        List<WhBase> list = whPermissionService.getUserWarehouse(id);
        return new BasicPageResultBean((long)list.size(), 0, list.size(), list);
    }
}
