package com.ratection.module.user.bean;

public class UserUpdateBean {
    private String nickname;
    private String icon;
    private String email;
    private String occupation;

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    @Override
    public String toString() {
        return "UserUpdateBean [nickname=" + nickname + ", icon=" + icon
                + ", email=" + email + ", occupation=" + occupation + "]";
    }

}
