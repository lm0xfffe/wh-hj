package com.ratection.module.user.bean;


public class RegisterResultBean {
    private String token;
    private Integer userId;
    private String phone;
    private Integer type;
    private String province;
    private String city;
    private String district;

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public RegisterResultBean(String token, Integer userId, String phone, Integer type) {
        super();
        this.token = token;
        this.userId = userId;
        this.phone = phone;
        this.type = type;
    }

    public RegisterResultBean(String token, Integer userId, String phone, Integer type, String province, String city, String district) {
        this.token = token;
        this.userId = userId;
        this.phone = phone;
        this.type = type;
        this.province = province;
        this.city = city;
        this.district = district;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Override
    public String toString() {
        return "RegisterResultBean{" +
                "token='" + token + '\'' +
                ", userId=" + userId +
                ", phone='" + phone + '\'' +
                ", type=" + type +
                ", province='" + province + '\'' +
                ", city='" + city + '\'' +
                ", district='" + district + '\'' +
                '}';
    }
}
