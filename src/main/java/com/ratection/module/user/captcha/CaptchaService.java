package com.ratection.module.user.captcha;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ratection.common.cache.CacheTableNames;
import com.ratection.common.cache.RedisService;
import com.ratection.module.user.captcha.bean.PhoneCaptcha;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * @author franklin.xkk
 */
@Component
public class CaptchaService extends RedisService {
    @Autowired
    ObjectMapper mapper;

    public String getCaptcha(String phone) {
        Object o = getValue(CacheTableNames.PREFIX_CAPTCHAP + phone);
        del(CacheTableNames.PREFIX_CAPTCHAP + phone);
        if (o == null) {
            return "-1";
        } else {
            try {
                PhoneCaptcha c = mapper.readValue(o.toString(), PhoneCaptcha.class);
                return c.getCaptcha();
            } catch (Exception e) {
                return "-1";
            }
        }
    }

    public void addCaptcha(String phone, String captcha, Date date) throws JsonProcessingException {
        PhoneCaptcha c = new PhoneCaptcha();
        c.setCaptcha(captcha);
        c.setPhone(phone);
        c.setTimestamp(date);
        addValue(CacheTableNames.PREFIX_CAPTCHAP + phone, mapper.writeValueAsString(c), 300L);
    }

    public String getEmailCaptcha(String email) {
        Object o = getValue(CacheTableNames.PREFIX_CAPTCHAE + email);
        del(CacheTableNames.PREFIX_CAPTCHAE + email);
        if (o == null) {
            return "-1";
        } else {
            try {
                return o.toString();
            } catch (Exception e) {
                return "-1";
            }
        }
    }

    public void addEmailCaptcha(String email, String captcha) throws JsonProcessingException {
        PhoneCaptcha c = new PhoneCaptcha();
        c.setCaptcha(captcha);
        c.setPhone(email);
        addValue(CacheTableNames.PREFIX_CAPTCHAE + email, captcha, 300L);
    }
}
