package com.ratection.module.device.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.commons.collections.MapUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ratection.common.bean.BasicPageResultBean;
import com.ratection.common.bean.SingleResultBean;
import com.ratection.common.exception.ErrorCode;
import com.ratection.common.exception.ErrorCodeException;
import com.ratection.common.mysql.model.CheckPointData;
import com.ratection.common.mysql.model.CheckPointStatisticsData;
import com.ratection.common.mysql.model.DeviceInfo;
import com.ratection.common.mysql.model.DistrictCheckPointData;
import com.ratection.common.mysql.model.Token;
import com.ratection.common.mysql.model.UserDevice;
import com.ratection.common.util.DateUtils;
import com.ratection.common.util.LocationUtil;
import com.ratection.common.util.MysqlHqlUtil;
import com.ratection.common.util.PageUtil;
import com.ratection.common.util.ValueUtil;
import com.ratection.module.auth.AuthValidater;
import com.ratection.module.device.bean.BaseCpDataBean;
import com.ratection.module.device.bean.CpdDataBean;
import com.ratection.module.device.bean.DeviceDataUploadBean;
import com.ratection.module.device.bean.MultiDeviceDataBean;
import com.ratection.module.device.service.DeviceService;
import com.ratection.module.user.facade.TokenFacade;
import com.ratection.module.user.facade.UserFacade;

@Controller
public class DeviceController {
    @Autowired
    ObjectMapper mapper;
    @Autowired
    TokenFacade tokenFacade;
    @Autowired
    UserFacade userFacade;
    @Autowired
    AuthValidater authValidater;
    @Autowired
    DeviceService deviceService;
    private static Logger logger = LoggerFactory.getLogger(DeviceController.class);
    @RequestMapping(value = "/data/mycpdatas", method = RequestMethod.GET)
    public
    @ResponseBody
    Object getMyCheckpointDatas(
            HttpServletRequest request,
            @RequestParam(required = false) String token,// 为空表示查询所有用户
            @RequestParam(required = false) Integer userid,
            @RequestParam(required = true) String date
    ) throws Exception {
        Token t = null;
        if(token != null){
        	t = tokenFacade.getToken(token);
        	if(t!= null && t.getUserId() != null){
        		userid = t.getUserId();
        	}
        }
       Date begindate = DateUtils.formatDate(date);
       Date enddate = DateUtils.getDate();
       List<CheckPointData> list = deviceService.getMyDeviceDatas(userid, begindate, enddate);
       return new BasicPageResultBean((long)list.size(), 0, list.size(), list);
    }
    @RequestMapping(value = "/data/cpdatas", method = RequestMethod.GET)
    public
    @ResponseBody
    Object getCheckpointDatas(
            HttpServletRequest request,
            @RequestParam(required = false) String province,
            @RequestParam(required = false) String city,
            @RequestParam(required = false) String district,
            @RequestParam(required = false) Double minlng,
            @RequestParam(required = false) Double minlat,
            @RequestParam(required = false) Double maxlng,
            @RequestParam(required = false) Double maxlat,
            @RequestParam(required = false,defaultValue="9.0") Double zoomlevel,
            @RequestParam(required = false) Integer envid,
            @RequestParam(required = false) String mac,
            @RequestParam(required = false) String token,// 为空表示查询所有用户
            @RequestParam(required = false) Integer userid,
            @RequestParam(required = false, defaultValue = "0") Integer cursor,
            @RequestParam(required = false, defaultValue = "1000000") Integer limit,
            @RequestParam(required = false) Integer page,
            @RequestParam(required = false, defaultValue = "1") Integer verbose
    ) throws Exception {
        Token t = null;
        if(token != null){
        	t = tokenFacade.getToken(token);
        	if(t!= null && t.getUserId() != null){
        		userid = t.getUserId();
        	}
        }
        logger.info("cp-query: minlng:{},minlat:{},maxlng:{},maxlat:{},userId:{},envid:{},zoom:{}", 
        		minlng, minlat, maxlng, maxlat, userid, envid, zoomlevel);
        limit = PageUtil.checkLimit(limit, null);
        cursor = PageUtil.checkCursorByPage(cursor, limit, page);
        if(zoomlevel != null && zoomlevel > 9.0d){
        	List<CheckPointData> list = deviceService.getDeviceDatas(userid, envid, mac, minlng, minlat, maxlng, maxlat, province, city, district, cursor, limit);
        	if(verbose < 1 && list != null && list.size() > 0){
        		List baseList = new ArrayList();
        		for(CheckPointData cpdata: list){
        			BaseCpDataBean bcpd = new BaseCpDataBean(cpdata.getId(),cpdata.getLongitude(),cpdata.getLatitude(),cpdata.getCheckTime(),cpdata.getCpValue());
        			baseList.add(bcpd);
        		}
        		Long total = deviceService.getDeviceDatasCount(userid, envid, mac, minlng, minlat, maxlng, maxlat, province, city, district);
            	return new BasicPageResultBean(total, cursor, limit, baseList);
        	}else{
        		Long total = deviceService.getDeviceDatasCount(userid, envid, mac, minlng, minlat, maxlng, maxlat, province, city, district);
            	return new BasicPageResultBean(total, cursor, limit, list);
        	}
        	
        }else{
        	Long total = deviceService.getDeviceDistrictDatasCount(userid, envid);
        	List<CheckPointData> list = deviceService.getDeviceDistrictDatas(userid, envid);
            return new BasicPageResultBean(total, cursor, limit, list);
        }
    }

    @RequestMapping(value = "/data/cpupload", method = RequestMethod.POST)
    public
    @ResponseBody
    Object deviceUpload(HttpServletRequest request,
            @Valid @RequestBody DeviceDataUploadBean data
    ) throws Exception {
        Token token = (Token) request.getAttribute("X-TOKEN");
        if(data.getMac() != null){
            logger.info("cpupload-data:{}",mapper.writeValueAsString(data));
            CheckPointData cpd = mapper.convertValue(data, CheckPointData.class);
            if(cpd.getProvince() == null){
                Map<String, String> map = LocationUtil.getLocation(data.getLatitude(), data.getLongitude());
                if (map != null && map.get("province") != null) {
                    cpd.setProvince(MapUtils.getString(map, "province"));
                    cpd.setCity(MapUtils.getString(map, "city"));
                    cpd.setDistrict(MapUtils.getString(map, "district"));
                }else{
                    cpd.setProvince("");
                    cpd.setCity("");
                    cpd.setDistrict("");
                }
            }
            cpd.setLatitude(ValueUtil.formateDouble(cpd.getLatitude(), 6));
            cpd.setLongitude(ValueUtil.formateDouble(cpd.getLongitude(), 6));
            cpd.setCpValue(ValueUtil.formateDouble(cpd.getCpValue(), 4));
            cpd.setCheckTime(DateUtils.getDate());
            cpd.setUserId(token.getUserId());
            cpd.setIsDelete(0);
            deviceService.createDeviceData(cpd);
            DistrictCheckPointData dcpd = mapper.convertValue(cpd, DistrictCheckPointData.class);
            deviceService.updateDistrictDeviceData(dcpd);
            UserDevice d = deviceService.getDeviceByMac(token.getUserId(), cpd.getMac());//:TODO cache
            if(d == null){
            	d = new UserDevice();
            	d.setCreateTime(cpd.getCheckTime());
            	d.setDeviceVersion(cpd.getDeviceVersion());
            	d.setMac(cpd.getMac());
            	d.setName(cpd.getDeviceName());
            	d.setUpdateTime(cpd.getCheckTime());
            	d.setUserId(token.getUserId());
            	deviceService.createDevice(d);
            }
            return new SingleResultBean(cpd);
        }else{
        	throw new ErrorCodeException(ErrorCode.MISS_REQUIRED_PARAMETER,"mac");
        }
    }
    
    @RequestMapping(value = "/data/multiupload", method = RequestMethod.POST)
    public
    @ResponseBody
    Object multiupload(HttpServletRequest request,
            @Valid @RequestBody List<MultiDeviceDataBean> datas
    ) throws Exception {
        Token token = (Token) request.getAttribute("X-TOKEN");
        List<CheckPointData> list = new ArrayList<>();
        if(datas != null && datas.size() > 0){
        	logger.info("multiupload-data:{}",mapper.writeValueAsString(datas));
        	for(MultiDeviceDataBean data: datas){
        		if(data != null){
        			if(data.getProvince() == null){
                        Map<String, String> map = LocationUtil.getLocation(data.getLatitude(), data.getLongitude());
                        if (map != null && map.get("province") != null) {
                        	data.setProvince(MapUtils.getString(map, "province"));
                        	data.setCity(MapUtils.getString(map, "city"));
                        	data.setDistrict(MapUtils.getString(map, "district"));
                        }else{
                        	data.setProvince("");
                        	data.setCity("");
                        	data.setDistrict("");
                        }
                    }
					CheckPointData cpd = new CheckPointData();
					cpd.setBusiness(data.getBusiness());
					cpd.setCheckTime(DateUtils.getDate(data.getCheckTime()));
					cpd.setCity(data.getCity());
					cpd.setContinueTime(data.getContinueTime());
					cpd.setCpValue(data.getCpValue());
					cpd.setDeviceVersion(data.getDeviceVersion());
					cpd.setDistrict(data.getDistrict());
					cpd.setGroupid(data.getGroupid());
					cpd.setEnvid(data.getEnvid());
					cpd.setEnvname(data.getEnvname());
					cpd.setFirmwareVersion(data.getFirmwareVersion());
					cpd.setLatitude(data.getLatitude());
					cpd.setLongitude(data.getLongitude());
					cpd.setMac(data.getMac());
					cpd.setManufacturerName(data.getManufacturerName());
					cpd.setModelNumber(data.getModelNumber());
					cpd.setProvince(data.getProvince());
					cpd.setSerialNumber(data.getSerialNumber());
					cpd.setStreet(data.getStreet());
					cpd.setTemp(data.getTemp());
					cpd.setType(data.getType());
					cpd.setUserId(token.getUserId());
					cpd.setIsDelete(0);
					list.add(cpd);
        	}
        }
        deviceService.multiCreateDeviceData(list);
      }
      return new SingleResultBean(list,true);
   }

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/data/hiscpdatas", method = RequestMethod.GET)
    public
    @ResponseBody
    Object getHisDatas(
            HttpServletRequest request,
            @RequestParam(required = true) String date,
            @RequestParam(required = true, defaultValue="1") Integer type
    ) throws Exception {
        Token token = (Token) request.getAttribute("X-TOKEN");
        Integer userId = token.getUserId();
        logger.info("cphistory-query: date:{},type:{},userId:{}", date, type, userId);
        Date begindate = null;
        Date enddate = null;
        if(type == 1){
        	begindate = DateUtils.getDateStartTime(DateUtils.formatDate(date));
        	enddate = DateUtils.getDateEndTime(DateUtils.formatDate(date));
        	List<CheckPointData> list = deviceService.getHisDeviceDatas(userId, begindate, enddate);
        	List<CpdDataBean> newListCpds = new ArrayList<>();
        	for(CheckPointData cpd : list){
        		CpdDataBean cdb = mapper.convertValue(cpd, CpdDataBean.class);
        		newListCpds.add(cdb);
            }
            Collections.sort(newListCpds);
            return new SingleResultBean(newListCpds);
        }else{
        	begindate = DateUtils.getMonthStartTime(DateUtils.formatDate(date));
        	enddate = DateUtils.getMonthEndTime(DateUtils.formatDate(date));
        	List<CheckPointData> list = deviceService.getHisDeviceDatas(userId, begindate, enddate);
        	CheckPointData[] newList = new CheckPointData[32];
            int[] count = new int[32];
            List<CpdDataBean> newListCpds = new ArrayList<>();
            for(CheckPointData cpd : list){
            	int day = DateUtils.getDay(cpd.getCheckTime());
            	double value = 0d;
            	if(newList[day] == null){
            		newList[day] = cpd;
            		value = cpd.getCpValue();
            		count[day] = count[day] +1;
            	}else{
            		value = newList[day].getCpValue() + cpd.getCpValue();
            		count[day] = count[day] +1;
            	}
            	newList[day] = cpd;
            	newList[day].setCpValue(value);
            }
            
            for(int i = 0; i < newList.length; i++){
            	if(newList[i] != null){
            		CpdDataBean cdb = mapper.convertValue(newList[i], CpdDataBean.class);
            		cdb.setCpValue(cdb.getCpValue()/count[i]);
            		newListCpds.add(cdb);
            	}
            }
            Collections.sort(newListCpds);
            return new SingleResultBean(newListCpds);
        }
    }
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/data/groupcpdatas", method = RequestMethod.GET)
    public
    @ResponseBody
    Object groupcpdatas(
            HttpServletRequest request,
            @RequestParam(required = true) List<String> groupids
    ) throws Exception {
        Token token = (Token) request.getAttribute("X-TOKEN");
        if(groupids != null){
        	List list = deviceService.getHisDeviceDatasByGroupid(MysqlHqlUtil.list2String2(groupids));
        	return new SingleResultBean(list);
        }else{
        	return new SingleResultBean(new ArrayList<>());
        }
        
        
    }
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/data/groups", method = RequestMethod.GET)
    public
    @ResponseBody
    Object groups(
            HttpServletRequest request,
            @RequestParam(required = true) String date,
            @RequestParam(required = true, defaultValue="1") Integer type
    ) throws Exception {
        Token token = (Token) request.getAttribute("X-TOKEN");
        Integer userId = token.getUserId();
        logger.info("cphistory-query: date:{},type:{},userId:{}", date, type, userId);
        Date begindate = null;
        Date enddate = null;
        List<String> groupids = new ArrayList<>();
        if(type == 1){//日
        	begindate = DateUtils.getDateStartTime(DateUtils.formatDate(date));
        	enddate = DateUtils.getDateEndTime(DateUtils.formatDate(date));
        }else{//月
        	begindate = DateUtils.getMonthStartTime(DateUtils.formatDate(date));
        	enddate = DateUtils.getMonthEndTime(DateUtils.formatDate(date));
        }
    	List<String> list = deviceService.getGroupDeviceDatas(userId, begindate, enddate);
    	return new SingleResultBean(list);
    }

    @RequestMapping(value = "/statistic/cpdatas", method = RequestMethod.GET)
    public
    @ResponseBody
    Object getStatisticDatas(HttpServletRequest request,
    		@RequestParam(required = true) String clientid) throws Exception {
    	if(!authValidater.validateClient(clientid)){
    		throw new ErrorCodeException(ErrorCode.INVALID_CLIENT);
    	}
        List<CheckPointStatisticsData> checkPointStatisticsDataList = deviceService.getDeviceStatisticsData();
        return new SingleResultBean(checkPointStatisticsDataList,true);
    }
    
    @RequestMapping(value = "/user/devices", method = RequestMethod.GET)
    public
    @ResponseBody
    Object devicelst( HttpServletRequest request
    ) throws Exception {
    	Token token = (Token) request.getAttribute("X-TOKEN");
        List<UserDevice> list = deviceService.getDevices(token.getUserId());
        long total = 0l;
        if(list != null){
        	total = (long)list.size();
        }
        return new BasicPageResultBean(total, 0, (int)total, list);
    }
    @RequestMapping(value = "/device/icons", method = RequestMethod.GET)
    public
    @ResponseBody
    Object icons( HttpServletRequest request
    ) throws Exception {
        List<DeviceInfo> list = deviceService.getDeviceIcons();
        return new SingleResultBean(list);
    }
}
