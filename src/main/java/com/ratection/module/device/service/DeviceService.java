package com.ratection.module.device.service;

import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.stereotype.Service;

import com.ratection.common.mysql.model.CheckPointData;
import com.ratection.common.mysql.model.CheckPointStatisticsData;
import com.ratection.common.mysql.model.DeviceInfo;
import com.ratection.common.mysql.model.DistrictCheckPointData;
import com.ratection.common.mysql.model.UserDevice;
import com.ratection.common.service.MysqlBasicService;
import com.ratection.common.util.DateUtils;
import com.ratection.common.util.MysqlHqlUtil;
import com.ratection.module.device.bean.BaseCpDataBean;

@Service
public class DeviceService extends MysqlBasicService {
	public void createDevice(UserDevice cpd) throws Exception {
        add(cpd);
    }
	public UserDevice getDeviceByMac(Integer userId,String mac)throws Exception{
		String hql = " from "+UserDevice.class.getName() + " where userId=" + userId + " and mac='" + mac + "'";
		return get(hql);
	}
	public void deleteDeviceByMac(Integer userId, String mac){
        String hql = "delete from " + UserDevice.class.getName() + " where userId=" + userId + " and mac='" + mac + "'";
        update(hql);
	}
	public List<UserDevice> getDevices(Integer userId)throws Exception{
		String hql = "from " +UserDevice.class.getName() + " where userId="+userId;
		return gets(hql);
	}
	public List<DeviceInfo> getDeviceIcons()throws Exception{
		String hql = "from " +DeviceInfo.class.getName();
		return gets(hql);
	}
	
    public void createDeviceData(CheckPointData cpd) throws Exception {
        add(cpd);
    }
/*    public void createDeviceDistrictData(DistrictCheckPointData cpd) throws Exception {
        add(cpd);
    }
    public boolean isDistrictDeviceDataExists(String district){
    	return isValueExists(DistrictCheckPointData.class.getName(), "district", district);
    }*/
    public void updateDistrictDeviceData(DistrictCheckPointData cpd){
    	try {
    		if(cpd != null && cpd.getDistrict() != null){
        		if(isValueExists(DistrictCheckPointData.class.getName(), "district", cpd.getDistrict())){
            		update(MysqlHqlUtil.getSingleUpdateHql(cpd, "district"));
            	}else{
            		add(cpd);
            	}
    		}
		} catch (Exception e) {
			e.printStackTrace();
			return;
		}
    }
    public void deleteDeviceData(Integer id) {
        String hql = "update " + CheckPointData.class.getName()
                + " m set m.isDelete=1 where id=" + id;
        update(hql);
    }

    public void deleteDeviceDatas(String ids) {
        String hql = "update " + CheckPointData.class.getName()
                + " m set m.isDelete=1 where id in(" + ids + ")";
        update(hql);
    }


    public List<CheckPointData> getDeviceDatas(Integer userId, Integer envid, String mac, 
    		Double minlng, Double minlat,Double maxlng, Double maxlat,
    		String province, String city, String district,
    		Integer cursor, Integer limit) throws Exception {
        String hql = "from " + CheckPointData.class.getName() + " where isDelete=0 ";
        if (userId != null) {
            hql += " and userId=" + userId;
        }
        if (envid != null) {
            hql += " and envid=" + envid;
        }
        if (province != null && !province.trim().equals("")) {
            hql += " and province='" + province + "' ";
        }
        if (city != null && !city.trim().equals("")) {
            hql += " and city='" + city + "' ";
        }
        if (district != null && !district.trim().equals("")) {
            hql += " and district='" + district + "' ";
        }
        if (mac != null) {
            hql += " and mac=" + mac;
        }
//        if (dis != null) {
//            double r = 6371.137;//地球半径米
//            double dlng = 2 * Math.asin(Math.sin(dis / (2 * r)) / Math.cos(Double.valueOf(latitude) * Math.PI / 180));
//            dlng = dlng * 180 / Math.PI;//角度转为弧度
//            double dlat = dis / r;
//            dlat = dlat * 180 / Math.PI;
//            double minlng = Double.valueOf(longitude) - dlat;
//            double minlat = Double.valueOf(latitude) - dlat;
//            double maxlng = Double.valueOf(longitude) + dlat;
//            double maxlat = Double.valueOf(latitude) + dlat;
        if(minlat != null && minlng != null && maxlat != null && maxlng != null){
        	hql += (" and longitude >=" + minlng + " and longitude <=" + maxlng + " and latitude>=" + minlat + " and latitude<=" + maxlat);
        }
            
//        }
        hql += " order by id desc ";
        return gets(hql, cursor, limit);
    }
   
    public long getDeviceDatasCount(Integer userId, Integer envid, String mac,
                                    Double minlng, Double minlat,Double maxlng, Double maxlat,
                                    String province, String city, String district) throws Exception {
        String hql = "select count(*) from " + CheckPointData.class.getName() + " where isDelete=0 ";
        if (userId != null) {
            hql += " and userId=" + userId;
        }
        if (envid != null) {
            hql += " and envid=" + envid;
        }
        if (province != null && !province.trim().equals("")) {
            hql += " and province='" + province + "' ";
        }
        if (city != null && !city.trim().equals("")) {
            hql += " and city='" + city + "' ";
        }
        if (district != null && !district.trim().equals("")) {
            hql += " and district='" + district + "' ";
        }
        if (mac != null) {
            hql += " and mac=" + mac;
        }
/*        if (dis != null) {
            double r = 6371.137;//地球半径米
            double dlng = 2 * Math.asin(Math.sin(dis / (2 * r)) / Math.cos(Double.valueOf(latitude) * Math.PI / 180));
            dlng = dlng * 180 / Math.PI;//角度转为弧度
            double dlat = dis / r;
            dlat = dlat * 180 / Math.PI;
            double minlng = Double.valueOf(longitude) - dlat;
            double minlat = Double.valueOf(latitude) - dlat;
            double maxlng = Double.valueOf(longitude) + dlat;
            double maxlat = Double.valueOf(latitude) + dlat;*/
        if(minlat != null && minlng != null && maxlat != null && maxlng != null){
        	hql += (" and longitude >=" + minlng + " and longitude <=" + maxlng + " and latitude>=" + minlat + " and latitude<=" + maxlat);
        }
//        }
        return getTotalCountByHql(hql);
    }
    public List<CheckPointData> getDeviceDatas(Integer userId, Integer envid) throws Exception {
        String hql = "from " + CheckPointData.class.getName() + " where isDelete=0 ";
        if (userId != null) {
            hql += " and userId=" + userId;
        }
        if (envid != null) {
            hql += " and envid=" + envid;
        }       
        return gets(hql);
    }
    public List<CheckPointData> getDeviceDistrictDatas(Integer userId, Integer envid) throws Exception {
        String hql = "from " + DistrictCheckPointData.class.getName() + " where isDelete=0 ";
        if (userId != null) {
            hql += " and userId=" + userId;
        }
        if (envid != null) {
            hql += " and envid=" + envid;
        }
        return gets(hql);
    }


    public long getDeviceDistrictDatasCount(Integer userId, Integer envid) throws Exception {
        String hql = "select count(*) from " + DistrictCheckPointData.class.getName() + " where isDelete=0 ";
        if (userId != null) {
            hql += " and userId=" + userId;
        }
        if (envid != null) {
            hql += " and envid=" + envid;
        }     
        return getTotalCountByHql(hql);
    }

    public List<CheckPointData> getHisDeviceDatas(Integer userId, Long begindate, Long enddate) throws Exception {
        String hql = "from " + CheckPointData.class.getName() + " where isDelete=0 ";
        if (userId != null) {
            hql += " and userId=" + userId;
        }
        if (begindate != null && enddate != null) {
            hql += " and checkTime >=:starDate and checkTime <=:endDate";
        }

        List<CheckPointData> list = getsByTime(begindate, enddate, hql);
        return list;
    }
    public List<CheckPointData> getHisDeviceDatas(Integer userId, Date begindate, Date enddate) throws Exception {
        String hql = "from " + CheckPointData.class.getName() + " where isDelete=0 ";
        if (userId != null) {
            hql += " and userId=" + userId;
        }
        if (begindate != null && enddate != null) {
            hql += " and checkTime >= :startDate and checkTime <= :endDate";
//            hql += " and checkTime between ? and ? ";
        }

        List<CheckPointData> list = getsByTime(begindate, enddate, hql);
        return list;
    }
    public List<CheckPointData> getHisDeviceDatasByGroupid(String groupid) throws Exception {
        String hql = "from " + CheckPointData.class.getName() + " where isDelete=0 and groupid in("+groupid+")";
        List<CheckPointData> list = gets(hql);
        return list;
    }
    public List<String> getGroupDeviceDatas(Integer userId, Date begindate, Date enddate){
        String hql = "select groupid from " + CheckPointData.class.getName() + " where isDelete=0 ";
        if (userId != null) {
            hql += " and userId=" + userId;
        }
        if (begindate != null && enddate != null) {
            hql += " and checkTime >= :startDate and checkTime <= :endDate ";
//            hql += " and checkTime between ? and ? ";
        }
        hql += " group by groupid ";
        List<String> list = getsByTime(begindate, enddate, hql);
        return list;
    }
    public List<CheckPointData> getMyDeviceDatas(Integer userId, Date begindate, Date enddate){
        String hql = "from " + CheckPointData.class.getName() + " where isDelete=0 ";
        if (userId != null) {
            hql += " and userId=" + userId;
        }
        if (begindate != null && enddate != null) {
            hql += " and checkTime >= :startDate and checkTime <= :endDate ";
//            hql += " and checkTime between ? and ? ";
        }
        List<CheckPointData> list = getsByTime(begindate, enddate, hql);
        return list;
    }
    public long getHisDeviceDatasCount(Integer userId, String begindate, String enddate) throws Exception {
        String hql = "select count(*) from " + CheckPointData.class.getName() + " where isDelete=0 ";
        if (userId != null) {
            hql += " and userId=" + userId;
        }
        if (begindate != null && enddate != null) {
            hql += " checkTime >=:starDate and checkTime <=:endDate";
        }

        Long count = null;
        Session session = sessionFacotry.getCurrentSession();
        try {
            session.beginTransaction();
            Query q = session.createQuery(hql);
            q.setTimestamp("starDate", DateUtils.formatTimestamp(begindate));
            q.setTimestamp("endDate", DateUtils.formatTimestamp(enddate));
            q.executeUpdate();
            session.getTransaction().commit();
            count = (Long) q.uniqueResult();
        } catch (Exception e) {
            session.getTransaction().rollback();
            e.printStackTrace();
        }

        return count;
    }

    public void createDeviceStatisticsData(Long begindate, Long enddate) throws Exception{
        String hql = "select avg(cpValue),province,city,district from " + CheckPointData.class.getName() + " where isDelete=0 ";
        if (begindate != null && enddate != null) {
            hql += " and checkTime >=:starDate and checkTime <=:endDate";
        }
        hql += " GROUP BY province,city,district";

        List results = getsByTime(begindate, enddate, hql);
        Date now = DateUtils.getDate();
        for (int i = 0; i < results.size();i++) {
            Object[] row = (Object[]) results.get(i);
            Double avgCpValue = (Double) row[0];
            String province = (String) row[1];
            String city = (String) row[2];
            String district = (String) row[3];

            CheckPointStatisticsData checkPointStatisticsData = new CheckPointStatisticsData();
            checkPointStatisticsData.setCreateTime(now);
            checkPointStatisticsData.setAvgCpValue(avgCpValue);
            checkPointStatisticsData.setProvince(province);
            checkPointStatisticsData.setCity(city);
            checkPointStatisticsData.setDistrict(district);
            add(checkPointStatisticsData);
        }
    }

    public List<CheckPointStatisticsData> getDeviceStatisticsData() throws Exception {
        String hql = "from " + CheckPointStatisticsData.class.getName() + " as A where A.createTime in (";
        hql += " select max(createTime) from " + CheckPointStatisticsData.class.getName() + " GROUP BY province,city,district)";

        List<CheckPointStatisticsData> results = gets(hql);

        return results;
    }
    public void multiCreateDeviceData(List<CheckPointData> list){
    	Session session = sessionFacotry.getCurrentSession();
		try {
			session.beginTransaction();
			for(CheckPointData cpd : list){
				session.save(cpd);
			}
			session.getTransaction().commit();
		} catch (Exception e) {
			session.getTransaction().rollback();
			e.printStackTrace();
		}
    }
}
