package com.ratection.module.device.bean;

import java.util.Date;

public class BaseCpDataBean {
	private Integer id;
	private Double longitude;
	private Double latitude;
	private Date checkTime;
	private Double cpValue;
	
	public BaseCpDataBean() {
		super();
	}
	public BaseCpDataBean(Integer id, Double longitude, Double latitude,
			Date checkTime, Double cpValue) {
		super();
		this.id = id;
		this.longitude = longitude;
		this.latitude = latitude;
		this.checkTime = checkTime;
		this.cpValue = cpValue;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Double getLongitude() {
		return longitude;
	}
	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}
	public Double getLatitude() {
		return latitude;
	}
	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}
	public Date getCheckTime() {
		return checkTime;
	}
	public void setCheckTime(Date checkTime) {
		this.checkTime = checkTime;
	}
	public Double getCpValue() {
		return cpValue;
	}
	public void setCpValue(Double cpValue) {
		this.cpValue = cpValue;
	}
	@Override
	public String toString() {
		return "BaseCpDataBean [id=" + id + ", longitude=" + longitude
				+ ", latitude=" + latitude + ", checkTime=" + checkTime
				+ ", cpValue=" + cpValue + "]";
	}
	
}
