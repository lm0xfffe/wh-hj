package com.ratection.module.device.bean;

import javax.validation.constraints.NotNull;

public class MultiDeviceDataBean {
	 @NotNull
	    private String nValue;        // noise_value
	    private Integer type;        // 检测类型(0：普通测量  1：飞行测量)
	    @NotNull
	    private Double longitude;        // 经度
	    @NotNull
	    private Double latitude;        // 纬度
	    private Double temp;        // 温度
	    @NotNull
	    private Integer envid;        // 检测环境类型
	    private String envname;        // 检测环境名称
	    @NotNull
	    private Integer continueTime;        // 持续时间
	    private String remark;        // 备注
	    private String deviceVersion;        // 核辐射设备版本号
	    private String firmwareVersion;
	    private String modelNumber;
	    private String serialNumber;
	    private String manufacturerName;
	    @NotNull
	    private String mac;        // 核辐射设备mac
	    private String province;        // 省
	    private String city;        // 市
	    private String district;        // 区
	    private String business;//商圈
	    private String street;//街道
	    private Long checkTime;
	    private Double cpValue;
	    private String groupid;
	    
		public String getGroupid() {
			return groupid;
		}
		public void setGroupid(String groupid) {
			this.groupid = groupid;
		}
		public String getnValue() {
			return nValue;
		}
		public void setnValue(String nValue) {
			this.nValue = nValue;
		}
		public Integer getType() {
			return type;
		}
		public void setType(Integer type) {
			this.type = type;
		}
		public Double getLongitude() {
			return longitude;
		}
		public void setLongitude(Double longitude) {
			this.longitude = longitude;
		}
		public Double getLatitude() {
			return latitude;
		}
		public void setLatitude(Double latitude) {
			this.latitude = latitude;
		}
		public Double getTemp() {
			return temp;
		}
		public void setTemp(Double temp) {
			this.temp = temp;
		}
		public Integer getEnvid() {
			return envid;
		}
		public void setEnvid(Integer envid) {
			this.envid = envid;
		}
		public String getEnvname() {
			return envname;
		}
		public void setEnvname(String envname) {
			this.envname = envname;
		}
		public Integer getContinueTime() {
			return continueTime;
		}
		public void setContinueTime(Integer continueTime) {
			this.continueTime = continueTime;
		}
		public String getRemark() {
			return remark;
		}
		public void setRemark(String remark) {
			this.remark = remark;
		}
		public String getDeviceVersion() {
			return deviceVersion;
		}
		public void setDeviceVersion(String deviceVersion) {
			this.deviceVersion = deviceVersion;
		}
		public String getFirmwareVersion() {
			return firmwareVersion;
		}
		public void setFirmwareVersion(String firmwareVersion) {
			this.firmwareVersion = firmwareVersion;
		}
		public String getModelNumber() {
			return modelNumber;
		}
		public void setModelNumber(String modelNumber) {
			this.modelNumber = modelNumber;
		}
		public String getSerialNumber() {
			return serialNumber;
		}
		public void setSerialNumber(String serialNumber) {
			this.serialNumber = serialNumber;
		}
		public String getManufacturerName() {
			return manufacturerName;
		}
		public void setManufacturerName(String manufacturerName) {
			this.manufacturerName = manufacturerName;
		}
		public String getMac() {
			return mac;
		}
		public void setMac(String mac) {
			this.mac = mac;
		}
		public String getProvince() {
			return province;
		}
		public void setProvince(String province) {
			this.province = province;
		}
		public String getCity() {
			return city;
		}
		public void setCity(String city) {
			this.city = city;
		}
		public String getDistrict() {
			return district;
		}
		public void setDistrict(String district) {
			this.district = district;
		}
		public String getBusiness() {
			return business;
		}
		public void setBusiness(String business) {
			this.business = business;
		}
		public String getStreet() {
			return street;
		}
		public void setStreet(String street) {
			this.street = street;
		}
		public Long getCheckTime() {
			return checkTime;
		}
		public void setCheckTime(Long checkTime) {
			this.checkTime = checkTime;
		}
		public Double getCpValue() {
			return cpValue;
		}
		public void setCpValue(Double cpValue) {
			this.cpValue = cpValue;
		}
		@Override
		public String toString() {
			return "MultiDeviceDataBean [nValue=" + nValue + ", type=" + type
					+ ", longitude=" + longitude + ", latitude=" + latitude
					+ ", temp=" + temp + ", envid=" + envid + ", envname="
					+ envname + ", continueTime=" + continueTime + ", remark="
					+ remark + ", deviceVersion=" + deviceVersion
					+ ", firmwareVersion=" + firmwareVersion + ", modelNumber="
					+ modelNumber + ", serialNumber=" + serialNumber
					+ ", manufacturerName=" + manufacturerName + ", mac=" + mac
					+ ", province=" + province + ", city=" + city
					+ ", district=" + district + ", business=" + business
					+ ", street=" + street + ", checkTime=" + checkTime
					+ ", cpValue=" + cpValue + ", groupid=" + groupid + "]";
		}
	    
}
