package com.ratection.module.device.bean;

import javax.validation.constraints.NotNull;


public class DeviceDataUploadBean {
    @NotNull
    private Double cpValue;        // 检测值
    private String nValue;        // noise_value
    private Integer type;        // 检测类型(0：普通测量  1：飞行测量)
    @NotNull
    private Double longitude;        // 经度
    @NotNull
    private Double latitude;        // 纬度
    private Double temp;        // 温度
    @NotNull
    private Integer envid;        // 检测环境类型
    private String envname;        // 检测环境名称
    @NotNull
    private Integer continueTime;        // 持续时间
    private String remark;        // 备注
    private String deviceVersion;        // 核辐射设备版本号
    private String firmwareVersion;
    private String modelNumber;
    private String serialNumber;
    private String manufacturerName;
    private String deviceName;
    @NotNull
    private String mac;        // 核辐射设备mac
    @NotNull
    private Integer userId;  //关联到用户
    private String province;        // 省
    private String city;        // 市
    private String district;        // 区
    private String business;//商圈
    private String street;//街道
    private String groupid;
    
	public String getGroupid() {
		return groupid;
	}
	public void setGroupid(String groupid) {
		this.groupid = groupid;
	}
	public Double getCpValue() {
		return cpValue;
	}
	public void setCpValue(Double cpValue) {
		this.cpValue = cpValue;
	}
	public String getnValue() {
		return nValue;
	}
	public void setnValue(String nValue) {
		this.nValue = nValue;
	}
	public Integer getType() {
		return type;
	}
	public void setType(Integer type) {
		this.type = type;
	}
	public Double getLongitude() {
		return longitude;
	}
	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}
	public Double getLatitude() {
		return latitude;
	}
	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}
	public Double getTemp() {
		return temp;
	}
	public void setTemp(Double temp) {
		this.temp = temp;
	}
	public Integer getEnvid() {
		return envid;
	}
	public void setEnvid(Integer envid) {
		this.envid = envid;
	}
	public String getEnvname() {
		return envname;
	}
	public void setEnvname(String envname) {
		this.envname = envname;
	}
	public Integer getContinueTime() {
		return continueTime;
	}
	public void setContinueTime(Integer continueTime) {
		this.continueTime = continueTime;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getDeviceVersion() {
		return deviceVersion;
	}
	public void setDeviceVersion(String deviceVersion) {
		this.deviceVersion = deviceVersion;
	}
	public String getFirmwareVersion() {
		return firmwareVersion;
	}
	public void setFirmwareVersion(String firmwareVersion) {
		this.firmwareVersion = firmwareVersion;
	}
	public String getModelNumber() {
		return modelNumber;
	}
	public void setModelNumber(String modelNumber) {
		this.modelNumber = modelNumber;
	}
	public String getSerialNumber() {
		return serialNumber;
	}
	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}
	public String getManufacturerName() {
		return manufacturerName;
	}
	public void setManufacturerName(String manufacturerName) {
		this.manufacturerName = manufacturerName;
	}
	public String getDeviceName() {
		return deviceName;
	}
	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}
	public String getMac() {
		return mac;
	}
	public void setMac(String mac) {
		this.mac = mac;
	}
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public String getProvince() {
		return province;
	}
	public void setProvince(String province) {
		this.province = province;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getDistrict() {
		return district;
	}
	public void setDistrict(String district) {
		this.district = district;
	}
	public String getBusiness() {
		return business;
	}
	public void setBusiness(String business) {
		this.business = business;
	}
	public String getStreet() {
		return street;
	}
	public void setStreet(String street) {
		this.street = street;
	}
	@Override
	public String toString() {
		return "DeviceDataUploadBean [cpValue=" + cpValue + ", nValue="
				+ nValue + ", type=" + type + ", longitude=" + longitude
				+ ", latitude=" + latitude + ", temp=" + temp + ", envid="
				+ envid + ", envname=" + envname + ", continueTime="
				+ continueTime + ", remark=" + remark + ", deviceVersion="
				+ deviceVersion + ", firmwareVersion=" + firmwareVersion
				+ ", modelNumber=" + modelNumber + ", serialNumber="
				+ serialNumber + ", manufacturerName=" + manufacturerName
				+ ", deviceName=" + deviceName + ", mac=" + mac + ", userId="
				+ userId + ", province=" + province + ", city=" + city
				+ ", district=" + district + ", business=" + business
				+ ", street=" + street + ", groupid=" + groupid + "]";
	}
    
}
