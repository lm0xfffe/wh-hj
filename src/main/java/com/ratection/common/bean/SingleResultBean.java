package com.ratection.common.bean;

/**
 * @author franklin.xkk
 */
public class SingleResultBean {
    private Object result;
    private boolean state = true;

    private int error_code;
    private String errorMsg;

    public int getError_code() {

        return error_code;
    }

    public void setError_code(int errorCode) {
        this.error_code = errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String error) {
        this.errorMsg = error;
    }

    public SingleResultBean(Object result) {
        this.result = result;

    }

    public SingleResultBean(Object result, Boolean state) {
        this.result = result;
        this.state = state;
    }

    public Object getResult() {
        return result;
    }

    public void setResult(Object result) {
        this.result = result;
    }

    public boolean isState() {
        return state;
    }

    public void setState(boolean state) {
        this.state = state;
    }

    @Override
    public String toString() {
        return "SingleResultBean{" +
                "result=" + result +
                ", state=" + state +
                ", errorCode=" + error_code +
                ", errorMsg='" + errorMsg + '\'' +
                '}';
    }
}
