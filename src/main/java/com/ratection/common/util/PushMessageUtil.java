package com.ratection.common.util;


import org.json.JSONArray;
import org.json.JSONObject;

import com.ratection.module.push.AndroidNotification;
import com.ratection.module.push.PushClient;
import com.ratection.module.push.android.AndroidBroadcast;
import com.ratection.module.push.android.AndroidCustomizedcast;
import com.ratection.module.push.android.AndroidFilecast;
import com.ratection.module.push.android.AndroidGroupcast;
import com.ratection.module.push.android.AndroidUnicast;
import com.ratection.module.push.ios.IOSBroadcast;
import com.ratection.module.push.ios.IOSCustomizedcast;
import com.ratection.module.push.ios.IOSFilecast;
import com.ratection.module.push.ios.IOSGroupcast;
import com.ratection.module.push.ios.IOSUnicast;

/**
 * 消息推送工具
 *
 */
public class PushMessageUtil {
	private static final String ANDROID_APP_KEY = "581afa52b27b0a2b9b002b24";
	private static final String ANDROID_APP_MASTER_SECRET = "sahnllfo7tmhguhv0z2h85wv6fdfdyry";
	
	private static final String IOS_APP_KEY = "58183a691061d20f50002719";
	private static final String IOS_APP_MASTER_SECRET = "xe8zbbitpc2znfr6vrfrbztc5xbt5dju";
	
	/**
	 * 安卓推送（广播）
	 * @param ticker
	 * @param title
	 * @param text
	 * @param desc
	 * @return
	 * @throws Exception
	 */
	public static String sendAndroidBroadcast(String ticker, String title, String text, String desc) throws Exception {
		PushClient client = new PushClient();
		AndroidBroadcast broadcast = new AndroidBroadcast(ANDROID_APP_KEY, ANDROID_APP_MASTER_SECRET);
		broadcast.setTicker(ticker);
		broadcast.setTitle(title);
		broadcast.setText(text);
		broadcast.setDescription(desc);
		broadcast.goAppAfterOpen();
		broadcast.setDisplayType(AndroidNotification.DisplayType.NOTIFICATION);
		// TODO Set 'production_mode' to 'false' if it's a test device. 
		// For how to register a test device, please see the developer doc.
		broadcast.setProductionMode();
		// Set customized fields
		//broadcast.setExtraField("test", "helloworld");
		return client.send(broadcast);
	}
	
	public static String sendAndroidUnicast(String devicetoken, String tricker,String title,String text, String key,String value) throws Exception {
		PushClient client = new PushClient();
		AndroidUnicast unicast = new AndroidUnicast(ANDROID_APP_KEY, ANDROID_APP_MASTER_SECRET);
		// TODO Set your device token
		unicast.setDeviceToken(devicetoken);
		unicast.setTicker(tricker);
		unicast.setTitle(title);
		unicast.setText(text);
		unicast.goAppAfterOpen();
		unicast.setDisplayType(AndroidNotification.DisplayType.NOTIFICATION);
		// TODO Set 'production_mode' to 'false' if it's a test device. 
		// For how to register a test device, please see the developer doc.
		unicast.setProductionMode();
		// Set customized fields
		unicast.setExtraField(key, value);
		return client.send(unicast);
	}
	
	public void sendAndroidGroupcast() throws Exception {
		PushClient client = new PushClient();
		AndroidGroupcast groupcast = new AndroidGroupcast(ANDROID_APP_KEY, ANDROID_APP_MASTER_SECRET);
		/*  TODO
		 *  Construct the filter condition:
		 *  "where": 
		 *	{
    	 *		"and": 
    	 *		[
      	 *			{"tag":"test"},
      	 *			{"tag":"Test"}
    	 *		]
		 *	}
		 */
		JSONObject filterJson = new JSONObject();
		JSONObject whereJson = new JSONObject();
		JSONArray tagArray = new JSONArray();
		JSONObject testTag = new JSONObject();
		JSONObject TestTag = new JSONObject();
		testTag.put("tag", "test");
		TestTag.put("tag", "Test");
		tagArray.put(testTag);
		tagArray.put(TestTag);
		whereJson.put("and", tagArray);
		filterJson.put("where", whereJson);
		System.out.println(filterJson.toString());
		
		groupcast.setFilter(filterJson);
		groupcast.setTicker( "Android groupcast ticker");
		groupcast.setTitle(  "中文的title");
		groupcast.setText(   "Android groupcast text");
		groupcast.goAppAfterOpen();
		groupcast.setDisplayType(AndroidNotification.DisplayType.NOTIFICATION);
		// TODO Set 'production_mode' to 'false' if it's a test device. 
		// For how to register a test device, please see the developer doc.
		groupcast.setProductionMode();
		client.send(groupcast);
	}
	
	public void sendAndroidCustomizedcast() throws Exception {
		PushClient client = new PushClient();
		AndroidCustomizedcast customizedcast = new AndroidCustomizedcast(ANDROID_APP_KEY, ANDROID_APP_MASTER_SECRET);
		// TODO Set your alias here, and use comma to split them if there are multiple alias.
		// And if you have many alias, you can also upload a file containing these alias, then 
		// use file_id to send customized notification.
		customizedcast.setAlias("alias", "alias_type");
		customizedcast.setTicker( "Android customizedcast ticker");
		customizedcast.setTitle(  "中文的title");
		customizedcast.setText(   "Android customizedcast text");
		customizedcast.goAppAfterOpen();
		customizedcast.setDisplayType(AndroidNotification.DisplayType.NOTIFICATION);
		// TODO Set 'production_mode' to 'false' if it's a test device. 
		// For how to register a test device, please see the developer doc.
		customizedcast.setProductionMode();
		client.send(customizedcast);
	}
	
	public void sendAndroidCustomizedcastFile() throws Exception {
		PushClient client = new PushClient();
		AndroidCustomizedcast customizedcast = new AndroidCustomizedcast(ANDROID_APP_KEY, ANDROID_APP_MASTER_SECRET);
		// TODO Set your alias here, and use comma to split them if there are multiple alias.
		// And if you have many alias, you can also upload a file containing these alias, then 
		// use file_id to send customized notification.
		String fileId = client.uploadContents(ANDROID_APP_KEY, ANDROID_APP_MASTER_SECRET, "aa"+"\n"+"bb"+"\n"+"alias");
		customizedcast.setFileId(fileId, "alias_type");
		customizedcast.setTicker( "Android customizedcast ticker");
		customizedcast.setTitle(  "中文的title");
		customizedcast.setText(   "Android customizedcast text");
		customizedcast.goAppAfterOpen();
		customizedcast.setDisplayType(AndroidNotification.DisplayType.NOTIFICATION);
		// TODO Set 'production_mode' to 'false' if it's a test device. 
		// For how to register a test device, please see the developer doc.
		customizedcast.setProductionMode();
		client.send(customizedcast);
	}
	
	public void sendAndroidFilecast() throws Exception {
		PushClient client = new PushClient();
		AndroidFilecast filecast = new AndroidFilecast(ANDROID_APP_KEY, ANDROID_APP_MASTER_SECRET);
		// TODO upload your device tokens, and use '\n' to split them if there are multiple tokens 
		String fileId = client.uploadContents(ANDROID_APP_KEY, ANDROID_APP_MASTER_SECRET,"aa"+"\n"+"bb");
		filecast.setFileId( fileId);
		filecast.setTicker( "Android filecast ticker");
		filecast.setTitle(  "中文的title");
		filecast.setText(   "Android filecast text");
		filecast.goAppAfterOpen();
		filecast.setDisplayType(AndroidNotification.DisplayType.NOTIFICATION);
		client.send(filecast);
	}
	/**
	 * ios推送（广播）
	 * @param text
	 * @param desc
	 * @throws Exception
	 */
	public static String sendIOSBroadcast(String text, String desc) throws Exception {
		PushClient client = new PushClient();
		IOSBroadcast broadcast = new IOSBroadcast(IOS_APP_KEY, IOS_APP_MASTER_SECRET);

		broadcast.setAlert(text);
		broadcast.setBadge(0);
		broadcast.setDescription(desc);
		//音频
		//broadcast.setSound(text);
		// TODO set 'production_mode' to 'true' if your app is under production mode
		broadcast.setProductionMode();
		// Set customized fields
		//broadcast.setCustomizedField("test", "helloworld");
		return client.send(broadcast);
	}
	
	public static void sendIOSUnicast(String deviceToken,String name, String content) throws Exception {
		PushClient client = new PushClient();
		IOSUnicast unicast = new IOSUnicast(IOS_APP_KEY, IOS_APP_MASTER_SECRET);
		// TODO Set your device token
		unicast.setDeviceToken(deviceToken);
		unicast.setAlert(name);
		unicast.setBadge( 0);
		unicast.setSound( "default");
		// TODO set 'production_mode' to 'true' if your app is under production mode
		unicast.setTestMode();
		// Set customized fields
		unicast.setCustomizedField(name, content);
		client.send(unicast);
	}
	
	public void sendIOSGroupcast() throws Exception {
		PushClient client = new PushClient();
		IOSGroupcast groupcast = new IOSGroupcast(IOS_APP_KEY, IOS_APP_MASTER_SECRET);
		/*  TODO
		 *  Construct the filter condition:
		 *  "where": 
		 *	{
    	 *		"and": 
    	 *		[
      	 *			{"tag":"iostest"}
    	 *		]
		 *	}
		 */
		JSONObject filterJson = new JSONObject();
		JSONObject whereJson = new JSONObject();
		JSONArray tagArray = new JSONArray();
		JSONObject testTag = new JSONObject();
		testTag.put("tag", "iostest");
		tagArray.put(testTag);
		whereJson.put("and", tagArray);
		filterJson.put("where", whereJson);
		System.out.println(filterJson.toString());
		
		// Set filter condition into rootJson
		groupcast.setFilter(filterJson);
		groupcast.setAlert("IOS 组播测试");
		groupcast.setBadge( 0);
		groupcast.setSound( "default");
		// TODO set 'production_mode' to 'true' if your app is under production mode
		groupcast.setTestMode();
		client.send(groupcast);
	}
	
	public void sendIOSCustomizedcast() throws Exception {
		PushClient client = new PushClient();
		IOSCustomizedcast customizedcast = new IOSCustomizedcast(IOS_APP_KEY, IOS_APP_MASTER_SECRET);
		// TODO Set your alias and alias_type here, and use comma to split them if there are multiple alias.
		// And if you have many alias, you can also upload a file containing these alias, then 
		// use file_id to send customized notification.
		customizedcast.setAlias("alias", "alias_type");
		customizedcast.setAlert("IOS 个性化测试");
		customizedcast.setBadge( 0);
		customizedcast.setSound( "default");
		// TODO set 'production_mode' to 'true' if your app is under production mode
		customizedcast.setTestMode();
		client.send(customizedcast);
	}
	
	public void sendIOSFilecast() throws Exception {
		PushClient client = new PushClient();
		IOSFilecast filecast = new IOSFilecast(IOS_APP_KEY, IOS_APP_MASTER_SECRET);
		// TODO upload your device tokens, and use '\n' to split them if there are multiple tokens 
		String fileId = client.uploadContents(IOS_APP_KEY, IOS_APP_MASTER_SECRET, "aa"+"\n"+"bb");
		filecast.setFileId( fileId);
		filecast.setAlert("IOS 文件播测试");
		filecast.setBadge( 0);
		filecast.setSound( "default");
		// TODO set 'production_mode' to 'true' if your app is under production mode
		filecast.setTestMode();
		client.send(filecast);
	}
	
	public static void main(String[] args) {
		try {
//			PushMessageUtil.sendIOSBroadcast("这是一个测试数据","测试");
//			PushMessageUtil.sendIOSUnicast("5cccccb893df271eba1681c72a090df017b106765d2c249445759cebe8f6a39a");
			PushMessageUtil.sendIOSBroadcast("这只是一个测试", "测试");
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	

}
