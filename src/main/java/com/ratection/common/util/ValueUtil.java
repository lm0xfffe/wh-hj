package com.ratection.common.util;

import java.math.BigDecimal;

public class ValueUtil {
	public static String PHONEREX = "((\\d{11})|^((\\d{7,8})|(\\d{4}|\\d{3})-(\\d{7,8})|(\\d{4}|\\d{3})-(\\d{7,8})-(\\d{4}|\\d{3}|\\d{2}|\\d{1})|(\\d{7,8})-(\\d{4}|\\d{3}|\\d{2}|\\d{1}))$)";
	public static double formateDouble(Double d,int n){
		BigDecimal   b   =   new   BigDecimal(d);  
		return b.setScale(n,   BigDecimal.ROUND_HALF_UP).doubleValue();  
	}
	public static void main (String []args){
		System.out.println(formateDouble(111.11d, 6));
	}
}
