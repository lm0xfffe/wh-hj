package com.ratection.common.util;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
/**
 * 
 * @author franklin.xkk
 *
 */
public class DateUtils {
	private static  final SimpleDateFormat shortSdf = new SimpleDateFormat("yyyy-MM-dd");
	private static  final SimpleDateFormat monthSdf = new SimpleDateFormat("yyyy-MM");
	private static final SimpleDateFormat longSdf= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	private static final Integer dayGap = 60*60*24*1000;
	public static Date getDate(long time){
		Calendar calendar = Calendar.getInstance();
		TimeZone tz = TimeZone.getDefault();
		calendar.setTimeZone(tz);
		calendar.setTimeInMillis(time);
		return calendar.getTime();
	}
	public static Date get1970Date()throws ParseException{
		return formatTimestamp("1970-01-01 00:00:00");
	}
	public static Date formatTimestamp(String dateString) throws ParseException{
	    SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	    TimeZone.getTimeZone("GMT+8");
	    return sdf.parse(dateString);
	}
	public static String formatTimestamp(Date date){
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		sdf.setTimeZone(TimeZone.getTimeZone("GMT+8"));
		return sdf.format(date);
	}
	public static Date getDate(){
		TimeZone time = TimeZone.getTimeZone("GMT+8");
		TimeZone.setDefault(time);// 设置时区

		Calendar calendar = Calendar.getInstance();// 获取实例
		Date date = calendar.getTime();
		return date;
	}

	public static Date getYesterDayBeginTime(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(calendar.DATE, -1);
		calendar.set(Calendar.HOUR, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		date = calendar.getTime();
		return date;
	}

	public static Date getYesterDayEndTime(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(calendar.DATE, -1);
		calendar.set(Calendar.HOUR, 23);
		calendar.set(Calendar.MINUTE, 59);
		calendar.set(Calendar.SECOND, 59);
		calendar.set(Calendar.MILLISECOND, 999);
		date = calendar.getTime();
		return date;
	}
	public static Date getDateStartTime(Date now) {
		try {
			now = longSdf.parse(shortSdf.format(now) + " 00:00:00");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return now;
	}

	/**
	 * 获得当天的结束时间
	 * 
	 */
	public static Date getDateEndTime(Date now) {
		try {
			now = longSdf.parse(shortSdf.format(now) + " 23:59:59");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return now;
	}
	public static Date getMonthStartTime(Date time) throws ParseException {
		Date now = monthSdf.parse(shortSdf.format(time));
		return formatTimestamp(longSdf.format(now));
	}

	/**
	 * 月的结束时间，
	 * 
	 * @return
	 * @throws ParseException 
	 */
	public static Date getMonthEndTime(Date time) throws ParseException {
		Date now = monthSdf.parse(shortSdf.format(time));
		return longSdf.parse(monthSdf.format(now)+"-31 23:59:59");
	}
	public static String formatDate(Date date){
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		return sdf.format(date);
	}
	public static Date formatDate(String dateString) throws ParseException{
	    SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
	    return sdf.parse(dateString);
	}
	public static int getDay(Date date){
		Calendar c = Calendar.getInstance();
		if(date == null){
			c.setTime(new Date());
		}else{
			c.setTime(date);
		}
		return c.get(Calendar.DAY_OF_MONTH);
	}
	public static void main(String[]args)throws Exception{
		String date = "2017-07-31";
		System.out.println(DateUtils.getMonthStartTime(DateUtils.formatDate(date)));
		System.out.println(DateUtils.getMonthEndTime(DateUtils.formatDate(date)));
	}
}
