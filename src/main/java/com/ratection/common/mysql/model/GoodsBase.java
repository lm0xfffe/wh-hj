package com.ratection.common.mysql.model;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 仓库信息表
 * @author
 * @version 1.0
 */
@Entity
@Table(name = "t_goods_base")
public class GoodsBase {
    @Id
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name = "increment", strategy = "increment")
    private Integer id;

    private String name;

    private String unit;

    private BigDecimal equivalent;

    @Column(name = "pro_company")
    private String proCompany;

    @Column(name = "validity_temr")
    private String validityTemr;

    private String type;

    public GoodsBase() {
        super();
    }

    public GoodsBase(String name, String unit, BigDecimal equivalent, String proCompany, String validityTemr, String type) {
        this.name = name;
        this.unit = unit;
        this.equivalent = equivalent;
        this.proCompany = proCompany;
        this.validityTemr = validityTemr;
        this.type = type;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public BigDecimal getEquivalent() {
        return equivalent;
    }

    public void setEquivalent(BigDecimal equivalent) {
        this.equivalent = equivalent;
    }

    public String getProCompany() {
        return proCompany;
    }

    public void setProCompany(String proCompany) {
        this.proCompany = proCompany;
    }

    public String getValidityTemr() {
        return validityTemr;
    }

    public void setValidityTemr(String validityTemr) {
        this.validityTemr = validityTemr;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
