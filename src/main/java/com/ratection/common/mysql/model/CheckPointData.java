package com.ratection.common.mysql.model;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

import java.util.Date;

@Entity
@Table(name = "hj_cp_data")
public class CheckPointData {
    @Id
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name = "increment", strategy = "increment")
    private Integer id;        // 检测id
    @Column(name = "checkpoint_value")
    private Double cpValue;        // 检测值
    @Column(name = "noise_value")
    private Double nValue;        // noise_value
    private Integer type;        // 检测类型(0：普通测量  1：飞行测量)
    private Double longitude;        // 经度
    private Double latitude;        // 纬度
    private Double temp;        // 温度
    private Integer envid;        // 检测环境类型
    private String envname;        // 检测环境名称
    @Column(name = "continue_time")
    private Integer continueTime;        // 持续时间
    private String remark;        // 备注
    @Column(name = "device_version")
    private String deviceVersion;        // 核辐射设备版本号
    @Column(name = "firmware_version")
    private String firmwareVersion;
    @Column(name = "model_number")
    private String modelNumber;
    @Column(name = "serial_number")
    private String serialNumber;
    @Column(name = "manufacturer_name")
    private String manufacturerName;
    
    private String groupid;
    
    private String mac;        // 核辐射设备mac
    @Column(name = "device_name")
    private String deviceName;
    private String province;        // 省
    private String city;        // 市
    private String district;        // 区
    private String business;//商圈
    private String street;//街道
    @Column(name = "user_id")
    private Integer userId;  //关联到用户
    @Column(name = "is_delete")
    private Integer isDelete;  //删除标记

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "check_time", updatable = false)
    private Date checkTime;        // 检测时间

    public String getGroupid() {
		return groupid;
	}

	public void setGroupid(String groupid) {
		this.groupid = groupid;
	}

	public String getDeviceName() {
		return deviceName;
	}
    
	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}

	public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getCpValue() {
        return cpValue;
    }

    public void setCpValue(Double cpValue) {
        this.cpValue = cpValue;
    }

    public Double getnValue() {
        return nValue;
    }

    public void setnValue(Double nValue) {
        this.nValue = nValue;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getTemp() {
        return temp;
    }

    public void setTemp(Double temp) {
        this.temp = temp;
    }

    public Integer getEnvid() {
        return envid;
    }

    public void setEnvid(Integer envid) {
        this.envid = envid;
    }

    public String getEnvname() {
        return envname;
    }

    public void setEnvname(String envname) {
        this.envname = envname;
    }

    public Integer getContinueTime() {
        return continueTime;
    }

    public void setContinueTime(Integer continueTime) {
        this.continueTime = continueTime;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getDeviceVersion() {
        return deviceVersion;
    }

    public void setDeviceVersion(String deviceVersion) {
        this.deviceVersion = deviceVersion;
    }

    public String getMac() {
        return mac;
    }

    public void setMac(String mac) {
        this.mac = mac;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getBusiness() {
        return business;
    }

    public void setBusiness(String business) {
        this.business = business;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Integer isDelete) {
        this.isDelete = isDelete;
    }

    public Date getCheckTime() {
        return checkTime;
    }

    public void setCheckTime(Date checkTime) {
        this.checkTime = checkTime;
    }

	public String getFirmwareVersion() {
		return firmwareVersion;
	}

	public void setFirmwareVersion(String firmwareVersion) {
		this.firmwareVersion = firmwareVersion;
	}

	public String getModelNumber() {
		return modelNumber;
	}

	public void setModelNumber(String modelNumber) {
		this.modelNumber = modelNumber;
	}

	public String getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}

	public String getManufacturerName() {
		return manufacturerName;
	}

	public void setManufacturerName(String manufacturerName) {
		this.manufacturerName = manufacturerName;
	}

	@Override
	public String toString() {
		return "CheckPointData [id=" + id + ", cpValue=" + cpValue
				+ ", nValue=" + nValue + ", type=" + type + ", longitude="
				+ longitude + ", latitude=" + latitude + ", temp=" + temp
				+ ", envid=" + envid + ", envname=" + envname
				+ ", continueTime=" + continueTime + ", remark=" + remark
				+ ", deviceVersion=" + deviceVersion + ", firmwareVersion="
				+ firmwareVersion + ", modelNumber=" + modelNumber
				+ ", serialNumber=" + serialNumber + ", manufacturerName="
				+ manufacturerName + ", groupid=" + groupid + ", mac=" + mac
				+ ", deviceName=" + deviceName + ", province=" + province
				+ ", city=" + city + ", district=" + district + ", business="
				+ business + ", street=" + street + ", userId=" + userId
				+ ", isDelete=" + isDelete + ", checkTime=" + checkTime + "]";
	}

}
