package com.ratection.common.mysql.model;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 仓库入库出库信息表
 * @author
 * @version 1.0
 */
@Entity
@Table(name = "t_warehouse_in_out_record")
public class WhInOutRecord{
    @Id
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name = "increment", strategy = "increment")
    private Integer id;

    @Column(name = "goods_id")
    private Integer goodsId;

    private BigDecimal num;

    private BigDecimal equivalent;

    @Column(name = "record_id")
    private Integer recordId;

    @Column(name = "warehouse_id")
    private Integer warehouseId;

    //批次
    private String batch;

    public WhInOutRecord() {
        super();
    }

    public WhInOutRecord(Integer goodsId, BigDecimal num, BigDecimal equivalent, Integer recordId, Integer warehouseId) {
        super();

        this.goodsId = goodsId;
        this.num = num;
        this.equivalent = equivalent;
        this.recordId = recordId;
        this.warehouseId = warehouseId;
    }

    public WhInOutRecord(Integer id, Integer goodsId, BigDecimal num, BigDecimal equivalent, Integer recordId, Integer warehouseId) {
        super();

        this.id = id;
        this.goodsId = goodsId;
        this.num = num;
        this.equivalent = equivalent;
        this.recordId = recordId;
        this.warehouseId = warehouseId;
    }

    public WhInOutRecord(Integer goodsId, BigDecimal num, BigDecimal equivalent, Integer recordId, Integer warehouseId, String batch) {
        this.goodsId = goodsId;
        this.num = num;
        this.equivalent = equivalent;
        this.recordId = recordId;
        this.warehouseId = warehouseId;
        this.batch = batch;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Integer goodsId) {
        this.goodsId = goodsId;
    }

    public BigDecimal getNum() {
        return num;
    }

    public void setNum(BigDecimal num) {
        this.num = num;
    }

    public BigDecimal getEquivalent() {
        return equivalent;
    }

    public void setEquivalent(BigDecimal equivalent) {
        this.equivalent = equivalent;
    }

    public Integer getRecordId() {
        return recordId;
    }

    public void setRecordId(Integer recordId) {
        this.recordId = recordId;
    }

    public Integer getWarehouseId() {
        return warehouseId;
    }

    public void setWarehouseId(Integer warehouseId) {
        this.warehouseId = warehouseId;
    }

    public String getBatch() {
        return batch;
    }

    public void setBatch(String batch) {
        this.batch = batch;
    }

    @Override
    public String toString() {
        return "WhInOutRecord{" +
                "id=" + id +
                ", goodsId=" + goodsId +
                ", num=" + num +
                ", equivalent=" + equivalent +
                ", recordId=" + recordId +
                ", warehouseId=" + warehouseId +
                ", batch='" + batch + '\'' +
                '}';
    }
}
