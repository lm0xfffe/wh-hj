package com.ratection.common.mysql.model;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 仓库入库出库详细信息表
 * @author
 * @version 1.0
 */
@Entity
@Table(name = "t_warehouse_in_out_record_detail")
public class WhInOutRecordDetail {
    @Id
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name = "increment", strategy = "increment")
    private Integer id;

    //类型
    private String type;

    //操作人
    private Integer operator;

    //协助人
    private Integer assistant;

    //操作时间
    private Date updateTime;

    //当量
    private BigDecimal equivalent;

    //数量
    private BigDecimal num;

    //交接人
    private Integer handover;

    //源地点
    private String origination;

    //目的地
    private String destination;

    //备注
    private String remark;

    public WhInOutRecordDetail() {
        super();
    }

    public WhInOutRecordDetail(String type, Integer operator, Integer assistant, Date updateTime, BigDecimal equivalent, BigDecimal num, Integer handover, String remark) {
        super();

        this.type = type;
        this.operator = operator;
        this.assistant = assistant;
        this.updateTime = updateTime;
        this.equivalent = equivalent;
        this.num = num;
        this.handover = handover;
        this.remark = remark;
    }

    public WhInOutRecordDetail(Integer id, String type, Integer operator, Integer assistant, Date update_time, BigDecimal equivalent, BigDecimal num, Integer handover, String remark) {
        super();
        this.id = id;
        this.type = type;
        this.operator = operator;
        this.assistant = assistant;
        this.updateTime = update_time;
        this.equivalent = equivalent;
        this.num = num;
        this.handover = handover;
        this.remark = remark;
    }

    public WhInOutRecordDetail(String type, Integer operator, Integer assistant, Date updateTime, BigDecimal equivalent, BigDecimal num, Integer handover, String destination, String remark) {
        this.type = type;
        this.operator = operator;
        this.assistant = assistant;
        this.updateTime = updateTime;
        this.equivalent = equivalent;
        this.num = num;
        this.handover = handover;
        this.destination = destination;
        this.remark = remark;
    }

    public WhInOutRecordDetail(String type, Integer operator, Integer assistant, Date updateTime, BigDecimal equivalent, BigDecimal num, Integer handover, String origination, String destination, String remark) {
        this.type = type;
        this.operator = operator;
        this.assistant = assistant;
        this.updateTime = updateTime;
        this.equivalent = equivalent;
        this.num = num;
        this.handover = handover;
        this.origination = origination;
        this.destination = destination;
        this.remark = remark;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getOperator() {
        return operator;
    }

    public void setOperator(Integer operator) {
        this.operator = operator;
    }

    public Integer getAssistant() {
        return assistant;
    }

    public void setAssistant(Integer assistant) {
        this.assistant = assistant;
    }

    public Date getUpdate_time() {
        return updateTime;
    }

    public void setUpdate_time(Date update_time) {
        this.updateTime = update_time;
    }

    public BigDecimal getEquivalent() {
        return equivalent;
    }

    public void setEquivalent(BigDecimal equivalent) {
        this.equivalent = equivalent;
    }

    public BigDecimal getNum() {
        return num;
    }

    public void setNum(BigDecimal num) {
        this.num = num;
    }

    public Integer getHandover() {
        return handover;
    }

    public void setHandover(Integer handover) {
        this.handover = handover;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getOrigination() {
        return origination;
    }

    public void setOrigination(String origination) {
        this.origination = origination;
    }

    @Override
    public String toString() {
        return "WhInOutRecordDetail{" +
                "id=" + id +
                ", type='" + type + '\'' +
                ", operator=" + operator +
                ", assistant=" + assistant +
                ", updateTime=" + updateTime +
                ", equivalent=" + equivalent +
                ", num=" + num +
                ", handover=" + handover +
                ", origination='" + origination + '\'' +
                ", destination='" + destination + '\'' +
                ", remark='" + remark + '\'' +
                '}';
    }
}
