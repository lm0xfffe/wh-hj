package com.ratection.common.mysql.model;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by twh on 17/7/16.
 */

@Entity
@Table(name = "hj_message_push")
public class MsgPush {
    @Id
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name = "increment", strategy = "increment")
    @Column(name = "id")
    private Integer id;
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "message_publish_time")
    private Date messagePublishTime;
    @Column(name = "message_title")
    private String messageTitle;		// 消息标题
    @Column(name = "message_content")
    private String messageContent;		// 消息内容
    private String status;		// 状态(0:未发布,1:已发布)
    private Integer fcu;		// 首次创建人（管理员）
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fcd")
    private Date fcd;		// 首次创建时间（管理员）
    private Integer lcu;		// 最后修改人（管理员）
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "lcd")
    private Date lcd;		// 最后修改时间（管理员）

    @Column(name = "is_delete")
    private Integer isDelete;  //删除标记

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getMessagePublishTime() {
        return messagePublishTime;
    }

    public void setMessagePublishTime(Date messagePublishTime) {
        this.messagePublishTime = messagePublishTime;
    }

    public String getMessageTitle() {
        return messageTitle;
    }

    public void setMessageTitle(String messageTitle) {
        this.messageTitle = messageTitle;
    }

    public String getMessageContent() {
        return messageContent;
    }

    public void setMessageContent(String messageContent) {
        this.messageContent = messageContent;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getFcu() {
        return fcu;
    }

    public void setFcu(Integer fcu) {
        this.fcu = fcu;
    }

    public Date getFcd() {
        return fcd;
    }

    public void setFcd(Date fcd) {
        this.fcd = fcd;
    }

    public Integer getLcu() {
        return lcu;
    }

    public void setLcu(Integer lcu) {
        this.lcu = lcu;
    }

    public Date getLcd() {
        return lcd;
    }

    public void setLcd(Date lcd) {
        this.lcd = lcd;
    }

    public Integer getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Integer isDelete) {
        this.isDelete = isDelete;
    }

    @Override
    public String toString() {
        return "MsgPush{" +
                "id=" + id +
                ", messagePublishTime=" + messagePublishTime +
                ", messageTitle='" + messageTitle + '\'' +
                ", messageContent='" + messageContent + '\'' +
                ", status='" + status + '\'' +
                ", fcu='" + fcu + '\'' +
                ", fcd=" + fcd +
                ", lcu='" + lcu + '\'' +
                ", lcd=" + lcd +
                ", isDelete=" + isDelete +
                '}';
    }
}
