package com.ratection.common.mysql.model;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 仓库货物表
 * @author
 * @version 1.0
 */
@Entity
@Table(name = "t_warehouse_goods")
public class WhGoods {
    @Id
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name = "increment", strategy = "increment")
    private Integer id;

    //仓库id
    @Column(name = "warehouse_id")
    private int warehouseId;

    //货品id
    @Column(name = "goods_id")
    private Integer goodsId;

    //数量
    private BigDecimal num;

    //当量
    private BigDecimal equivalent;

    //更新时间
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "update_time")
    private Date updateTime;

    //更新人
    @Column(name = "update_person")
    private Integer updatePerson;

    //位置
    private String position;

    public WhGoods() {
        super();
    }

    public WhGoods(Integer id, int warehouseId, Integer goodsId, BigDecimal num, BigDecimal equivalent, Date updateTime, Integer updatePerson, String position) {
        super();
        this.id = id;
        this.warehouseId = warehouseId;
        this.goodsId = goodsId;
        this.num = num;
        this.equivalent = equivalent;
        this.updateTime = updateTime;
        this.updatePerson = updatePerson;
        this.position = position;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getWarehouseId() {
        return warehouseId;
    }

    public void setWarehouseId(int warehouseId) {
        this.warehouseId = warehouseId;
    }

    public Integer getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Integer goodsId) {
        this.goodsId = goodsId;
    }

    public BigDecimal getNum() {
        return num;
    }

    public void setNum(BigDecimal num) {
        this.num = num;
    }

    public BigDecimal getEquivalent() {
        return equivalent;
    }

    public void setEquivalent(BigDecimal equivalent) {
        this.equivalent = equivalent;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Integer getUpdatePerson() {
        return updatePerson;
    }

    public void setUpdatePerson(Integer updatePerson) {
        this.updatePerson = updatePerson;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    @Override
    public String toString() {
        return "WhGoods{" +
                "id=" + id +
                ", warehouseId=" + warehouseId +
                ", goodsId=" + goodsId +
                ", num=" + num +
                ", equivalent=" + equivalent +
                ", updateTime=" + updateTime +
                ", updatePerson=" + updatePerson +
                ", position='" + position + '\'' +
                '}';
    }
}
