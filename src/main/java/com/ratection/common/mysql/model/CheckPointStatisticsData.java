package com.ratection.common.mysql.model;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by twh on 17/7/16.
 */
@Entity
@Table(name = "hj_cp_statistics_data")
public class CheckPointStatisticsData {
    @Id
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name = "increment", strategy = "increment")
    private Integer id;        // id
    @Column(name = "avg_checkpoint_value")
    private Double avgCpValue;        // 平均检测值
    private String province;        // 省
    private String city;        // 市
    private String district;        // 区
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "create_time", updatable = false)
    private Date createTime;        // 创建时间

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getAvgCpValue() {
        return avgCpValue;
    }

    public void setAvgCpValue(Double avgCpValue) {
        this.avgCpValue = avgCpValue;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Override
    public String toString() {
        return "CheckPointStatisticsData{" +
                "id=" + id +
                ", avgCpValue='" + avgCpValue + '\'' +
                ", province='" + province + '\'' +
                ", city='" + city + '\'' +
                ", district='" + district + '\'' +
                ", createTime=" + createTime +
                '}';
    }
}
