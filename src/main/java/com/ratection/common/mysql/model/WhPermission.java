package com.ratection.common.mysql.model;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

/**
 * 用户仓库权限
 * @author
 * @version 1.0
 */
@Entity
@Table(name = "t_jcqx_warehouse")
public class WhPermission {
    @Id
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name = "increment", strategy = "increment")
    private Integer id;

    @Column(name = "user_id")
    private Integer userId;

    @Column(name = "warehouse_id")
    private Integer warehouseId;

    public WhPermission() {
        super();
    }

    public WhPermission(Integer id, Integer userId, Integer warehouseId) {
        super();
        this.id = id;
        this.userId = userId;
        this.warehouseId = warehouseId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getWarehouseId() {
        return warehouseId;
    }

    public void setWarehouseId(Integer warehouseId) {
        this.warehouseId = warehouseId;
    }

    @Override
    public String toString() {
        return "WhPermission{" +
                "id=" + id +
                ", userId=" + userId +
                ", warehouseId=" + warehouseId +
                '}';
    }
}
