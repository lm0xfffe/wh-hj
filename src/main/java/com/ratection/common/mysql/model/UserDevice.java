package com.ratection.common.mysql.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "hj_device")
public class UserDevice {
	 @Id
	    @GeneratedValue(generator = "increment")
	    @GenericGenerator(name = "increment", strategy = "increment")
	    private Integer id;
	    private String name;
	    @Column(name = "device_version")
	    private String deviceVersion;        // 核辐射设备版本号
	    @Column(name = "firmware_version")
	    private String firmwareVersion;
	    @Column(name = "model_number")
	    private String modelNumber;
	    @Column(name = "serial_number")
	    private String serialNumber;
	    @Column(name = "manufacturer_name")
	    private String manufacturerName;
	    private String mac;        // 核辐射设备mac
	    @Column(name = "user_id")
	    private Integer userId;
	    
	    @Temporal(TemporalType.TIMESTAMP)
	    @Column(name = "update_time")
	    private Date updateTime;

	    @Temporal(TemporalType.TIMESTAMP)
	    @Column(name = "create_time", updatable = false)
	    private Date createTime;

		public String getFirmwareVersion() {
			return firmwareVersion;
		}

		public void setFirmwareVersion(String firmwareVersion) {
			this.firmwareVersion = firmwareVersion;
		}

		public String getModelNumber() {
			return modelNumber;
		}

		public void setModelNumber(String modelNumber) {
			this.modelNumber = modelNumber;
		}

		public String getSerialNumber() {
			return serialNumber;
		}

		public void setSerialNumber(String serialNumber) {
			this.serialNumber = serialNumber;
		}

		public String getManufacturerName() {
			return manufacturerName;
		}

		public void setManufacturerName(String manufacturerName) {
			this.manufacturerName = manufacturerName;
		}

		public Integer getId() {
			return id;
		}

		public void setId(Integer id) {
			this.id = id;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getDeviceVersion() {
			return deviceVersion;
		}

		public void setDeviceVersion(String deviceVersion) {
			this.deviceVersion = deviceVersion;
		}

		public String getMac() {
			return mac;
		}

		public void setMac(String mac) {
			this.mac = mac;
		}

		public Integer getUserId() {
			return userId;
		}

		public void setUserId(Integer userId) {
			this.userId = userId;
		}

		public Date getUpdateTime() {
			return updateTime;
		}

		public void setUpdateTime(Date updateTime) {
			this.updateTime = updateTime;
		}

		public Date getCreateTime() {
			return createTime;
		}

		public void setCreateTime(Date createTime) {
			this.createTime = createTime;
		}

		@Override
		public String toString() {
			return "UserDevice [id=" + id + ", name=" + name
					+ ", deviceVersion=" + deviceVersion + ", firmwareVersion="
					+ firmwareVersion + ", modelNumber=" + modelNumber
					+ ", serialNumber=" + serialNumber + ", manufacturerName="
					+ manufacturerName + ", mac=" + mac + ", userId=" + userId
					+ ", updateTime=" + updateTime + ", createTime="
					+ createTime + "]";
		}

	    
}
