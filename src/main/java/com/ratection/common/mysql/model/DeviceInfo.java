package com.ratection.common.mysql.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "hj_device_info")
public class DeviceInfo {
	 @Id
	    @GeneratedValue(generator = "increment")
	    @GenericGenerator(name = "increment", strategy = "increment")
	    private Integer id;
	    private String name;
	    private String version;
	    private String icon;
		public Integer getId() {
			return id;
		}
		public void setId(Integer id) {
			this.id = id;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getVersion() {
			return version;
		}
		public void setVersion(String version) {
			this.version = version;
		}
		public String getIcon() {
			return icon;
		}
		public void setIcon(String icon) {
			this.icon = icon;
		}
		@Override
		public String toString() {
			return "DeviceInfo [id=" + id + ", name=" + name + ", version="
					+ version + ", icon=" + icon + "]";
		}

}
