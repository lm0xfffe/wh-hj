package com.ratection.common.mysql.model;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Date;

/**
 * 仓库库存设置关系表
 * @author
 * @version 1.0
 */
@Entity
@Table(name = "t_warehouse_goods_base")
public class WhGoodsBase {
    @Id
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name = "increment", strategy = "increment")
    private Integer id;

    //仓库id
    @Column(name = "warehouse_id")
    private Integer warehouseId;

    //货物id
    @Column(name = "good_id")
    private Integer goodId;

    //创建时间
    @Column(name = "create_time")
    private Date createTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getWarehouseId() {
        return warehouseId;
    }

    public void setWarehouseId(Integer warehouseId) {
        this.warehouseId = warehouseId;
    }

    public Integer getGoodsId() {
        return goodId;
    }

    public void setGoodsId(Integer goodsId) {
        this.goodId = goodsId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public WhGoodsBase() {
        super();
    }

    public WhGoodsBase(Integer id, Integer warehouseId, Integer goodsId, Date createTime) {
        super();
        this.id = id;
        this.warehouseId = warehouseId;
        this.goodId = goodsId;
        this.createTime = createTime;
    }

    @Override
    public String toString() {
        return "WhGoodsBase{" +
                "id='" + id + '\'' +
                ", warehouseId='" + warehouseId + '\'' +
                ", goodsId='" + goodId + '\'' +
                ", createTime=" + createTime +
                '}';
    }
}
