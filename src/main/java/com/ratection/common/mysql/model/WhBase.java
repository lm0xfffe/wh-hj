package com.ratection.common.mysql.model;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 仓库信息表
 * @author
 * @version 1.0
 */
@Entity
@Table(name = "t_warehouse_base")
public class WhBase {
    @Id
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name = "increment", strategy = "increment")
    private Integer id;

    //名称
    private String name;

    //当前当量
    @Column(name = "current_equivalent")
    private BigDecimal currentEquivalent;

    //剩余当量
    @Column(name = "surplus_equivalent")
    private BigDecimal surplusEquivalent;

    //总当量
    @Column(name = "total_equivalent")
    private BigDecimal totalEquivalent;

    //上次更新时间
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "last_update_time")
    private Date lastUpdateTime;

    //上次检查时间
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "last_inspect_time")
    private Date lastInspectTime;

    //上次盘库时间
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "last_check_time")
    private Date lastCheckTime;

    private BigDecimal threshold;

    @Column(name = "device_id")
    private String deviceId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getCurrentEquivalent() {
        return currentEquivalent;
    }

    public void setCurrentEquivalent(BigDecimal currentEquivalent) {
        this.currentEquivalent = currentEquivalent;
    }

    public BigDecimal getSurplusEquivalent() {
        return surplusEquivalent;
    }

    public void setSurplusEquivalent(BigDecimal surplusEquivalent) {
        this.surplusEquivalent = surplusEquivalent;
    }

    public BigDecimal getTotalEquivalent() {
        return totalEquivalent;
    }

    public void setTotalEquivalent(BigDecimal totalEquivalent) {
        this.totalEquivalent = totalEquivalent;
    }

    public Date getLastUpdateTime() {
        return lastUpdateTime;
    }

    public void setLastUpdateTime(Date lastUpdateTime) {
        this.lastUpdateTime = lastUpdateTime;
    }

    public Date getLastInspectTime() {
        return lastInspectTime;
    }

    public void setLastInspectTime(Date lastInspectTime) {
        this.lastInspectTime = lastInspectTime;
    }

    public Date getLastCheckTime() {
        return lastCheckTime;
    }

    public void setLastCheckTime(Date lastCheckTime) {
        this.lastCheckTime = lastCheckTime;
    }

    public BigDecimal getThreshold() {
        return threshold;
    }

    public void setThreshold(BigDecimal threshold) {
        this.threshold = threshold;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public WhBase() {
        super();
    }

    public WhBase(String name, BigDecimal currentEquivalent, BigDecimal surplusEquivalent, BigDecimal totalEquivalent, Date lastUpdateTime, Date lastInspectTime, Date lastCheckTime, BigDecimal threshold, String deviceId) {
        super();

        this.name = name;
        this.currentEquivalent = currentEquivalent;
        this.surplusEquivalent = surplusEquivalent;
        this.totalEquivalent = totalEquivalent;
        this.lastUpdateTime = lastUpdateTime;
        this.lastInspectTime = lastInspectTime;
        this.lastCheckTime = lastCheckTime;
        this.threshold = threshold;
        this.deviceId = deviceId;
    }

    @Override
    public String toString() {
        return "WhBase{" +
                "id=" + id +
                ", name=" + name +
                ", currentEquivalent=" + currentEquivalent +
                ", surplusEquivalent=" + surplusEquivalent +
                ", totalEquivalent=" + totalEquivalent +
                ", lastUpdateTime=" + lastUpdateTime +
                ", lastInspectTime=" + lastInspectTime +
                ", lastCheckTime=" + lastCheckTime +
                ", threshold=" + threshold +
                ", deviceId='" + deviceId + '\'' +
                '}';
    }
}
