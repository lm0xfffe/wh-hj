package com.ratection.common.cache;

public class CacheTableNames {
    public static final String PREFIX_TOKEN = "hjtoken:info:";
    public static final String PREFIX_USER = "hjuser:info:";
    public static final String PREFIX_CAPTCHAP = "hjcaptcha:phone:";
    public static final String PREFIX_CAPTCHAE = "hjcaptcha:email:";
}
