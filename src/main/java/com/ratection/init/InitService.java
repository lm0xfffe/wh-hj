package com.ratection.init;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;


@Component
public class InitService {
    private static Logger logger = LoggerFactory.getLogger(InitService.class);

    @PostConstruct
    public void init() throws Exception {
        initcatche();
        startService();
    }

    @PreDestroy
    public void destory() {
    }

    private void initcatche() {
        try {
        } catch (Exception e) {
            logger.error("error to init device key cache:{}", e.getMessage());
            e.printStackTrace();
        }
    }

    private void initPay() {
        //TODO:
    }

    private void startService() {
        try {
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }
}

